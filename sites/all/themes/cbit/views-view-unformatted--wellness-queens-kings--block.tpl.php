<?php
/**
 * List all the Wellness Queens in a block in Home (View Name: wellness_queens_kings (User) [Block])
 * 
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
$term_data = $view->style_plugin->rendered_fields;
// echo '<pre>'; print_r($term_data); exit;
?>
<?php foreach ($term_data as $key => $value) { ?>
  <div class="brick brick-overflow">
    <div class="grid-item-02"> <!--first grid-->
      <a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$term_data[$key]['uid_1']); ?>"><img src="<?php echo $term_data[$key]['field_wellness_queen_kings_image']; ?>" width="100%"></a>
      <div class="grid2-info wellness">
        <h3><?php echo $term_data[$key]['nothing']; ?></h3>
        <span class="blog"><?php echo _user_node_count($term_data[$key]['uid_1']); ?> BLOGINDLÆG </span><br>
        <span class="followers"><?php print $term_data[$key]['count']; ?> Følgere</span>
        <?php if ($user->uid != $term_data[$key]['uid_1']) { ?>
          <div class="follow">
            <?php
            if ($user->uid == 0) {
              print '<span class="follow-title">' . l('FØLG mig', 'ajax_register/login/nojs', $options) . '</span>';
            } else {
              print '<span class="follow-title">' . $term_data[$key]['ops'] . '</span>';
            }
            ?>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php } ?>
