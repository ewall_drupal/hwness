<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
$term_datas = $view->style_plugin->rendered_fields;
global $base_url;
// echo '<pre>';print_r($term_datas);exit;
?>
<?php
foreach($term_datas as $term_data) {
	//echo '<pre>';print_r($term_data);exit;
    $iuid = $term_data['uid'];
		$user_uid = $iuid;
	 if(is_numeric($user_uid)){
		$iuid = $term_data['uid'];
	 }else {
		 $user_array = str_replace("<a href=\"","",$user_uid);
		 $user_dts = explode(">",$user_array);
		 $iuid = $user_dts[1];
	 }
	 
		$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('blog_profile', $images );
		}else {
		$user_thumbnail_url =  $term_data['field_enterprise_blog_picture'];
		}

   // $user_thumbnail_url = get_user_image($uid);
?>
<div class="blog-grid-item">
		<span class="label"><?php echo $term_data['field_categories']; ?></span>
		<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data['nid']); ?>"><img src="<?php echo $term_data['field_image'] ?>" class="img-responsive"></a>
		<p><?php echo $term_data['title'] ?></p>
		<div class="blog-grid-details">
			<span class="blog-author">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$term_data['uid']); ?>">
					<img src="<?php echo $user_thumbnail_url; ?>">
				</a>
			</span>
			<span class="blog-author-info">
				<p><?php echo $term_data['created'] ?>&nbsp;<span class="blog-post-time post-time"><?php echo $term_data['created_1'] ?></span><br><?php echo $term_data['name'] ?></p>
			</span>
			<span class="blog-likes">
				<?php echo like_widget_node($term_data['nid']); ?>
			</span>
			<!--<span class="blog-comments">
				<a href="<?php //echo base_path().'node/'.$term_data['nid'].'#comment-list'; ?>"><span class="blog-commentsicon"></span><?php //echo $term_data['comment_count'] ?> kommentarer</a>
			</span>-->
			<span class="comment">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data['nid']).'#comment-list'; ?>"><?php echo $term_data['comment_count'] ?> kommentarer</a>
			</span>
			<span class="clear"></span>
		</div>
		<div class="clear"></div>
</div>
<?php
}
?>
