<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
/*
$vid = 6;
$kategori = taxonomy_get_tree($vid);
$kategori_options = '<option value="All">--- Vælg kategori ---</option>';
foreach ($kategori as $key => $val) {
  $kategori_options .= '<option value="' . $kategori[$key]->tid . '">' . $kategori[$key]->name . '</option>';
} */
$kategori_options = active_comment_terms();
?>

<?php 
	//$detect = mobile_detect_get_object();
	//$is_mobile = $detect->isMobile();
	$device = check_device();
?>

<?php if($device != "mobile") { ?>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/freewall.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>

	<?php if($device != "mobile") { ?>	

	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			   <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
	
	<?php } ?>

	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			<!-- <li><a title="" href="<?php //echo base_path(); ?>/last-minute">OPRET PROFIL</a></li>-->
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->		
	
	
<div class="main-content-container container">

		<?php if($device == "mobile") { ?>
			  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>	

	<div class="clear-float"></div>

<?php print $messages; ?>
	<!-- main content -->
	<div class="lastminute-header">
	  <!--header-->
	  <h1>Anmeldelser</h1>
	  <h5>Læs de ærlige anmeldelser direkte fra kunderne selv... Husk at
		skrive dine egne anmeldelser og vær med i konkurrencen!</h5>
	</div>
	<div class="clear-float"></div>

	<div class="anmeldelser">
	  <div class="lastminute-content">
		<div class="lastminut-form">
			<?php if($device != "mobile") { ?>
				<h2>&nbsp;</h2>
			<?php } ?>
		  <form id="anmeld-form">
			<div class="lastminute-selectbox category">
			  <h4>Anmeldelses kategori</h4>
			  <label>
				<select style= "width:240px;" id ="select1">
				  <?php echo $kategori_options; ?>
				</select>
			  </label>
			</div>

			<div class="lastminute-selectbox location">
			  <h4>Sted</h4>
			  <input onfocus="if (this.value === 'Indtast by eller postnr.') {
					this.value = '';
				  }" style="width:257px;" id="place" type="text" value="Indtast by eller postnr.">
			</div>

			<div class="lastminute-selectbox radius">
			  <h4>Område radius</h4>
			  <label>
				<select style="width:158px;" id ="select2">
				  <option value="5">0-5 km</option>
				  <option value="10">0-10 km</option>
				  <option value="15">0-15 km</option>
				  <option value="20">0-20 km</option>
				</select>
			  </label>
			</div>

			<div class="lastminute-selectbox sort">
			  <h4>Sorter efter</h4>
			  <label>
				<select style="width:181px;" id ="select3">
				  <option value="created">Seneste anmeldelse</option>
				  <option value="field_clinic_loc_rating_rating">Mest populære</option>
				</select>
			  </label>
			</div>
			<input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>
		  </form>
		</div>
		<div class="clear-float"></div>

		<div class="anmeldelser-maincontent"> <!--anmeldelser-maincontent-->
		  <div class="anmeldelser-left">
			<div class="container-freewall-a">
			  <div class="free-wall" id="freewalla">
				<?php print render($page['content']); ?>
			  </div>
			</div>
		  </div>

		  <div class="anmeldelser-right anmeldelser-latest">
			<div class="blog-sidegrid-item">
			  <h1>ANMELDELSER</h1>
			  <?php print render($page['anmeldelser_latest']); ?>
			</div>
		  </div>

		  <div class="anmeldelser-right anmeldelser-popular">
			<div class="blog-sidegrid-item">
			  <h1><?php print t('POPULÆRE KLINIKKER') ?></h1>
			<?php print render($page['popular_clinic']); ?>
			</div>
		  </div>
		</div> <!--anmeldelser-maincontent-->
	  </div> <!--lastminute-content-->
	  
	  <div class="clear-float"></div>

	  <div class="blogpost-footer">
		<div class="footer-nav">
		  <?php print render($page['footer_menu']); ?>
		</div>
		<div class="copyrights"><?php print render($page['copyrights']); ?></div>
	  </div> <!-- /.Same blog post Footer-->

	</div>
</div>

<?php if($device != "mobile") { ?>
<script type="text/javascript">
  jQuery.noConflict();
  jQuery(function($) {
    var walla = new freewall("#freewalla .view-content");
    walla.reset({
      selector: '.brick',
      animate: true,
      cellW: 320,
      cellH: 'auto',
	  gutterY: 10,
	  gutterX:  10,
      onResize: function() {
        walla.fitWidth();
      }
    });
    walla.fitWidth();
    walla.container.find('.brick img').load(function() {
      walla.fitWidth();
    });
  });
</script>
<?php } ?>

<?php if($device != "mobile") { ?>	
<script>
  jQuery(document).ready(function() {
    jQuery('.lastminut-form h2').hide();
    var stickyNavTop = jQuery('.lastminut-form').offset().top;

    var stickyNav = function() {
      var scrollTop = jQuery(window).scrollTop();

      if (scrollTop > stickyNavTop) {
        jQuery('.lastminut-form').addClass('highlighted');
        jQuery('.lastminut-form h2').show('slow');
      } else {
        jQuery('.lastminut-form').removeClass('highlighted');
        jQuery('.lastminut-form h2').hide('fast');
      }
    };

    stickyNav();

    jQuery(window).scroll(function() {
      stickyNav();
    });
  });
</script>


<script type="text/javascript">
	/*
   jQuery('#anmeld-form').submit(function (evt) {
    evt.preventDefault();
    var place = jQuery('#place').val();
    if (place == '' || place == 'Indtast by eller postnr.') {
      return false;
    } else {
      var category = jQuery('#select1 option:selected').val();
      var radius = jQuery('#select2 option:selected').val();
      var rating = jQuery('#select3 option:selected').val();
      var place = jQuery('#place').val();

      var distancefrom;
      var distanceto;
      var sorter;
      var unit = 6371;

      switch (radius) {
        case '0':
          distancefrom = 1;
          distanceto = 5;
          break;
        case '1':
          distancefrom = 5;
          distanceto = 10;
          break;
        case '2':
          distancefrom = 10;
          distanceto = 15;
          break;
        case '3':
          distancefrom = 15;
          distanceto = 20;
          break;
      }

      switch (rating) {
        case '2':
          sorter = 'field_clinic_loc_rating_rating';
          break;
        case '1':
          sorter = 'created';
          break;
      }

      var theUrl1 = "<?php echo $base_url; ?>/anmeldelser";
      var extraParameters1 = {
        'field_clinic_loc_category_tid': category,
        'field_geofield_distance[distance]': distancefrom,
        'field_geofield_distance[distance2]': distanceto,
        'field_geofield_distance[unit]': unit,
        'field_geofield_distance[origin]': place,
        'sort_by': sorter
      };
      window.location = getURL(theUrl1, extraParameters1);
    }
  });
  function getURL(theUrl, extraParameters) {
    var extraParametersEncoded = jQuery.param(extraParameters);
    var seperator = theUrl.indexOf('?') == -1 ? "?" : "&";
    return(theUrl + seperator + extraParametersEncoded);
  }
  */
</script>

	
<?php } ?>	


<script type="text/javascript">
	jQuery(document).ready(function() {
		
		jQuery( "#select1" ).change(function() {
		jQuery( "#edit-field-clinic-loc-category-tid" ).val(jQuery(this).val());
		jQuery( "#edit-field-clinic-loc-category-tid" ).trigger("change");
		});
		
		jQuery( "#select3" ).change(function() {
		jQuery( "#edit-sort-by" ).val(jQuery(this).val());
		jQuery( "#edit-sort-by" ).trigger("change");
		});
		
		
		jQuery( "#place" ).keyup(function() {
		jQuery( "#edit-field-geofield-distance-origin" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-origin" ).trigger("keyup");
		});
		
		jQuery( "#select2" ).change(function() {
		jQuery( "#edit-field-geofield-distance-distance2" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-distance2" ).trigger("keyup");
		
		
		});
		
	});		
		
</script>

<script>
    jQuery(function(){
        jQuery('.blog-categories .blog-selectbox .container-inline').removeClass('container-inline').addClass('container-inline1');
        jQuery('#select1').selectric();
        jQuery('#select2').selectric();
        jQuery('#select3').selectric();
});
</script>

<script type="text/javascript">
	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});



</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>

<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>

