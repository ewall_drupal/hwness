 <?php

/**
 * tpl used to display the comments of location on separate page
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
global $base_url;
 $term_data = $view->style_plugin->rendered_fields;
// echo '<pre>';print_r($term_data); exit;

if(!empty($term_data)) {
 
?>
	<?php foreach($term_data as $key => $value) {

     $iuid = $term_data[$key]['uid'];
     $cid = $term_data[$key]['cid'];
     

		 		$user_uid = $iuid;
		if(is_numeric($user_uid)){
			$iuid = $term_data[$key]['uid'];
		}else {
			$user_array = str_replace("<a href=\"","",$user_uid);
			$user_dts = explode(">",$user_array);
			$iuid = $user_dts[1];
		}
		
		 $iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('thumbnail', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}

    // $user_thumbnail_url = get_user_image($uid);

     $changed = $term_data[$key]['changed'];
     $subject = $term_data[$key]['subject'];
     $changed_1 = $term_data[$key]['changed_1'];
     $name = $term_data[$key]['name'];
     $comment_body = $term_data[$key]['comment_body'];
     $share_url = $base_url.'/location/comment/'.$term_data[$key][ 'cid'];
?>
        <div class="blogpost-left-content child-comments">
            <div class="comments-block">
			<div class="comments-section">
				<div class="comments-left">
					<div class="user-picture">
						<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$iuser->uid); ?>"><img src="<?php echo $user_thumbnail_url; ?>"></a>
					</div>
                    <div class="user-name">
						<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$iuser->uid); ?>"><?php echo $iuser->name; ?></a>
					</div>
					<div class="comment-date">
						<p><?php echo $changed ?> <span class="blog-post-time post-time"><?php echo $changed_1 ?></span></p>
					</div>
				</div>
				<div class="comments-right">
					<!-- <div class="comment-title"><?php echo $subject; ?></div> -->
                    <!-- <div id ="find-short-<?php echo $key; ?>" class="find-short comment-content">
						<p><?php echo $comment_body; ?></p>
						<p><a class="read-more" rel="<?php echo $key; ?>" href="javascript:void(0);"><?php echo t('Read more'); ?></a></p>
					</div> -->
					<div id ="find-long-<?php echo $key; ?>" class="find-long comment-content" style="display:block">
						<p><?php echo $comment_body; ?></p>
						<!-- <p><a class="read-less" rel="<?php echo $key; ?>" href="javascript:void(0);"><?php echo t('Less'); ?></a></p> -->
					</div>
					<!-- <div class="likes-rating">
						<div class="comment-likes">
							<?php print drupal_render(plus1_build_node_jquery_widget($term_data[$key]['cid'], $tag = 'plus1_comment_vote')); ?>
							<?php  // print like_widget_comment($cid); ?>
						</div>
					</div> -->
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
	</div>
        </div>
	<?php }
} else { ?>
<div class="blogpost-left-content child-comments">
    <p><?php echo t("No comments made yet.");?></p>
    </div>
<?php } ?>
