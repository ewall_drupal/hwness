<?php

/**
 * Listing the treatment categories with AJAX call from CBIT.module and ewall.js (Views: Location Categories (Term))
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$nid = arg(1);
$term_data = $view->style_plugin->rendered_fields;
?>

<ul class="og-grid" id="og-grid">
	<?php 
		foreach($term_data as $key => $value) {
            $title = $term_data[$key]['name'];
            $field_category_image = $term_data[$key]['field_category_image'];
            $tid = $term_data[$key]['tid'];
            $nodecount = getTermNodeCount($tid);

        ?>
            <li class="term<?php print $tid; ?>" rel="<?php print $tid; ?>">
                <div class="locate" class="locate<?php print $tid; ?>" rel="<?php print $tid; ?>" data="<?php print arg(1); ?>" >
                <a class="locate-img">
                       <img alt="img01" src="<?php print $field_category_image; ?>"></a>
                <div class="grid-item-01">
                    <h5><?php print $title; ?></h5>
                    <div class="treatment-categories">
                        <span class="count-left">Antal:&nbsp;<?php print _treatments_of_location($nid, $tid); ?></span>
                        <span class="count-right">se alle behandlinger  <img src="<?php echo base_path() . path_to_theme(); ?>/images/readmore.png"></span>
                    </div>
                </div>
                </div>

                <div style ="display:none" class="og-expander og-expander-<?php print $tid; ?>" style="transition: height 350ms ease 0s; height: 500px;">
                    <span rel="<?php print $tid ; ?>" class="close<?php print $tid; ?> og-closes"></span>
                </div>

            </li>
	<?php } ?>
</ul>

