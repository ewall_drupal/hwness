<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
// echo '<pre>';print_r($term_data);exit;
?>

<?php 
	foreach($term_data as $key => $value) {

        $iuid = $term_data[$key]['uid'];

						$user_uid = $iuid;
					if(is_numeric($user_uid)){
						$iuid = $term_data[$key]['uid'];
					}else {
						$user_array = str_replace("<a href=\"","",$user_uid);
						$user_dts = explode(">",$user_array);
						$iuid = $user_dts[1];
					}
					
		$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('home_profile', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}
		

       // $user_thumbnail_url = get_user_image($uid);

		$subject = $term_data[$key]['subject'];
		$comment_body = $term_data[$key]['comment_body'];
		$changed = $term_data[$key]['changed'];
		$changed_1 = $term_data[$key]['changed_1'];
		$name = $term_data[$key]['name'];
		$rating = $term_data[$key]['field_clinic_loc_rating'];


//		$picture = $term_data[$key]['field_enterprise_blog_picture'];
		$picture = $user_thumbnail_url;

		$cid = $term_data[$key]['cid'];
		$nid = $term_data[$key]['nid'];
		$uid = $term_data[$key]['uid'];
?>
	<li class="brick-comment-list views-row">
		<div class="grid-item-01">
			<!--first grid-->
			<div class="left-container">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$uid); ?>">
					<span class="image-container">
						<img src="<?php echo $picture; ?>"	style="border-radius:50%; width:44px; height:44px; top:0px; left:0px;">
					</span>
				</a>
				<span class="details-container"><?php echo $changed; ?>
					<span class="comment-time"><?php echo $changed_1; ?></span>
					<br>
					<span class="commenter-name"><?php echo $name; ?></span>
					<br>
					<span class="details-anmeldelser">
						<?php echo $rating; ?>
						<p class="comments"><a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$uid); ?>"><?php echo _user_anmeldelser_count($uid); ?> anmeldeser</a></p>
					</span>
				</span>
			</div>
			<div class="info-details">
				<p class="profile-total"><?php echo $subject; ?></p>
				<p><?php echo $comment_body; ?>
				<span class="readmore">
					<a href="<?php echo $base_url.'/location/comment/'.$cid; ?>">
						<img src="<?php echo base_path() . path_to_theme(); ?>/images/readmore.png">
					</a>
				</span> 
				</p>
			</div>
			<div class="comments-home">
				<?php //print drupal_render(plus1_build_node_jquery_widget($cid, $tag = 'plus1_comment_vote')); ?>
				<?php print like_widget_comment($cid);  ?>
				<a href="<?php echo $base_url.'/location/comment/'.$cid; ?>"><span class="comment"><?php echo _node_comment_count($nid); ?> kommentarer</span></a>
			</div>
		</div>
	</li>
<?php } ?>
