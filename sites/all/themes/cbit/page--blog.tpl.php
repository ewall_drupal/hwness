<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
?>
<?php 
	// $detect = mobile_detect_get_object();
	// $is_mobile = $detect->isMobile();
	$device = check_device();
?>

<link href="<?php echo base_path() . path_to_theme(); ?>/css/jquery.bxslider.css" rel="stylesheet" />
<?php if($device != "mobile") { ?>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/freewall.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>

<?php if($device != "mobile") { ?>
    <nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			   <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
<?php } ?>	

	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <!--<a href="#menu" class="menu-link">Menu</a>-->
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->
	
	<?php print $messages; ?>
	<div class="main-content-container container">
	
		  <?php if($device == "mobile") { ?>
		  <div class="logo">
			   <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>
		
		<div class="blog-header"><!--banner-->
			<h1>BLOG</h1>
		</div>
		<div class="clear-float"></div>
		<div class="blog">
			<div class="blog-container">
			
			<?php 
			global $user;
			$vipuser = $user->roles[5];
			if($vipuser){   ?>
			<div class="blog-grids addblogs">
				<?php  
				if($device != "mobile") {
					$options = array('attributes' => array('class' => 'form-submit'));
					echo l(t("Add Blog"),"add/blog", $options); 
				}
				?>
			</div>
		<?php	} ?>
				<div class="blog-grid"> 
					<?php 
					  //$block = module_invoke('views', 'block_view', '67ed6d423c9b7bb35384bd6041f1207e');
					  //print render($block['content']);
					?> 
					<?php if($device == "mobile") { ?>
					<div class="blog-categories">
						<div class="blog-selectbox">
							<label>
								<?php
									$jump_menu_block = module_invoke('views', 'block_view', 'treatment_category-block_3');
									print render($jump_menu_block['content']);
								?>
							</label>
						</div>
					</div> 
					<div id="freewalla" class="free-wall">
					<?php 
					  $block = module_invoke('views', 'block_view', '67ed6d423c9b7bb35384bd6041f1207e');
					  print render($block['content']); 
					  ?>
					  </div>
					<?php } else {
						print render($page['latest_all_blog']); 
					}?>
				</div>
				<?php if($device != "mobile") { ?>
				<div class="blog-rightgrid">
					<div class="blog-sidegrid">
						<div class="blog-sidegrid-item">
							<h1>mest populære</h1>
							<?php print render($page['popular_blog']); ?> 
						</div>
					</div>
					
					<div class="blog-categories">
						<div class="blog-selectbox">
							<h1>KATEGORIER</h1>
							<label>
								<?php
									$jump_menu_block = module_invoke('views', 'block_view', 'treatment_category-block_3');
									print render($jump_menu_block['content']);
								?>
							</label>
						</div>
					</div>
					
					<div class="post-sidegrid">
						<div class="blog-sidegrid-item">
							<h1>SENESTE INDLÆG</h1>
							<?php print render($page['blog_indlaeg']); ?> 
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="clear"></div>
			</div>

		</div>
		<div class="clear-float"></div>
		<?php if($device != "mobile") { ?>
		<div class="slider-background"><!-- slider-->
			<div class="container-freewall0a">
				<div class="title">
					<div class="heading">
					</div>
				</div>
			<h1 class="title-name">Mød vores wellness queens (& kings)</h1>
			</div>
			<div class="clear-float"></div>
			  <div class="slider">
				<!-- thumbnail slider -->
				<div class="container-fluid">
				  <div class="col-md-12sd">
					<div class="well-none">
					  <div id="myCarousel" class="carousel slide">
						<!-- Carousel items -->
						<div class="carousel-inner">
						  <?php
						  $block = module_invoke('views', 'block_view', 'wellness_queens_kings-block_1');
						  print render($block['content']);
						  ?>
						</div>
						<!--/carousel-inner-->
					  </div>
					  <!--/myCarousel-->
					</div>
					<!--/well-->
				  </div>
				</div>
				<!-- ./ thumbnail slider end -->
			  </div>
			  <div class="clear-float"></div>
		</div>
		<div class="section-02">
			<div class="container-freewall" style="padding-top:30px;">
				<div id="freewalla" class="free-wall">
					<?php print render($page['all_blog']); ?>
				</div>
				<div class="footer-nav">
				  <?php print render($page['footer_menu']); ?>
				</div>
				<div class="copyrights"><?php print render($page['copyrights']); ?></div>
			</div>
		</div><!-- /. section-02 background -->
		<?php } ?>
	</div>
 
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.bxslider.min.js"></script>
<script type="text/javascript">
  jQuery.noConflict();
  jQuery(function($) {
    jQuery('.slider4').bxSlider({
      minSlides: 4,
      maxSlides: 4,
      slideWidth: 320,
      //moveSlides: 4,
      slideMargin: 10,
      speed: 800
    });
  });
</script>

<?php if($device != "mobile") { ?>
	<script type="text/javascript">
		jQuery(function($) {
			var wall = new freewall("#freewalla .view-content");
			wall.reset({
				selector: '.brick',
				animate: true,
				cellW: 320,
				cellH: 'auto',
				gutterY: 10,
				gutterX: 10,
				onResize: function() {
					wall.fitWidth();
				}
			});
			wall.fitWidth();
			wall.container.find('.brick img').load(function() {
				wall.fitWidth();
			});
		});
	</script>
<?php } ?>

<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.blockUI.js"></script>
<script type="text/javascript">
	jQuery(function(){
		jQuery('.blog-categories .blog-selectbox .container-inline').removeClass('container-inline').addClass('container-inline1');
		jQuery('.page-blog .ctools-jump-menu-select').selectric();
	});
	
	jQuery(".menu-link").click(function(){
		jQuery("#menu").toggleClass("active");
		jQuery(".container").toggleClass("active");
	});
	

	
	jQuery(document).on("click",".down",function(){
		jQuery.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .8, 
            color: '#fff'
        } }); 
		jQuery(this).parent(".tonotshowinfo").css({'display':'none'});
		jQuery(this).parents(".brick").find(".toshowinfo").slideDown();
		jQuery(this).parents(".brick").find(".toshowup").show();
		setTimeout(function() {	jQuery.unblockUI(); }, 200);
	});
	
	jQuery(document).on("click",".up",function(){
		jQuery.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .8, 
            color: '#fff',
            fontSize: '12px' 
        } }); 
		jQuery(this).parents(".toshowup").css({'display':'none'});
		jQuery(this).parents(".brick").find(".toshowinfo").slideUp('fast');
		jQuery(this).parents(".brick").find(".tonotshowinfo").slideDown('fast');
		setTimeout(function() {	jQuery.unblockUI(); }, 200);
	});
	
	jQuery(document).on("click",".brick span.category a",function(){
		return false;
	});
</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>



<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>
