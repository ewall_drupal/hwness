<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
// echo "<pre>"; print_r($term_data); exit;
$device = check_device();

?>
<?php 
	foreach ($term_data as $key => $value) 
	{
		$iuid = $term_data[$key]['uid'];
		$nid = $term_data[$key]['nid'];
		$cid = $term_data[$key]['cid_1'];
        $comment_url = $base_url.'/'.drupal_get_path_alias('location/comment/' . $cid);
		$user_uid = $iuid;
		if(is_numeric($user_uid)){
			$iuid = $term_data[$key]['uid'];
		}
		else {
			$user_array = str_replace("<a href=\"","",$user_uid);
			$user_dts = explode(">",$user_array);
			$iuid = $user_dts[1];
		}		
			$iuser = user_load($iuid);
			$user_name = $iuser->name;
			$img_path = $iuser->picture;
		if($img_path){
			$img_path = $iuser->picture->uri;
			$images = substr($img_path,'9'); 
			$user_thumbnail_url = image_style_url('home_profile', $images );
		}
		else {
			$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}		
//$user_thumbnail_url = get_user_image($uid);
?>
	<div class="brick views-row">
		<div class="grid-item-01">
		  <!--first grid-->		  
				<div class="anmeldelser-headline">
					<div class="headline-container">
						<span class="anmeldelser-rating" style="padding-bottom: 5px;"><?php echo clinik_avg_rating($nid, 'widget'); ?></span>
						<div class="clear"></div>
						<h1><?php echo $term_data[$key]['field_location_custref']; ?></h1>
					</div>
				</div>
				<div class="item-01-info">
					<div class="anmeldelser-info-01">
						<div class="anmeldelser-info-details">
							<div class="anmeldelser-date">
								<div class="anmeldelser-page-left">
									<a title="View <?php echo ucfirst($term_data[$key]['name']); ?>'s page" href="<?php echo $base_url.'/'.drupal_get_path_alias('user/' . $term_data[$key]['uid']); ?>">
										<img align="left" src="<?php echo $user_thumbnail_url; ?>" style="border-radius:50%; width:45px; height:45px; margin-right:10px;">
									</a>
								</div>
								<div class="anmeldelser-page-right">
									<p>
										<span class="date"><?php echo $term_data[$key]['created']; ?></span> 
										<span class="time"><?php echo $term_data[$key]['created_1']; ?></span>
										<span class="clear"></span>
									</p>
									<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/' . $term_data[$key]['uid']); ?>">
										<span class="user-name"><?php echo $user_name ?></span>
									</a>
									<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/' . $term_data[$key]['uid']); ?>">
										<span class="user-review"><?php echo _user_anmeldelser_count($term_data[$key]['uid']); ?> anmeldelser</span>
									</a>
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="clear-float"></div>
					</div>
					<hr>
					
					<div class="clear-float"></div>
					
					<div class="anmeldelser-info-01">
						<div class="info-details">
							<h1 class="anmeldelser-info-content"><a target="_blank" href="<?php echo $comment_url; ?>"><?php echo $term_data[$key]['subject']; ?></a></h1>
							<?php 
								$detects = mobile_detect_get_object();
								$is_mobiles = $detects->isMobile();
							if($device == "mobile") { ?>
								<?php echo $term_data[$key]['comment_body_1']; ?>
							<?php } else { ?>
								<a target="_blank" href="<?php echo $comment_url; ?>"><?php echo $term_data[$key]['comment_body']; ?></a>
							<?php } ?>
						</div>
					</div>
					<hr>
				</div>
			 
			<div class="clear-float"></div>
			
			<div class="anmeldelser-comments-home">
                <?php print drupal_render(plus1_build_comment_jquery_widget($cid, $tag = 'plus1_comment_vote')); ?>
				<span class="comment">
					<a target="_blank" href="<?php echo $comment_url; ?>">
						<?php echo kommentarer_count($cid); ?> kommentarer
					</a>
				</span>
			</div>
		</div>
	</div>
<?php 
	} 
?>
