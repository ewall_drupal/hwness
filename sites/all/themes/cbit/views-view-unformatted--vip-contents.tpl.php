<?php
 $term_data = $view->style_plugin->rendered_fields;
// echo '<pre>';print_r($term_data);exit;
 global $base_url; 
?>

	
	<?php  
	foreach($term_data as $key => $value) {
		$categories = '';
		$title = $term_data[$key]['title'];
		$nid = $term_data[$key]['nid'];
		$body = $term_data[$key]['body'];
		$comment_count = $term_data[$key]['comment_count'];
		$created = $term_data[$key]['created'];
		$created_1 = $term_data[$key]['created_1'];
		$gallery = $term_data[$key]['field_image'];
		if($term_data[$key]['field_categories']){
			$categories = $term_data[$key]['field_categories'];
		}
		if($term_data[$key]['field_trend_category']){
			$categories = $term_data[$key]['field_trend_category'];
		}
		$blog_picture = $term_data[$key]['field_enterprise_blog_picture'];

        $iuid = $term_data[$key]['uid'];

						$user_uid = $iuid;
						if(is_numeric($user_uid)){
						$iuid = $term_data[$key]['uid'];
						}else {
						$user_array = str_replace("<a href=\"","",$user_uid);
						$user_dts = explode(">",$user_array);
						$iuid = $user_dts[1];
						}
						

        $iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$blog_picture = image_style_url('thumbnail', $images );
		}else {
		$blog_picture =  $term_data[$key]['field_enterprise_blog_picture'];
		}
		

	?>	<div class="brick">
			<div class="grid-item-01">
			<!--first grid-->
                <?php if($categories) { ?>
				<span class="label"><?php if($categories){ echo $categories; }?></span>
                <?php } ?>
                <a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$nid); ?>"><img src="<?php echo $gallery; ?>" width="100%"></a>
				<div class="item-01-info">
					<div class="actions">
						<a class="btn btn-secondary btn-queen" href="#">
                            <span class="clip_mask">
                                <img src="<?php echo $blog_picture; ?>"	style="border-radius:50%; width:44px; height:44px; top:0px; left:0px;">
                            </span>
                        </a>
					</div>
					<div class="info-01">
						<p class="info-title"><?php echo $created; ?> <span class=
						"text-01"><?php echo $created_1; ?></span></p>

						<p class="info-content"><?php echo $title; ?></p>
						<div class="info-details"><?php echo $body; ?><span class="readmore"><a href=
						"<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$nid); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/readmore.png"></a></span></div>
					</div>
				</div>
				<div class="comments-home">
					<?php echo drupal_render(plus1_build_node_jquery_widget($term_data[$key]['nid'], $tag = 'plus1_node_vote')); ?>
					<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$nid).'#comment-list'; ?>"><span class="comment"><?php echo $comment_count; ?> kommentarer</span></a>
				</div>
			</div>
		</div>	
	<?php } ?>
