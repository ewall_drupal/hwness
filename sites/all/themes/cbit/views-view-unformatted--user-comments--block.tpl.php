<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
global $user;

$device = check_device();

$dest = drupal_get_destination();
$destination = $dest['destination'];
$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();

if($device != "mobile") { 
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$term_datas = $view->style_plugin->rendered_fields;
// echo '<pre>';print_r($term_datas);exit;

if(!empty($term_datas)) {
foreach($term_datas as $key => $val) {
	// echo '<pre>';print_r($term_data);exit;
?>
<div class="profile-title">
				<span class="title-content">
					<h1><?php echo $term_datas[$key]['field_location_custref']; ?></h1>
					<?php echo $term_datas[$key]['field_clinic_loc_rating']; ?>
				</span>
				<span class="address">
				<p><?php echo $term_datas[$key]['field_location_address_thoroughfare']; ?></p>
				</span>
			</div>
			<div class="profile-content">
				<p class="profile-date"><?php echo $term_datas[$key]['changed']; ?></p>
				<p class="profile-total"><?php echo $term_datas[$key]['subject']; ?></p>
				<span class="content-details">
					<?php echo $term_datas[$key]['comment_body']; ?>
				</span>
				<div class="profile-icons">
					<span class="icon-like">
						<?php print drupal_render(plus1_build_comment_jquery_widget($term_datas[$key]['cid'], $tag = 'plus1_comment_vote')); ?>
					</span>
					<span class="icon-comments">
						<img src="<?php echo base_path() . path_to_theme(); ?>/images/comments.png" class="comments-icon"><?php echo $term_datas[$key]['comment_count']; ?> kommentarer
					</span>
					<span class="icon-del">
						<img src="<?php echo base_path() . path_to_theme(); ?>/images/del.png" class="del-icon" />Del
					</span>
					<?php if($term_datas[$key]['uid'] == $user->uid) { 
					$cid = $term_datas[$key]['cid'];
					?>
						<span class="icon-close">
							<a href="<?php echo base_path().'comment/'.$term_datas[$key]['cid'].'/delete-own?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/close.png" class="close-icon">Fjern</a>
						</span>
						<span class="icon-edit">
							<a href="<?php echo base_path().'comment/'.$term_datas[$key]['cid'].'/edit?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/edit.png" class="edit-icon">Rediger</a>
						</span>
					<?php } ?>
				</div>
			</div>
			
<?php
} } else {
	echo "No reviews made yet.";
} } else { // Mobile View

$term_datas = $view->style_plugin->rendered_fields;
// echo '<pre>';print_r($term_datas);exit;


if(!empty($term_datas)) {
foreach($term_datas as $key => $val) {
	// echo '<pre>';print_r($term_data);exit;
?>
<div class="profile-title">
				<span class="title-content">
					<h1><?php echo $term_datas[$key]['field_location_custref']; ?></h1>
					<?php echo $term_datas[$key]['field_clinic_loc_rating']; ?>
				</span>
			</div>
			<div class="profile-content">
				<p class="profile-date"><?php echo $term_datas[$key]['changed']; ?></p>
				<p class="profile-total">
				<?php if($term_datas[$key]['uid'] == $user->uid) { 
					$cid = $term_datas[$key]['cid'];
					?>
						<span class="review-title">
						<?php echo $term_datas[$key]['subject']; ?>
						</span>
						<span class="review-edit">
						<span class="icon-edit">
							<a href="<?php echo base_path().'comment/'.$term_datas[$key]['cid'].'/edit?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/edit.png" class="edit-icon">Rediger</a>
						</span>
						<span class="icon-close">
							<a href="<?php echo base_path().'comment/'.$term_datas[$key]['cid'].'/delete-own?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/close.png" class="close-icon">Fjern</a>
						</span>
						</span>
					<?php } else { ?>
					<span class="review-title">
						<?php echo $term_datas[$key]['subject']; ?>
						</span>
						<?php } ?>
					</p>
				<span class="content-details">
					<?php echo $term_datas[$key]['comment_body']; ?>
				</span>
				<div class="profile-icons">
					<span class="icon-like">
						<?php print drupal_render(plus1_build_comment_jquery_widget($term_datas[$key]['cid'], $tag = 'plus1_comment_vote')); ?>
					</span>
					<span class="icon-comments">
						<img src="<?php echo base_path() . path_to_theme(); ?>/images/comments.png" class="comments-icon"><?php echo $term_datas[$key]['comment_count']; ?> kommentarer
					</span>
				</div>
			</div>
			
<?php
} } else {
	echo "No reviews made yet.";
} } ?>
