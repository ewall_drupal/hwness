jQuery(document).ready(function () {
		jQuery(window).data('ajaxready', true);
		jQuery('input[type="submit"]').removeAttr('disabled');
    jQuery('li.first #slidediv').hide();
    jQuery('li.second #slidediv1').hide();
    jQuery('#slidediv1 input#edit-mailchimp-lists-mailchimp-drupal-mailchimp-mergevars-email').val('Tilmeld nyhedsbrev');
    jQuery( ".section-06 input.form-text" ).val('Indtast email adresse');

    jQuery( "#slidediv1 input.form-text" ).focusin(function() {
      if (jQuery(this).val() == 'Tilmeld nyhedsbrev') {
          jQuery(this).val('');
      }
    });
    jQuery( "#slidediv1 input.form-text" ).focusout(function() {
      if (jQuery(this).val() == '') {
          jQuery(this).val('Tilmeld nyhedsbrev');
      }
    });

    jQuery( ".section-06 input.form-text" ).focusin(function() {
      if (jQuery(this).val() == 'Indtast email adresse') {
          jQuery(this).val('');
      }
    });

    jQuery( ".section-06 input.form-text" ).focusout(function() {
      if (jQuery(this).val() == '') {
          jQuery(this).val('Indtast email adresse');
      }
    });

    /** add class to the body for the modal popup **/
    jQuery('body').addClass('modal-login');

    jQuery('li.usr-login a').click(function () {
        jQuery('body').removeClass('modal-login');
        jQuery('body').removeClass('modal-register');
        jQuery('body').addClass('modal-login');
    });
    
    jQuery('li.usr-logout a').click(function () {
        jQuery('body').removeClass('modal-login');
        jQuery('body').removeClass('modal-register');
        jQuery('body').addClass('modal-register');
    });
    
   /*** Onclick action on header top ***/
    jQuery("li.first img#btnleft").click(function() {
        jQuery('li.first #slidediv').toggle();
    });


    jQuery("li.second img#btnleft1").click(function() {
        jQuery('li.second #slidediv1').toggle();
    });
    /*** Onclick action on header top ***/

    /** Readmore toggle on comment listing ***/
    jQuery(".read-more").click(function(){
        var rel = jQuery(this).prop('rel');
        jQuery("#find-short-"+rel).hide();
        jQuery("#find-long-"+rel).fadeIn("slow");
    });

    jQuery(".read-less").click(function(){
        var rel = jQuery(this).prop('rel');
        jQuery("#find-long-"+rel).hide();
        jQuery("#find-short-"+rel).fadeIn("slow");
    });
    /** Readmore toggle on comment listing ***/

    /*** Comment form toggle **/
    jQuery(".review").click(function(){
        jQuery("#comment-form").slideToggle();
    });
    /*** Comment form toggle **/

	var q = true;
    var current = 0;
    
	jQuery(document).on("click",".locate",function(){
        jQuery(".og-grid li").css({'transition':'height 350ms ease 0s','height':'auto'});
		jQuery(".og-expander" ).hide("fast");
		jQuery("#og-grid li" ).removeClass( "og-expanded" );
		var term_uid =  jQuery(this).attr('rel');
		// alert(term_uid);
		var loc_id =  jQuery(this).attr('data');
		
		jQuery(this).parent().css({'transition' : 'height 350ms ease 0s','height' : '808px'});
		jQuery(".term" +term_uid +" .og-expander").css({'height':'402px','transition':'height 350ms ease 0s'});
		jQuery( ".term" +term_uid +" .og-expander" ).slideToggle("slow");
		jQuery(this).parent().toggleClass("og-expanded");
		if (jQuery(this).parent().hasClass('og-expanded')) {
            if (jQuery(window).data('ajaxready') === false)
            return;
            jQuery(window).data('ajaxready', false);
            jQuery(".term" +term_uid +" .og-expander-inner" ).remove();
			get_user_detailss(term_uid,loc_id);
		} else {
			jQuery(".term" +term_uid +" .og-expander-inner" ).remove();
			jQuery(this).parent().css({'transition':'height 350ms ease 0s','height':'auto'});
		}
		
	});
    
	jQuery(".og-closes").click(function (event) {            
		q=true;
		var close_uid =  jQuery(this).attr('rel');
		jQuery(".og-expander").hide();
		jQuery("#og-grid li" ).removeClass( "og-expanded" );
		// jQuery(".term" +close_uid).css("height", "402px" );
        jQuery(".og-grid li").css({'transition':'height 350ms ease 0s','height':'auto'});
		
	});
    
    jQuery('#subcomment-form').submit(function (e) {
        e.preventDefault(); 
				jQuery('input[type="submit"]').attr('disabled','disabled');
				jQuery('#subcomment-form .throbber').show();
        var comment = jQuery('#subcomment-form textarea').val();
        var pcid = jQuery('input#pcid').val();
        var nid = jQuery('input#nid').val();
        if(comment == '') {
            alert("Please fill the comment section");
            return false;
        }
        jQuery.ajax({
            type: "POST",
            url: Drupal.settings.basePath + 'postsubcomment',
            dataType: 'json',
            data: {
                'comment': comment,'pcid':pcid,'nid':nid
            },
            success: function (json) {
                location.reload();
            }
        });
    });
     

    jQuery('.add-sub-comment-link').click(function(){
    jQuery('.add-sub-comment-area').slideToggle();
    });
    
	
	function get_user_detailss(term_uid,loc_id){
        
        var term_node_id = term_uid;
        var result;

        jQuery.ajax({
            type: "POST",
            url: Drupal.settings.basePath + 'locationterm',
            dataType: 'json',
            data: {
                'term_node_id': term_node_id,'loc_id':loc_id
            },
            success: function (json) {
            result = json.data;
            if(result){
                jQuery("#og-grid li.term"+term_node_id+" .og-expander .og-closes").after(result);
            }
            jQuery(window).data('ajaxready', true);
            }
        });
	}
    

});

