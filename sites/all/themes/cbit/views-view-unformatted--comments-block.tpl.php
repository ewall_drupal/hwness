<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
//echo '<pre>'; print_r($term_data); exit;
?>

<?php foreach($term_data as $key => $value){
	$subject = $term_data[$key]['subject'];
	$comment_body = $term_data[$key]['comment_body'];
	$nid = $term_data[$key]['nid'];
	$name = $term_data[$key]['name'];
	$user_picture = $term_data[$key]['field_enterprise_blog_picture'];
	$rating = $term_data[$key]['field_clinic_loc_rating'];
	$comment_count = $term_data[$key]['comment_count'];
	$changed = $term_data[$key]['changed'];
	$changed_1 = $term_data[$key]['changed_1'];

    $iuid = $term_data[$key]['uid'];

				$user_uid = $iuid;
		if(is_numeric($user_uid)){
			$iuid = $term_data[$key]['uid'];
		}else {
			$user_array = str_replace("<a href=\"","",$user_uid);
			$user_dts = explode(">",$user_array);
			$iuid = $user_dts[1];
		}
		
		 $iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('thumbnail', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}

    //$user_thumbnail_url = get_user_image($uid);

?>
	<div class="comments-block">
			<div class="comments-section">
				<div class="comments-left">
					<div class="user-picture">
						<img src="<?php echo $user_thumbnail_url; ?>">
					</div>
					<div class="comment-date">
						<p><?php echo $changed ?> <div class="blog-post-time post-time"><?php echo $changed_1 ?></div></p>
					</div>
					<div class="user-name">
						<?php echo $name; ?>
					</div>
				</div>
				<div class="comments-right">
					<div class="comment-title"><?php echo $subject; ?></div>
					<div class="comment-content">
						<?php echo $comment_body; ?>
					</div>
					<div class="likes-rating">
						<div class="comment-likes">
							<?php echo like_widget_node($nid); ?>
						</div>
						<div class="comment-rating">
							<?php echo $rating; ?>
						</div>
					</div>
				</div>	
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
	</div>
<?php } ?>
