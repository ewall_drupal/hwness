<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$term_datas = $view->style_plugin->rendered_fields;
// echo '<pre>';print_r($term_datas);exit;
?>
<?php
foreach($term_datas as $key=> $term_data) {
	//echo '<pre>';print_r($term_data);exit;
	$user_details = user_load($term_datas[$key]['uid_1']);
	//echo '<pre>';print_r($user_details);exit;
?>
<?php if($key == 2){ ?>
<div class="brick brick-overflow wellness_bloggere-block">
	<div class="grid-item-02">
		<div class="grid2-info wellness wellness_bloggere">
			<?php 
				$block = module_invoke('block', 'block_view', '2');
				print render($block['content']);
			?>
		</div>
	</div>
</div>
<?php } ?>
<div class="brick brick-overflow views-row">
	<div class="grid-item-02">
		<!--first grid-->
		<a href='<?php echo $base_url.'/user/'.$term_datas[$key]['uid_1']; ?>'><img src="<?php echo $term_datas[$key]['field_wellness_queen_kings_image']; ?>" width="100%"></a>
		<?php if($term_datas[$key]['field_vip_category']){ ?>
			<span style="background-color : <?php echo $term_datas[$key]['field_vip_color'] ?>" class="wellness-label"><?php echo $term_datas[$key]['field_vip_category']; ?></span>
		<?php } ?>
		<div class="grid2-info wellness">
			<a class="queen-king-title-link" href='<?php echo $base_url.'/user/'.$term_datas[$key]['uid_1']; ?>'>
				<h3>
					<?php echo $term_datas[$key]['field_enterprise_blog_firstname']; ?><br>
					<?php echo $term_datas[$key]['field_enterprise_blog_lastname']; ?>
				</h3>
			</a>
			<span class="blog"><?php echo _user_node_count($term_datas[$key]['uid_1']); ?> BLOGINDLÆG</span><br>
			<span class="followers"><?php echo _followers_count($term_datas[$key]['uid_1']); ?> Følgere</span>

			<div class="follow">
				<?php if ($user->uid == 0) { ?>
					<span class="follow-title anonymous"><?php echo l('FØLG MIG', 'ajax_register/login/nojs', $options); ?></span>
				<?php } else { ?>
					<span class="follow-title anonymous"><?php print flag_create_link('follow', $term_datas[$key]['uid_1']); ?></span>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php
}
?>
