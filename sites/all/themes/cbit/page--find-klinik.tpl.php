<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$kategori_options = active_comment_terms();

// Checks the device whether its mobile/tablet/desktop
$device = check_device();
?>

<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/freewall.js"></script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>


<?php if($device != "mobile") { ?>
    	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			   <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
	<?php } ?>		
	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <!--<a href="#menu" class="menu-link">Menu</a>-->
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->
	
<?php print $messages; ?>
	<div class="main-content-container container">
	
			<?php if($device == "mobile") { ?>
		  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>
	
    <div class="findklinik-container"><!-- main content -->

	<div class="findklinik-header"><!--header-->
		<h1>Find Klinik</h1>
		<h5>Søg efter klinikker</h5>
	</div>
	<div class="clear-float"></div>
	
	<div class="findklinik-content">
		
		<div class="findklinik-form" style="height:90px;">
			<form id="klinik-form">
				<div class="findklinik-selectbox select-cat">
					<h4>Vælg behandling</h4>
					<label>
						<select id ="select1">
						  <?php echo $kategori_options; ?>
						</select>
					</label>
				</div>
				<div class="findklinik-selectbox">
					<h4>Sted</h4>
					<input id="place" type="text" value="Indtast by eller postnr." onfocus="if(this.value == 'Indtast by eller postnr.') { this.value = '';}">
				</div>
				<div class="findklinik-selectbox select-radius">
					<h4>Område radius</h4>
					<label>
						<select id ="select2">
								<option value="5">0-5 km</option>
								<option value="10">0-10 km</option>
								<option value="15">0-15 km</option>
								<option value="20">0-20 km</option>
						</select>
					</label>
				
				</div>
				 <!--<input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>-->
			</form>
		</div>
		
		<div class="clear-float"></div>
		<?php print render($page['content']); ?>
		<div class="try-search view-empty">
			<p><?php echo t('Try out the Search'); ?></p>
		</div>
	</div>
	
	
		<div class="blogpost-footer">
			<div class="footer-nav">
			  <?php print render($page['footer_menu']); ?>
			</div>
			<div class="copyrights"><?php print render($page['copyrights']); ?></div>
		  </div> <!-- /.Same blog post Footer-->

	</div>
	</div><!-- /. main content -->



    <script>
		jQuery(document).ready(function(){
			jQuery(document).on("click",".read-more",function(){
				//alert("read more");
				var rel = jQuery(this).prop('rel');
					jQuery("#find-short-"+rel).hide();
					jQuery("#find-long-"+rel).fadeIn("slow");	
				});
			jQuery(document).on("click",".read-less",function(){
				//alert("read less");
				var rel = jQuery(this).prop('rel');
					jQuery("#find-long-"+rel).hide();
					jQuery("#find-short-"+rel).fadeIn("slow");		
				});
			});
	</script>

<?php if($device != "mobile") { ?> 
    <script>
	  jQuery(document).ready(function() {
			jQuery( ".view-content" ).hide();
			jQuery( ".item-list" ).hide();

			jQuery( "#select1" ).change(function() {
				jQuery( "#edit-field-clinic-loc-category-tid" ).val(jQuery(this).val());
				jQuery( "#edit-field-clinic-loc-category-tid" ).trigger("change");
				jQuery( ".try-search" ).hide();
				jQuery( ".view-content" ).show();
				jQuery( ".item-list" ).show();
			});

			jQuery( "#place" ).keyup(function() {
				jQuery( "#edit-field-geofield-distance-origin" ).val(jQuery(this).val());
				jQuery( "#edit-field-geofield-distance-origin" ).trigger("keyup");
				jQuery( ".try-search" ).hide();
				jQuery( ".view-content" ).show();
				jQuery( ".item-list" ).show();
			});

			jQuery( "#select2" ).change(function() {
				jQuery( "#edit-field-geofield-distance-distance2" ).val(jQuery(this).val());
				jQuery( "#edit-field-geofield-distance-distance2" ).trigger("keyup");
				jQuery( ".try-search" ).hide();
				jQuery( ".view-content" ).show();
				jQuery( ".item-list" ).show();
			});
			
			headHeight = jQuery('nav.header').height() - 3;
			var scrolled = false;
			stickyNavTop = jQuery('.findklinik-form').offset().top;
			stickyNav = function(hg){
				// console.log(hg);
				//headHeight = jQuery('nav.header').height() - 3;
				scrollTop = jQuery(window).scrollTop();
				if (scrollTop > stickyNavTop) { 
					jQuery('.findklinik-form').addClass('highlighted');
					jQuery('.findklinik-form').animate({top:hg},'slow');
				} else {
					jQuery('.findklinik-form').removeAttr('style');
					jQuery('.findklinik-form').removeClass('highlighted'); 
				}
			}; 
			
			stickyNavAlt = function(hg){
				console.log('alt: '+hg);
				scrollTop = jQuery(window).scrollTop();
				if (scrollTop > stickyNavTop) { 
					jQuery('.findklinik-form').addClass('highlighted');
					jQuery('.findklinik-form').animate({top:hg},'slow');
				} else {
					jQuery('.findklinik-form').removeAttr('style');
					jQuery('.findklinik-form').removeClass('highlighted'); 
				}
			}; 
		
		stickyNav(headHeight);
		
		var idCounter1 = 0;
		jQuery(window).scroll(function() {
				//alert(headHeight);
				stickyNav(headHeight);
			});
		

		jQuery(window).on('resize', function() {
			topVal = jQuery('.findklinik-container .findklinik-form').css('top');
			topVal = topVal.replace("px", "");
			console.log('topVal: '+topVal);
				headHeight = jQuery('nav.header').height() - 3;
				console.log('headHeight: '+headHeight);
				if(headHeight != topVal) {
					console.log('if: '+headHeight);
					if(jQuery('.findklinik-form').hasClass('highlighted')) {
						jQuery('.findklinik-form').css({'top':headHeight});
						jQuery('.findklinik-form.highlighted').css({'top':headHeight});
						/*var findklinik-form = document.querySelectorAll(".findklinik-form"); 
						for (var i = 0; i < findklinik-form.length; i++) {
								findklinik-form[i].style.top = headHeight;
						}
						var findklinik-form-highlighted = document.querySelectorAll(".findklinik-form.highlighted"); 
						for (var i = 0; i < findklinik-form-highlighted.length; i++) {
								findklinik-form-highlighted[i].style.top = headHeight;
						}*/
						console.log('mod top: '+jQuery('.findklinik-container .findklinik-form').css('top'));
						console.log('css top: '+headHeight);
					}
				}

		});
		
		
	  });
		
		// jQuery(document).ready(function() {
			// rs_headHeight = 0;
			// headHeight = jQuery('nav.header').height() - 3;
			// var scrolled = false;
			// stickyNavTop = jQuery('.findklinik-form').offset().top;
			// stickyNav = function(hg){
				//alert(hg);
				//headHeight = jQuery('nav.header').height() - 3;
				// scrollTop = jQuery(window).scrollTop();
				// if (scrollTop > stickyNavTop) { 
					// jQuery('.findklinik-form').addClass('highlighted');
					// jQuery('.findklinik-form.highlighted').animate({top:hg},'slow');
				// } else {
					// jQuery('.findklinik-form.highlighted').removeAttr('style');
					// jQuery('.findklinik-form').removeClass('highlighted'); 
				// }
			// }; 
			
			//stickyNav(headHeight);

			// jQuery(window).scroll(function() {
				// if(rs_headHeight != 0) {
					// headHeight = rs_headHeight;
				// }
				// stickyNav(headHeight);
				
			// });
			
   //});
		
		// var idCounter = 0;
		// jQuery(window).on('resize', function() {
			
					// jQuery('.findklinik-form.highlighted').removeAttr('style');
					// jQuery('.findklinik-form').removeAttr('style');
					// jQuery('.findklinik-form').removeClass('highlighted');
			// var myId=(++idCounter);
			// setTimeout(function(){
				// if(myId===idCounter){
					// rs_headHeight = jQuery('nav.header').height() - 3; 
					// stickyNav(rs_headHeight);
				// }
			// }, 500); // 500 milli the user most likely wont even notice it
		// });
		
		
			
	</script>
	
<?php } else  { ?> 	

 <script>
	  jQuery.noConflict();
	  jQuery(document).ready(function() {
		  jQuery( ".view-content" ).hide();
		  jQuery( ".item-list" ).hide();
		jQuery('.findklinik-form h2').hide();
	

		
		
		jQuery( "#select1" ).change(function() {
		jQuery( "#edit-field-clinic-loc-category-tid" ).val(jQuery(this).val());
		jQuery( "#edit-field-clinic-loc-category-tid" ).trigger("change");
		jQuery( ".try-search" ).hide();
		jQuery( ".view-content" ).show();
		jQuery( ".item-list" ).show();
		});
		
		jQuery( "#place" ).keyup(function() {
		jQuery( "#edit-field-geofield-distance-origin" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-origin" ).trigger("keyup");
		jQuery( ".try-search" ).hide();
		jQuery( ".view-content" ).show();
		jQuery( ".item-list" ).show();
		});
		
		jQuery( "#select2" ).change(function() {
		jQuery( "#edit-field-geofield-distance-distance2" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-distance2" ).trigger("keyup");
		jQuery( ".try-search" ).hide();
		jQuery( ".view-content" ).show();
		jQuery( ".item-list" ).show();
		});
		
	  });
	</script>


<?php } ?>

<script type="text/javascript">
 /*
   jQuery('#klinik-form').submit(function (evt) {
        evt.preventDefault();
       var place = jQuery('#place').val();
		if (place == '' || place == 'Indtast by eller postnr.') {
		  return false;
		} else {
		  var category = jQuery('#select1 option:selected').val();
		  var radius = jQuery('#select2 option:selected').val();
		  var place = jQuery('#place').val();

		  var distancefrom;
		  var distanceto;
		  var sorter;
		  var unit = 6371;

		  switch (radius) {
			case '0':
			  distancefrom = 1;
			  distanceto = 5;
			  break;
			case '1':
			  distancefrom = 5;
			  distanceto = 10;
			  break;
			case '2':
			  distancefrom = 10;
			  distanceto = 15;
			  break;
			case '3':
			  distancefrom = 15;
			  distanceto = 20;
			  break;
		  }
		  
		  var theUrl1 = "<?php echo $base_url; ?>/find-klinik";
		  var extraParameters1 = {
			'field_clinic_loc_category_tid': category,
			'field_geofield_distance[distance]': distancefrom,
			'field_geofield_distance2[distance2]': distanceto,
			'field_geofield_distance[unit]': unit,
			'field_geofield_distance[origin]': place
		  };
		  window.location = getURL(theUrl1, extraParameters1);
		}
    });
    function getURL(theUrl, extraParameters) {
		var extraParametersEncoded = jQuery.param(extraParameters);
		var seperator = theUrl.indexOf('?') == -1 ? "?" : "&";
		return(theUrl + seperator + extraParametersEncoded);
	  }
		*/
		
</script>

<script>
    jQuery(function(){
        jQuery('.blog-categories .blog-selectbox .container-inline').removeClass('container-inline').addClass('container-inline1');
        jQuery('#select1').selectric();
        jQuery('#select2').selectric();
});

	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});
	

	
</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>

<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>
