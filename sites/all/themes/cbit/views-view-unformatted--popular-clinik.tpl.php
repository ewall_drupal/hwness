<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
//echo '<pre>';print_r($term_data);exit;
global $base_url;
?>
<?php foreach ($term_data as $key => $value) { ?>
<?php
	// $node_details = node_load($term_data[$key]['nid']);
	// $path = $node_details->field_location_image['und'][0]['uri'];
	// $thumbnail_url = image_style_url('popular', $path);
	$thumbnail_url =$term_data[$key]['field_location_image'];
	//echo "<pre>";print_r($node_details);exit;
?>
	<div class="item-popular">
		<div class="anmeldelser-popular-img">
			<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>">
				<img src="<?php echo $thumbnail_url ?>">
			</a>
		</div>

		<div class="anmeldelser-popular">
		  <h2>
			  <a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>">
				<?php echo $term_data[$key]['title'] ?>
			  </a>
		  </h2>

		  <h3>
			  <a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']).'#tab2' ?>">
				<?php echo $term_data[$key]['comment_count'].' anmeldelser' ?>
			  </a>
		  </h3>
		  <?php //echo $term_data[$key]['field_clinic_loc_rating'] ?>
		  <?php echo clinik_avg_rating($term_data[$key]['nid'], 'widget'); ?>
		</div>
	</div>
<?php } ?>
