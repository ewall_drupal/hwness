<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
	global $base_url;
	$term_data = $view->style_plugin->rendered_fields;
	//echo '<pre>';print_r($term_data);exit;
 
?>

<div class="slider4">
	<?php foreach($term_data as $key => $value){ 
		$tid = $term_data[$key]['tid'];
		$nodecount = _get_treatment_count($tid);
	?>
		<a href="<?php echo $base_url.'/last-minute/'.$tid; ?>">
			<div class="slide">
				<img src="<?php echo $term_data[$key]['field_category_image']; ?>" width="100%">
				<div class="carousel-info">
					<h1><?php echo $term_data[$key]['name']; ?></h1>									
					<span class="number-treatments">Antal: <?php echo $nodecount; ?></span> 
					<span class="number-treatments view-treatment">Se alle behandlinger</span>
				</div>
			</div>	
		</a>
	<?php } ?>
</div>	

