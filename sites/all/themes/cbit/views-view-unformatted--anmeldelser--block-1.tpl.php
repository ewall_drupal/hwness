<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
//echo '<pre>';print_r($term_data);exit;
?>

<?php
foreach ($term_data as $key => $value) {
  if ($term_data[$key]['field_category_image'] == '') {
    $image = base_path() . path_to_theme() . '/images/default-recent.jpg';
  } else {
    $image = $term_data[$key]['field_category_image'];
  }
  ?>
  <div class="item-popular">
    <div class="popular-details">
      <div class="popular-date" style="float:left;"><a href="<?php echo $base_url.'/'.drupal_get_path_alias('location/comment/' . $term_data[$key]['cid']); ?>"><?php echo $term_data[$key]['changed']; ?></a></div>
      <div class="list-rating"><?php echo $term_data[$key]['field_clinic_loc_rating']; ?></div>
		<div class="anmeldelses-list">
		  <a href="<?php echo $base_url.'/'.drupal_get_path_alias('location/comment/' . $term_data[$key]['cid']); ?>">
			<?php echo $term_data[$key]['comment_body']; ?>
		  </a>
		</div>
    </div>
    <div class="popular-image">
		<a href="<?php echo $base_url.'/'.drupal_get_path_alias('location/comment/' . $term_data[$key]['cid']); ?>">
			<img width="80" height="70" src="<?php echo $image; ?>">
		</a>
	</div>
  </div>
<?php } ?>
