<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$arg_1 = arg(1);
$vid = 6;
$kategori = taxonomy_get_tree($vid);
$kategori_options = '<option value="Any">Vælg kategori</option>';
foreach ($kategori as $key => $val) {
	if($arg_1 == $kategori[$key]->tid) {
		$kategori_options .= '<option selected value="' . $kategori[$key]->tid . '">' . $kategori[$key]->name . '</option>';
	} else {
		$kategori_options .= '<option value="' . $kategori[$key]->tid . '">' . $kategori[$key]->name . '</option>';
	}
}
?>

<?php 
	/* $detect = mobile_detect_get_object();
	$is_mobile = $detect->isMobile(); */
	$device = check_device();
?>

<?php if($device == "mobile-tab") { ?>
    <link href="<?php echo base_path() . path_to_theme(); ?>/css/ipad.css" rel="stylesheet" />
<?php } ?>

<link href="<?php echo base_path() . path_to_theme(); ?>/css/humanity/jquery-ui.css" rel="stylesheet">
<link href="<?php echo base_path() . path_to_theme(); ?>/css/lastmint-m.css" rel="stylesheet">
  <script src="<?php echo base_path() . path_to_theme(); ?>/js/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>

<?php if($device != "mobile") { ?>

  	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			   <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
	<?php } ?>
	<?php print $messages; ?>
	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <!--<a href="#menu" class="menu-link">Menu</a>-->
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET PROFIL', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			 <!--<li><a title="" href="<?php //echo base_path(); ?>/last-minute">OPRET PROFIL</a></li>-->
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->	
		
<div class="main-content-container container">

		<?php if($device == "mobile") { ?>
		  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>

  <div class="lastminute-container">
	<div class="backdrop" style="opacity: 0; display: none;"></div>
    <!-- main content -->
    <div class="lastminute-header">
      <!--header-->
      <h1>last minute</h1>
      <h5>Søg efter last-minute wellness tilbud</h5>
    </div>

    <div class="clear-float"></div>
    <div class="lastminute-content">
	  <?php //if($is_mobile) { ?>
      <div class="lastminut-form lastminute-sticky">
        <form>
          <div class="lastminute-selectbox day">
			<h4>Vælg dag</h4>
			<div class="last-min-date">
				<input id="datepicker" style="width:145px;" type="text">
			</div>
          </div>
        <!--
          <div class="lastminute-selectbox for">
				<h4>Til</h4>
				<select class="select-category" style="width:125px;">
					<option selected>
						Kvinde
					</option>
					<option>
						Mand
					</option>
					<option>
						Par
					</option>
				</select>
          </div>
        -->
          <div class="lastminute-selectbox radius">
            <h4>Område radius</h4>
				<select class="select-distance" style="width:125px;">
				  <option selected>
					0-5 km
				  </option>
				  <option>
					5-10 km
				  </option>
				  <option>
					15-20 km
				  </option>
				</select>
          </div>

          <div class="lastminute-selectbox treatment">
				<h4>Vælg behandling</h4>
				<select class="select-rating" style="width:155px;" id ="select1">
					<?php echo $kategori_options; ?>
				</select>
          </div>

          <div class="lastminute-selectbox location">
			<?php if($device == "mobile") { ?>
				<h4>Område</h4>
			<?php } else { ?>
				<h4>Sted</h4>
            <?php } ?>
			<input onfocus="if(this.value == 'Indtast by eller postnr.') { this.value = '';}" type="text" value="Indtast by eller postnr.">
          </div>

          <div class="clear"></div>

          <div class="lastminute-time">
            <div class="left time-label">
              <span>Vælg dit/dine ønskede tidspunkter</span><input class=" left btn-book" onclick="location.reload();" type="submit" value="NULSTIL">
            </div>

            <div class="right">
              <div class="left time-section">
                <ul class="list">
                  <li><input type="checkbox"><label class="check-label">08 - 11</label></li>
                  <li><input type="checkbox"><label class="check-label">11 - 14</label></li>
                  <li><input type="checkbox"><label class="check-label">14 - 17</label></li>
                  <li><input type="checkbox"><label class="check-label">17 - 20</label></li>
                  <li><input type="checkbox"><label class="check-label">20 - 23</label></li>
                </ul>
              </div>
              <?php if($device == "mobile"){ ?>
					<input class=" left btn-book lastsub" onclick="location.reload();" type="submit" value="Find ledige tider">
			  <? } ?>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </form>
      </div>
			<div class="temp-form-last-min" style="height:160px; width:auto; clear: both; display: none;">&nbsp;</div>
	<?php //} ?>		

<?php if($device != "mobile") {  /*?>
      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>
            <a href="<?php echo $base_url; ?>/content/verto"><h1>Cha Cha Cha</h1>
            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>
            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint1">
            <h6>BEHANDLINGSTYPE</h6>
            <h1>Strip-voks</h1>
            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>
            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>
            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>
            <div class="clear-float"></div>
          </div>
        </div>
    </div>

      <div class="clear-float"></div><!--second block star-->

      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>

            <a href="<?php echo $base_url; ?>/content/obruo"><h1>Tmit</h1>

            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>

            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint2">
            <h6>BEHANDLINGSTYPE</h6>

            <h1>Strip-voks</h1>

            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>

            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>

            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>

            <div class="clear-float"></div>
          </div>
        </div>
      </div>

      <div class="clear-float"></div><!--third block star-->

      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>

            <a href="<?php echo $base_url; ?>/content/antehabeo-torqueo"><h1>Clinique KAVI Hudpleje</h1>

            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>

            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint3">
            <h6>BEHANDLINGSTYPE</h6>

            <h1>Strip-voks</h1>

            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>

            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>

            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>

            <div class="clear-float"></div>
          </div>
        </div>
      </div>

      <div class="clear-float"></div><!--fourth block star-->

      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>

            <a href="<?php echo $base_url; ?>/content/verto"><h1>Cha Cha Cha</h1>

            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>

            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint4">
            <h6>BEHANDLINGSTYPE</h6>

            <h1>Strip-voks</h1>

            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>

            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>

            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>

            <div class="clear-float"></div>
          </div>
        </div>
      </div>

      <div class="clear-float"></div><!--five block star-->

      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>

            <a href="<?php echo $base_url; ?>/content/obruo"><h1>Tmit</h1>

            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>

            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint5">
            <h6>BEHANDLINGSTYPE</h6>

            <h1>Strip-voks</h1>

            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>

            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>

            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>

            <div class="clear-float"></div>
          </div>
        </div>
      </div>

      <div class="clear-float"></div><!--six block star-->

      <div class="lastminut-block">
        <div class="lastmint-item">
          <div class="lastmint-product">
            <h6>STED</h6>

            <a href="<?php echo $base_url; ?>/content/antehabeo-torqueo"><h1>Clinique KAVI Hudpleje</h1>

            <p>Kompagnistræde 15 st.<br>
            208 København K<br>
            +45 5050 2660</p></a>

            <!--<div class="lasatmint-rating"><img src="<?php //echo base_path() . path_to_theme(); ?>/images/stars.png"></div>-->
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-details lastmint6">
            <h6>BEHANDLINGSTYPE</h6>

            <h1>Strip-voks</h1>

            <p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
            <a class="read-more lightbox">Læs mere</a>
			<div style="opacity: 0; display: none;" class="box">
				<div class="close"><img src="<?php echo base_path().path_to_theme().'/images/close-button.png' ?>" alt="img" title="img" /></div>
				<div class="more-info">
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
					<p>Strip-voks er den nok metode de fleste forbinder med voksning, og forgår ved at voks påføres og herefter fjernes med en kraftig pairstrip. Til strip-voks anvender vi hovedsageligt hygiejnisk 100% ren bivoks. Strip-voks er hurtig og nemt til dig der gerne vil have tingen overstået kvikt og effektivt.</p>
				</div>
			</div>
          </div>
        </div>

        <div class="lastmint-item">
          <div class="lastmint-price">
            <p>Dato: 26.03.2014. Tid:125 min</p>

            <h1>Pris: 250 kr.</h1><span class="timer">08 - 11</span> <span class="timer">17 - 20</span><br>
            <input class="btn-book" type="submit" value="SE TIDER &amp; BOOK">
            <hr>

            <div class="price-inf0">
              <span class="inf0">Værdi 550,-</span> <span class="inf0">Rabat 50%</span> <span class="inf0">Spar 275,-</span>
            </div>

            <div class="clear-float"></div>
          </div>
        </div>
      </div>
    </div><!--Same blog post Footer-->

    <div class="clear-float"></div>
	<?php   */ } ?>	
        

        
<div class="clear-float"></div>
<!-- /******************************** MOBILE MENU START *********************************/	   --->	


<?php if($device != "mobile") {  ?>
		<div class="lastminut-block">
             <?php print render($page['last_minute_desc']); ?>
		</div>
        		
		<div class="clear-float"></div>

<?php  }  ?>

<!-- /******************************** MOBILE MENU END *********************************/	   --->	
    <div class="blogpost-footer">
      <div class="footer-nav">
        <?php print render($page['footer_menu']); ?>
      </div>
      <div class="copyrights"><?php print render($page['copyrights']); ?></div>
    </div><!-- /.Same blog post Footer-->
  </div>
</div>
	

	<script>
	jQuery.noConflict();
	jQuery(function($) {

		// Display today's date while loading
		var d = new Date();

		var month = d.getMonth()+1;
		var day = d.getDate();

		var output = (month<10 ? '0' : '') + month + '/' +
			(day<10 ? '0' : '') + day + '/' + d.getFullYear();
		jQuery('input#datepicker').val(output);

		jQuery('input#datepicker').focus(function() {
			if(jQuery('#ui-datepicker-div').css('display') == 'none'){
				jQuery('.ui-datepicker-trigger').trigger('click');
		    }
		});
		
		jQuery( "#datepicker" ).datepicker({
			/*showOn: "button",
			buttonImage: "<?php// echo base_path() . path_to_theme(); ?>/images/calendar.gif",
			buttonImageOnly: true,	*/
           // showButtonPanel: true,
           // currentText: "Today:" + jQuery.datepicker.formatDate('MM dd, yy', new Date())
            
            
               // var dateObject = jQuery(this).datepicker('getDate'); 
               // alert(dateObject);
            
            onSelect: function()
            { 
                var dateObject = jQuery("#datepicker").datepicker('getDate'); 
                jQuery('.ui-datepicker-trigger').html(jQuery.datepicker.formatDate('dd', dateObject));
            },
            showOn: "both",
            buttonImageOnly: false,
            /*buttonImage: "calendar.gif",*/
            buttonText: jQuery.datepicker.formatDate('dd', new Date()),
            
		});
		
		jQuery('label.check-label').click(function() {
			if(jQuery(this).hasClass('green')) {
				jQuery(this).removeClass('green');
			} else {
				jQuery(this).addClass('green');
			}
		});
	});
	</script>
	
	<?php if($device != "mobile") { ?>
	<script>
	jQuery(document).ready(function() {
		jQuery('.lastminute-sticky h2').hide();
		var headHeight_lm = jQuery('nav.header').height() - 3;

			jQuery(window).resize(function () {
				jQuery('.lastminut-form').removeAttr('style');
				jQuery('.lastminut-form').removeClass('highlighted'); 
				headHeight_lm = jQuery('nav.header').height() - 3;
			});
		var stickyNavTop = jQuery('.lastminute-sticky').offset().top;

		var stickyNav = function(){
			var scrollTop = jQuery(window).scrollTop();

			if (scrollTop > stickyNavTop) { 
				jQuery('.lastminut-form').addClass('highlighted');
				jQuery('.lastminut-form.highlighted').animate({top:headHeight_lm},'slow');
			} else {
				jQuery('.lastminut-form').removeAttr('style');
				jQuery('.lastminut-form').removeClass('highlighted'); 
			}
		}; 

		stickyNav();

		jQuery(window).scroll(function() {
			stickyNav();
		});
	});
	</script>
	<?php } ?>
<script>
    jQuery(function(){
  jQuery('.select-category').selectric();
  jQuery('.select-distance').selectric();
  jQuery('.select-rating').selectric();
});
</script>
<script>
jQuery(function(){
    jQuery('.lastmint-price .price-inf0 > span:last-child').addClass('last');
    jQuery('.lastmint-price .price-inf0 > span:nth-of-type(2)').addClass('middle');
});

	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});

    </script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>
<script type="text/javascript">
		
	jQuery(document).ready(function(){
		jQuery('.lightbox').click(function(){
			//var cls = jQuery(this).parent().parent().attr('class').split(' ');
			var cls = jQuery(this).parent().attr('class').split(' ');
			jQuery('.backdrop, '+'.'+cls[1]+' .box').animate({'opacity':'.50'}, 300, 'linear');
			jQuery('.'+cls[1]+' .box').animate({'opacity':'1.00'}, 300, 'linear');
			jQuery('.backdrop, '+'.'+cls[1]+' .box').css('display', 'block');
		});
		
		jQuery('.close').click(function(){
			close_box();
		});
		
		jQuery('.backdrop').click(function(){
			close_box();
		});
	
	});
	
	function close_box()
	{
		jQuery('.backdrop, .box').animate({'opacity':'0'}, 300, 'linear', function(){
			jQuery('.backdrop, .box').css('display', 'none');
		});
	}

</script>


<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>




