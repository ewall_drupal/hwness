<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
//echo '<pre>';print_r($term_data);exit;
// foreach($term_data as $key => $value) {   }

?>



<?php foreach ($term_data as $key => $value) {

    $iuid = $term_data[$key]['uid'];

		$user_uid = $iuid;
		if(is_numeric($user_uid)){
		$iuid = $term_data[$key]['uid'];
		}else {
		$user_array = str_replace("<a href=\"","",$user_uid);
		$user_dts = explode(">",$user_array);
		$iuid = $user_dts[1];
		}
		
		 $iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('thumbnail', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}
		

   // $user_thumbnail_url = get_user_image($uid);
 ?>
    <div class="brick views-row">
      <div class="grid-item-01"> <!--first grid-->
		<span class="label"><?php echo $term_data[$key]['field_trend_category']; ?></span>
        <a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>"><img src="<?php echo $term_data[$key]['field_image']; ?>" width="100%"></a>

        <div class="item-01-info">
          <div class="actions">
            <a class="btn btn-secondary btn-round1" href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$term_data[$key]['uid']); ?>">
              <span class="clip_mask"><img style="border-radius:50%; margin-top:0px; width:44px; height:44px; top:0px; left:0px;" src="<?php echo $user_thumbnail_url; ?>"></span>
            </a>
          </div>
			<div class="info-01 tonotshowinfo">
            <p class="info-title">
							<span class="created-date"><?php echo $term_data[$key]['created']; ?></span>&nbsp;<span class="profile-date"><?php echo $term_data[$key]['created_1']; ?></span>
							<br>
							<span class="author-name">Af gæsteblogger <?php echo $term_data[$key]['name']; ?></span>
						</p>
            <p class="info-content"><?php echo $term_data[$key]['title']; ?></p>
            <p class="info-details"><?php echo $term_data[$key]['body']; ?></p>
            <div class="down"><img src="<?php echo base_path().path_to_theme() ?>/images/down-arrow.png"/></div>
          </div>
           <div style="clear:both"></div>
        </div>
       <div class="info-01 toshowinfo">
			<span class="created-date"><?php echo $term_data[$key]['created']; ?></span>&nbsp;<span class="profile-date"><?php echo $term_data[$key]['created_1']; ?></span>
							<br>
							<span class="author-name">Af gæsteblogger <?php echo $term_data[$key]['name']; ?></span>
						</p>
            <p class="info-content"><?php echo $term_data[$key]['title']; ?></p>
			<div class="info-details1">
				<?php echo $term_data[$key]['body_1']; ?>
			</div>
        </div>
        <div class="toshowup">
        
        <?php
        // echo like_widget_node($term_data[$key]['nid']);
        print drupal_render(plus1_build_node_jquery_widget($term_data[$key]['nid'], $tag = 'plus1_node_vote'));
        ?>
        <span class="comment">
          <a target="_blank" href="<?php echo base_path().'node/'.$term_data[$key]['nid'].'#comment-list'; ?>"><?php echo $term_data[$key]['comment_count']; ?> kommentarer</a>
        </span>
        <hr>
        <div class="up"><img src="<?php echo base_path().path_to_theme() ?>/images/up-arrow.png"/></div>
        </div>
      </div>
    </div>
  <?php } ?>
