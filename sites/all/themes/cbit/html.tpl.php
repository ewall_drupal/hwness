<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html>
<html xml:lang="<?php print $language->language; ?>">
	<head profile="<?php print $grddl_profile; ?>">
		<?php print $head; ?>
        <script type="text/javascript">
            // FB HAsh URL ignorer
//            if (window.location.hash == '#_=_') {
//                window.location.hash = '';
//                location.reload();
//            }
        </script>
		<title><?php print $head_title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes"/>	
		<?php print $styles; ?>
		<?php print $scripts; ?>
        
        <script type="text/javascript">
            jQuery(document).ready(function() {
            //Event Tracking - START

            //Last minute page - Se tider & book in list
			jQuery('.page-last-minute .lastminut-block .btn-book').on('click', function() {
				ga('send', 'event', 'TEST - Last minute page', 'click', jQuery(this).attr('value'));
				//alert("click");
			});
			
			//Last minute page - Book tid in splash
			jQuery('.page-last-minute .bookurl a').on('click', function() {
				ga('send', 'event', 'TEST - Last minute page', 'click', 'Test - Book tid');
			});
			
			//Behandlinger page - book
			jQuery('.page-find-behandling .lastminut-block .btn-book').on('click', function() {
				ga('send', 'event', 'TEST - Behandlinger page', 'click', jQuery(this).attr('value'));
			});
			
			//Find klinik page - email and website url
			jQuery('.page-find-klinik .findclinik-mail a').on('click', function() {
				ga('send', 'event', 'TEST - Find klinik page', 'click', jQuery(this).attr('href'));
			});

            //Event Tracking - END
            });
        </script>
		
	</head>
	
<?php 

	// $detect = mobile_detect_get_object();
	// $is_mobile = $detect->isMobile();
	
	$device = check_device();
	
	if($device == "mobile"){
		$classes .= " mobile-view";
	}
	
?>
	
	<body class="<?php print $classes; ?>" <?php print $attributes;?>>
        <script type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-52300633-1', 'auto');
            ga('send', 'pageview');
        </script>
		<?php/* if($device == "mobile"){ ?>
			<link href="<?php echo base_path().path_to_theme(); ?>/css/mobile.css" rel="stylesheet" />
			<link href="<?php echo base_path().path_to_theme(); ?>/css/mobile1.css" rel="stylesheet" />
			<link href="<?php echo base_path().path_to_theme(); ?>/css/mobile-responsive.css" rel="stylesheet" />
		<?php }*/ ?>
		<?php print $page_top; ?>
		<?php print $page; ?>
		<?php print $page_bottom; ?>
	</body>
</html>
