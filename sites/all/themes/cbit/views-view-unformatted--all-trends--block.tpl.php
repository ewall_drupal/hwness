<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
//echo '<pre>';print_r($term_data);
?>
<?php
foreach($term_data as $key => $value) {

		$iuid = $term_data[$key]['uid'];

		$user_uid = $iuid;
		if(is_numeric($user_uid)){
		$iuid = $term_data[$key]['uid'];
		}else {
		$user_array = str_replace("<a href=\"","",$user_uid);
		$user_dts = explode(">",$user_array);
		$iuid = $user_dts[1];
		}

		$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('blog_profile', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}		

   // $user_thumbnail_url = get_user_image($uid);

	if(isset($term_data[$key]['field_ad_image'])){
		$image_path = $term_data[$key]['field_ad_image'];
	}
	if(isset($term_data[$key]['field_image'])){
		$image_path = $term_data[$key]['field_image'];
	}
?>
<div class="blog-grid-item">
		<span class="label"><?php echo $term_data[$key]['field_trend_category']; ?></span>
		<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>"><img src="<?php echo $image_path; ?>" class="img-responsive"></a>
		<p><?php echo $term_data[$key]['title'] ?></p>
		<div class="blog-grid-details">
			<span class="blog-author">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$term_data[$key]['uid']); ?>"><img src="<?php echo $user_thumbnail_url; ?>"></a>
			</span>
			<span class="blog-author-info">
				<p><?php echo $term_data[$key]['created'] ?> <br><?php echo $term_data[$key]['name']; ?></p>
			</span>
			<span class="blog-likes">
				<?php echo like_widget_node($term_data[$key]['nid']); ?>
			</span>
			<!--<span class="blog-comments">
				<a href="<?php //echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']).'#comment-list'; ?>"><span class="blog-commentsicon"></span><?php //echo $term_data[$key]['comment_count']; ?> kommentarer</a>
			</span>-->
			<span class="comment">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']).'#comment-list'; ?>"><?php echo $term_data[$key]['comment_count'] ?> kommentarer</a>
			</span>
			<span class="clear"></span>
		</div>
		<div class="clear"></div>
</div>
<?php
}
?>
