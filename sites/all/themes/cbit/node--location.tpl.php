<?php  // echo "<pre>";print_r($node); exit; ?>

<?php 

	$device = check_device();
	
	_ajax_register_include_modal();
	$classes = array();
	$classe = array();
	$classes[] = 'ctools-use-modal';
	$classes[] = 'ctools-modal-ctools-ajax-register-style';
	$classes[] = 'btn-skriv';
	
	$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
	
	$classe[] = 'btn-skriv';
	$mobile_options = array('attributes' => array('class' => $classe));
	

	$clinic_nid = $node->field_location_custref['und'][0]['entity']->nid;

	$clinic = node_load($clinic_nid);
	$clinic_title = $clinic->title;
	$clinic_nid = $clinic->nid;
	$comment_count = $clinic->comment_count;
	$clinic_website= strip_tags($clinic->field_customer_website['und'][0]['value']);

	$address = '';
	if(isset($node->field_location_address['und'][0]['thoroughfare'])){
		$address .= $node->field_location_address['und'][0]['thoroughfare'].', ';
	}
	if(isset($node->field_location_address['und'][0]['premise'])){
		$address .= $node->field_location_address['und'][0]['premise'].', ';
	}
	if(isset($node->field_location_address['und'][0]['locality'])){
		$address .= $node->field_location_address['und'][0]['locality'].', ';
	}
	if(isset($node->field_location_address['und'][0]['postal_code'])){
		$address .= $node->field_location_address['und'][0]['postal_code'].', ';
	}
	if(isset($node->field_location_address['und'][0]['administrative_area'])){
		$address .= $node->field_location_address['und'][0]['administrative_area'].', ';
	}
	if(isset($node->field_location_address['und'][0]['country'])){
		$address .= $node->field_location_address['und'][0]['country'];
	}
	$location_email = strip_tags($node->field_location_email['und'][0]['value']);


	$om_location = $node->field_location_desc_textarea['und']["0"]['value'];

	$img_path = $node->field_location_image['und']["0"]['uri'];
	$images = substr($img_path,'9'); 
	$location_thumb_image = image_style_url('client_details', $images );
	if(isset($node->field_location_openhours['und'][0]['value'])){
		$abningstider = $node->field_location_openhours['und'][0]['value'];
	}
	global $base_url;

?>

<?php  if($device != "mobile") { ?>

<div class="clinicprofile-topsection">
    <!--Topsection -->
    <div class="clinicprofile-rating">
        <!--rating -->
        <div class="clinicprofile-rating-content">
            <h2 class="rating-title"><?php print $clinic_title; ?></h2>
						<?php echo clinik_avg_rating($nid, 'widget'); ?>
            <p class="anmedelser-count">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$node->nid).'#tab2' ?>"><?php echo $node->comment_count.' anmeldelser' ?></a>
			</p>
            <p class="line"></p>
            <p class="rating-address">
                <?php print $address; ?>
			</p>            
            <p class="line"></p>
            <div class="mails">
                <span><?php print $clinic_website; ?></span>
                <span><?php print $location_email; ?></span>
            </div>
            <input class="btn-rating-book" type="submit" value="Book Tid">
            <div class="sealle">                
               <!-- <p>sealle klinikker <a href="#"><img src="<?php// echo base_path() . path_to_theme(); ?>/images/readmore.png"></a>
                </p> -->
            </div>
        </div>
    </div>
    <?php if($device == "mobile") { ?>
		<div class="mobile clinicprofile-sidebar">
			<!-- side bar -->
			<div class="clinicprofile-login">
				<h5>skriv en anmeldelse</h5>
				<?php
				  if ($user->uid == 0) {
					print l('Skriv en anmeldelse', 'ajax_register/login/nojs', $options);
				  } else {
					$skriv_options = array('attributes' => array('class' => 'btn-skriv', 'rel' => 'nofollow'));
					print l('Skriv en anmeldelse', 'comment/reply/' . $node->nid, $skriv_options);
				  }
				?>
			</div>
			<div class="clear-float"></div>
			
			<?php if(isset($abningstider)) { ?>
				<!--Timings Left -->
				<div class="sidebar-content">
					<h1>Abningstider</h1>
					<div class="openhours-content">
					<?php echo $abningstider; ?>
					</div>
				</div>
				<!-- / .Timings Left -->
			<?php } ?>
			<?php if(isset($address)) { ?>
				<div id="map" style="width: 320px; height: 250px;margin-top:10px;border:1px solid #fff;"></div>
			<?php } ?>
			<div class="clear-float"></div>
		</div>	    
    <?php } ?>
    <!--rating -->
	<?php if($device != "mobile") { ?>
		<div class="clinicprofile-photo">
			<!--clinicprofile-photo-->
			<?php if($location_thumb_image){ ?>
			<img src="<?php print $location_thumb_image; ?>">
			<?php } else { ?>
				<img src="<?php echo base_path() . path_to_theme(); ?>/images/klinik_default.jpg">
			<?php } ?>
		</div>
     <?php } ?>
    <!--clinicprofile-photo-->
</div>
<!--Topsection -->

<div class="clinicprofile-content">
    <!-- Blog Post content-->
    <div class="clinicprofile-left">
        <!-- left content-->
        <div class="clinicprofile-left-content">
            <div class="tab-container" id="tab-container">
                <ul class='tabs-clinic-profile'>
                    <li class="first"><a class="active" href="#tab1">behandlingstyper</a></li>
                    <li><a data-toggle="tab" href="#tab2">se anmeldelser</a></li>
                    <li><a class="" href="#tab3">Om klinikken</a></li>
                </ul>
                <div class="clear-float"></div>
				<!---Tab1-->
                <div id="tab1">
                    <?php
						$location_categories = module_invoke('views', 'block_view', 'location_categories-block');
						print render($location_categories['content']);
					?>
                </div>
                <!-- Tab1-->
                <div id="tab2" style="display: block;">
                    <div class="free-wall" id="freewall-01">
						<div class="region region-location_comments">
							<div class="block block-views clearfix" id="block-views-location_comments-block">
								<div class="content">
									<?php
										$location_comments = module_invoke('views', 'block_view', 'location_comments-block');
										print render($location_comments['content']);
									?>
								</div>
							</div>
						</div>
                    </div>
                </div>
                <div id="tab3" style="display: none;">
                    <div class="qp-tab-container clinic-data-container">
                        <?php print $om_location; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /left content-->
  <?php if($device != "mobile") { ?>
		<div class="clinicprofile-sidebar">
			<!-- side bar -->
			<div class="clinicprofile-login">
				<h5>skriv en anmeldelse</h5>
				<?php
				  if ($user->uid == 0) {
					print l('Skriv en anmeldelse', 'ajax_register/login/nojs', $options);
				  } else {
					$skriv_options = array('attributes' => array('class' => 'btn-skriv', 'rel' => 'nofollow'));
					print l('Skriv en anmeldelse', 'comment/reply/' . $node->nid, $skriv_options);
				  }
				?>
			</div>
			<div class="clear-float"></div>
			
			<?php if(isset($abningstider)) { ?>
				<!--Timings Left -->
				<div class="sidebar-content">
					<h1>Abningstider</h1>
					<div class="openhours-content">
					<?php echo $abningstider; ?>
					</div>
				</div>
				<!-- / .Timings Left -->
			<?php } ?>
			<?php if(isset($address)) { ?>
				<div id="map" style="width: 320px; height: 250px;margin-top:10px;border:1px solid #fff;">
                    <img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo trim($address); ?>&zoom=13&size=320x250&markers=color:blue%7Clabel:S%7C<?php echo trim($address); ?>&key=AIzaSyCYsQYcEO9mZ2u4mbdxQGJELMfKzXQhpb8" />
                </div>
			<?php } ?>
			<div class="clear-float"></div>
		</div>
  <?php } ?>
    <!-- /. side bar -->
</div>
<!-- /. Blog Post content-->


<?php /******************************************************** Mobile View Start ********************************************************/ ?>

<?php }  else { ?>


<div class="clinicprofile-topsection">
		<div class="clinicprofile-photo">
			<!--clinicprofile-photo-->
			<?php if($location_thumb_image)	{ ?>
				<img src="<?php print $location_thumb_image; ?>">
			<?php } else { ?>
				<img src="<?php echo base_path() . path_to_theme(); ?>/images/klinik_default.jpg">
			<?php } ?>
		</div>
	<!--clinicprofile-photo-->
    
    <!--Topsection -->
    <div class="clinicprofile-rating">
        <!--rating -->
        <div class="clinicprofile-rating-content">
            <h2 class="rating-title"><?php print $clinic_title; ?></h2>
						<?php echo clinik_avg_rating($nid, 'widget'); ?>
            <p class="anmedelser-count">
				<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$node->nid).'#tab2' ?>"><?php echo $node->comment_count.' anmeldelser' ?></a>
			</p>
            <p class="line"></p>
            <p class="rating-address">
                <?php print $address; ?>
			</p>            
            <p class="line"></p>
            <div class="mails">
                <span><?php print $clinic_website; ?></span>
                <span><?php print $location_email; ?></span>
            </div>
            <input class="btn-rating-book" type="submit" value="Book Tid">
            <div class="sealle">                
               <!-- <p>sealle klinikker <a href="#"><img src="<?php// echo base_path() . path_to_theme(); ?>/images/readmore.png"></a>
                </p> -->
            </div>
        </div>
    </div>
    <div id="aboutus-location">
		<div class="aboutus">
			<h2><?php print t("Om os"); ?></h2>
			<?php 
			$res_count = strlen($om_location);
			$trim_content = substr($om_location,0,200);
			if($res_count <= 200) {
			?>
				<?php echo $om_location; ?>	
			<?php } else { ?>
			
			
			<div class="info-01 tonotshowinfo">            
				<?php echo $trim_content."..."; ?>	
            <div class="down"><img src="<?php echo base_path().path_to_theme() ?>/images/down-arrow.png"/></div>
          </div>
          
           <div class="info-01 toshowinfo">
			<div class="info-details1">
					<?php echo $om_location; ?>	
			</div>
			<div class="up"><img src="<?php echo base_path().path_to_theme() ?>/images/up-arrow.png"/></div>
        </div>
          <?php } ?>      
			
		</div>
     </div>
     
		<div class="mobile clinicprofile-sidebar">
			<!-- side bar -->
			<div class="clinicprofile-login">
				<h5>skriv en anmeldelse</h5>
				<?php
				  if ($user->uid == 0) {
					print l('Skriv en anmeldelse', 'user/login', $mobile_options);
				  } else {
					$skriv_options = array('attributes' => array('class' => 'btn-skriv', 'rel' => 'nofollow'));
					print l('Skriv en anmeldelse', 'comment/reply/' . $node->nid, $skriv_options);
				  }
				?>
			</div>
			<div class="clear-float"></div>
			
			<?php if(isset($abningstider)) { ?>
				<!--Timings Left -->
				<div class="sidebar-content">
					<h1>Abningstider</h1>
					<div class="openhours-content">
					<?php echo $abningstider; ?>
					</div>
				</div>
				<!-- / .Timings Left -->
			<?php } ?>
			
			<div class="lastminut-block">
			<div class="lastmint-item">
				<div class="lastmint-product">
                    <h1 class="m-title">Ansigtsbehandling<span class="m-kr">250kr</span></h1>
                    <div class="price-inf0">
                        <ul class="m-price">
                        <li>Vaerdi  550,-</li>
                        <li>Rabat 50%</li>
                        <li>Spar 275,-</li>
                        </ul>
                   </div>
                    <div class="m-subtitle">
                        <h2>Cha Cha Cha</h2>
                        <img src="<?php echo base_path() . path_to_theme(); ?>/images/last-star.png">
                        <p>Kompagnistræde 15 st.<br>208 København K</p>
                     </div>  
                     <div class="m-date">
                        <p>Dato: 26.03.2014 </p>
                        <p>Varighed: 125 min </p>
                        <div class="m-readmore">
                              <a href="#">Laes mere</a>
                        </div>
                     </div>
                      <h3> Dine valgte timeslots</h3>
                    <span class="timer">08 - 11</span>
				</div>
			</div>
		</div>
        		<div class="lastminut-block">
			<div class="lastmint-item second">
				<div class="lastmint-product">
                    <h1 class="m-title">Voksbehandling.<span class="m-kr">250kr</span></h1>
                    <div class="price-inf0">
                        <ul class="m-price">
                        <li>Vaerdi 550,-</li>
                        <li>Rabat 50%</li>
                        <li>Spar 275,-</li>
                        </ul>
                   </div>
                    <div class="m-subtitle">
                        <h2>Cha Cha Cha</h2>
                        <img src="<?php echo base_path() . path_to_theme(); ?>/images/last-star.png">
                        <p>Kompagnistræde 15 st.<br>208 København K</p>
                     </div>  
                     <div class="m-date">
                        <p>Dato: 26.03.2014 </p>
                        <p>Varighed: 125 min </p>
                        <div class="m-readmore">
                              <a href="#">Laes mere</a>
                        </div>
                     </div>
                      <h3> Dine valgte timeslots</h3>
                    <span class="timer">08 - 11</span> <span class="timer">08 - 11</span> <span class="timer">08 - 11</span> <span class="timer">08 - 11</span><span class="timer">08 - 11</span>
				</div>
			</div>
		</div>
		<div class="clear-float"></div>
					  
					  	

					  
			<?php if(isset($address)) { ?>
				<div id="map">
                    <img src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo trim($address); ?>&zoom=18&size=480x480&markers=color:blue%7Clabel:S%7C<?php echo trim($address); ?>&key=AIzaSyCYsQYcEO9mZ2u4mbdxQGJELMfKzXQhpb8" />
                </div>
			<?php } ?>
			<div class="clear-float"></div>
		</div>	    
		

</div>


<?php } ?>
