<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
//~ echo '<pre>';
//~ print_r($term_data);
//~ exit;
// echo '<pre>'; print_r($term_data); exit;
?>
<?php foreach ($term_data as $key => $value) { 
	$nodeid = $term_data[$key]['nid'];
	$wesite_address = "http://".$term_data[$key]['field_customer_website']; 
	?>
	<div class="findklinik-block">
		<div class="findklinik-contents">
			<div class="findklinik-item">
				<div class="findklinik-product">
					<h6>STED</h6>
					<h1><?php echo $term_data[$key]['field_location_custref']; ?></h1>
					<div class="findklinik-rating">
						<?php echo $term_data[$key]['field_clinic_loc_rating']; ?>
					</div>				
					<p><?php echo $term_data[$key]['field_location_address']; ?></p>
					<div class="findclinik-mail">
						<a><span><?php echo $term_data[$key]['field_location_email']; ?></span></a><br/>
						<a target="_blank" href="<?php echo $wesite_address; ?>"><span><?php echo $term_data[$key]['field_customer_website']; ?></span></a>
					</div>	
				</div>
			</div>			
			<div class="findklinik-item">
				<div class="findklinik-details">
					<h6>OM KLINIKKEN</h6>
					<div id ="find-short-<?php echo $nodeid; ?>" class="find-short">
						<p><?php echo $term_data[$key]['field_location_shortdesc_1']; ?></p>
						<a class="read-more" rel="<?php echo $nodeid; ?>" href="javascript:void(0);"><?php echo t('Read more'); ?></a>
					</div>	
					<div id ="find-long-<?php echo $nodeid; ?>" class="find-long">
						<p><?php echo $term_data[$key]['field_location_shortdesc']; ?></p>
						<a class="read-less" rel="<?php echo $nodeid; ?>" href="javascript:void(0);"><?php echo t('Less'); ?></a>
					</div>				
				</div>				
			</div>
		</div>	
		<div class="findklinik-map">	
			<div class="findklinik-item">
				<?php // echo $term_data[$key]['field_location_geo']; ?>
                <!-- http://maps.googleapis.com/maps/api/staticmap?center=40.714728,-73.998672&zoom=12&size=400x400markers=color:red%7Clabel:S%7C11211%7C11206%7C11222&key=AIzaSyCYsQYcEO9mZ2u4mbdxQGJELMfKzXQhpb8 -->
                <img style="height=230px;" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $term_data[$key]['field_location_geo'].' ,'.$term_data[$key]['field_location_geo_1']; ?>&zoom=14&size=320x230&markers=color:red%7Clabel:S%7C<?php echo $term_data[$key]['field_location_geo'].' ,'.$term_data[$key]['field_location_geo_1']; ?>&key=AIzaSyCYsQYcEO9mZ2u4mbdxQGJELMfKzXQhpb8" />
			</div>
		</div>
	</div>

	<div class="clear-float"></div>
  <?php } ?>
