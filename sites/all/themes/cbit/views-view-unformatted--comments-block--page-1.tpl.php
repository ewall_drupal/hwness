 <?php

/**
 * tpl used to display the comments of location on separate page
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

global $base_url;
 $term_data = $view->style_plugin->rendered_fields;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
// $classes[] = 'btn-skriv';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
 // echo '<pre>';print_r($term_data);exit;

 $device = check_device();
 
?>
	<?php foreach($term_data as $key => $value) {

     $iuid = $term_data[$key]['uid'];

		 		$user_uid = $iuid;
		if(is_numeric($user_uid)){
			$iuid = $term_data[$key]['uid'];
		}else {
			$user_array = str_replace("<a href=\"","",$user_uid);
			$user_dts = explode(">",$user_array);
			$iuid = $user_dts[1];
		}
		
		 $iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('thumbnail', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}

    // $user_thumbnail_url = get_user_image($uid);


     $share_url = $base_url.'/location/comment/'.$term_data[$key][ 'cid'];
?>
        <div class="blogpost-left-content">
            <div class="blogpost-info">
                <p>
                    <?php echo $term_data[$key][ 'subject']; ?>
                </p>
            </div>
            <div class="blogpost-author-content">
                <div class="blogpost-author">
<!--                    <img align="left" style="border-radius:50%; width:44px; height:44px;" vertical-align="middle" src="<?php// echo $term_data[$key]['field_enterprise_blog_picture']; ?>">-->
                    <a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$iuser->uid); ?>"><img align="left" style="border-radius:50%; width:44px; height:44px;" vertical-align="middle" src="<?php echo $user_thumbnail_url; ?>"></a>
                </div>
                <div class="blogpost-author-details">
                    <p>
                        <?php echo $term_data[$key][ 'changed']; ?>&nbsp;<span class="blog-post-time post-time"><?php echo $term_data[$key]['changed_1']; ?></span>
                        <br>
                        <?php echo $term_data[$key][ 'name']; ?>
                    </p>
                </div>
                <div class="blogpost-author-likes">
                    <?php print drupal_render(plus1_build_comment_jquery_widget($term_data[$key]['cid'], $tag = 'plus1_comment_vote')); ?>
					<?php  //print like_widget_comment($term_data[$key]['cid']); ?>
                </div>
                <div class="blogpost-author-comments">
                    <p><a href="<?php echo base_path().'node/'.$term_data[$key]['nid']; ?>"><span class="blog-commentsicon"></span>
                        <?php echo kommentarer_count($term_data[$key][ 'cid']); ?> kommentarer</a></p>
                </div>
                <div class="clear-float"></div>
            </div>
            <div class="blogpost-article">
                <p>
                    <?php echo $term_data[$key][ 'comment_body']; ?>
                </p>
            </div>
            
            <div class="clear-float"></div>
            <div class="add-sub-comment">
                <div class="add-sub-comment-link">
                    <?php
                    if ($user->uid == 0) {
                        if($device == "mobile") {
                            print '<div class="subcomment"><span class="addsubcomment">'.l('Add Comment', $base_url.'/user/login').'</span></div>';
                        }
                        else {
                            print '<div class="subcomment"><span class="addsubcomment">'.l('Add Comment', 'ajax_register/login/nojs', $options).'</span></div>';
                        }
                    } else {
                        print '<div class="subcomment"><span class="addsubcomment">'.t('Add Comment').'</span></div>';
                    }
                    ?>
                </div>
                <div class="add-sub-comment-area">
                    <form id="subcomment-form">
                        <!-- <label>Subject: </label>
                        <input type="text" name="name" value="" /><br /><br />
                        <label>Your Comment: </label> -->
                        <textarea style="width: 100%; height: 70px;" name="message"></textarea>
                        <input id="pcid" type="hidden" name="pcid" value="<?php  print $term_data[$key]['cid']; ?>" />
                        <input id="nid" type="hidden" name="nid" value="<?php  print $term_data[$key]['nid']; ?>" />
                        <br /><br /> 
                        <input id="submit" type="submit" name="submit" value="Submit" /><div class="throbber left" style=""></div><div class="clear"></div><br />
                    </form>
                </div>
            </div>
            <!--
            <div class="blogpost-clink">
                <h4>Kategorier</h4>
                <ul class="clink">
                    <li>
                        <?php echo $term_data[$key][ 'field_clinic_loc_category']; ?>
                    </li>

                </ul>
                <ul class="blogpost-social">
				<li>
					<a id="ref_fb"  href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $term_data[$key][ 'subject']; ?>&amp;p[summary]=<?php print $term_data[$key][ 'comment_body_1']; ?>&amp;p[url]=<?php echo urlencode($share_url);?>&amp;p[images][0]=<?php echo urlencode($term_data[$key][ 'field_category_image']);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600'); return false;">
						<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-facbook.png">
					</a>
				</li>
				<li>
					<a id="ref_tw" href="http://twitter.com/home?status=<?php echo $term_data[$key][ 'subject']; ?>+<?php echo urlencode($share_url);?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
						<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-twitter.png">
					</a>
				</li>
				<li>
					<a id="ref_pr" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo urlencode($term_data[$key][ 'field_category_image']);?>&amp;url=<?php echo urlencode($share_url);?>&amp;is_video=false&amp;description=<?php print $term_data[$key][ 'comment_body_1'] ?>" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
						<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-pinterest.png">
					</a>
				</li>
				<li>
					<a id="ref_gp" href="https://plus.google.com/share?url=<?php echo urlencode($share_url);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false">
						<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-g+.png">
					</a>
				</li>
			</ul>
                <div class="clear-float"></div>
            </div>
        -->
        </div>
	<?php } ?>
