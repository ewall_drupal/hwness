<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
 $term_data = $view->style_plugin->rendered_fields;
 
 //echo '<pre>';print_r($term_data);exit;
 
 
?>

	
	<link rel="stylesheet" type="text/css" href="<?php echo base_path().path_to_theme(); ?>/css/ticker-style.css" />

	<script src="<?php echo base_path().path_to_theme() ?>/js/jquery.ticker.js" type="text/javascript"></script>
	<script src="<?php echo base_path().path_to_theme() ?>/js/site.js" type="text/javascript"></script>

	<ul id="js-news" class="js-hidden">
	
	<?php  foreach($term_data as $key => $value) { 
		$title = $term_data[$key]['title'];
	?>
	
		<li class="news-item"><?php echo $title ; ?></li>

		
	<?php } ?>	
	
	</ul>
	