<?php
global $user;
global $base_url;

$arg0 = arg(0);
$arg1 = arg(1);
$arg2 = arg(2);

$page_name = '';
if ($arg0 == 'user') {
  if (($arg1 != '') && (is_numeric($arg1))) {
    $page_name = 'user-profile';
  }
}

_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
?>
<?php 
	//	$detect = mobile_detect_get_object();
	//	$is_mobile = $detect->isMobile();
	
	$device = check_device();
?>
<?php if($device != "mobile") { ?>
	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			 <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
<?php } ?>
	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			 <li><a title="" href="<?php echo base_path(); ?>/last-minute">OPRET PROFIL</a></li>
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->



    <div class="main-content-container container">
		
		<?php if($device == "mobile") { ?>
		 <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>
		
		<div class="profile pagetpl">
		  <?php if ($page_name != 'user-profile') { ?>  <!-- FOR ALL NORMAL CMS PAGES -->
			<div class = "profile-container">
			  <div class = "statisk-container">
				<div id = "main-content">
				  <?php if ($tabs):
					?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
				  <?php print render($title_prefix); ?>
				  
				  
				  <?php if ($title): ?>
				  <?php $arg = arg(0); $arg2 = arg(1); $arg3 = arg(2);
				  
						if($device != "mobile"){ ?>
							<h1 <?php print $tabs ? ' class="with-tabs"' : ''  ?>> <?php print $title ?> </h1>
						<?php } elseif ((($arg != "node") && ($arg2 != "add") && ($arg3 != "enterprise-blog")) || (($arg != "node") && ($arg2 != "add") && ($arg3 != "trends"))) { ?>
							<h1 <?php print $tabs ? ' class="with-tabs"' : ''  ?>><?php print $title ?></h1>
						<?php } 
						endif;
						?>
						
						
					<?php print render($title_suffix); ?>
					<?php if (arg(0)!="search" and arg(1)!="node"):?>
					<?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
					<?php endif; ?>
				  <?php print render($tabs2); ?>
				  <?php print $messages; ?>
				  
				  <?php 
					$arg = arg(0); $arg2 = arg(1); $arg3 = arg(2);
					
					if($device != "mobile"){
						print render($page['content']);
					}else if( (($arg == "node") &&  ($arg2 == "add") && ($arg3 == "enterprise-blog")) || (($arg == "node") && ($arg2 == "add") && ($arg3 == "trends")) ) {
						echo "<h4 style=text-align:center;>This page can't be accessed on mobile</h4>"; 						
					}
					
				  ?>
				  
				</div>
			  </div>
			</div>
		  <?php } else { ?>  <!-- FOR USER PROFILE PAGES -->
			<div class = "profile-container">
			  <?php print $messages; ?>
			  <?php print render($page['content']); ?>
			</div>
		  <?php } ?>

		  <div class="footer-section">
			<div class="footer-nav">
			  <?php print render($page['footer_menu']); ?>
			</div>
			<div class="copyrights"><?php print render($page['copyrights']); ?></div>
		  </div>
		</div>
	</div>
	
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>	
<script type="text/javascript">

	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});



	jQuery('.comment-form .field-name-field-clinic-loc-category .form-select').selectric();

</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>


<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>
