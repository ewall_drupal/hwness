<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
$term_datas = $view->style_plugin->rendered_fields;
//echo '<pre>';print_r($term_datas);exit;
?>
<?php
foreach($term_datas as $term_data) {
	//echo '<pre>';print_r($term_data);exit;
?>
<div class="item-post">
	<div class="post-details">
		<span class="post-date"><?php echo $term_data['created'] ?></span><span class="blog-post-time post-time"><?php echo $term_data['created_1'] ?></span>
		<p> <?php print l(t($term_data['body']), 'node/'.$term_data['nid'], array('attributes' => array('class' => array('indla-link')))); ?></p>
	</div>
	<hr>
</div>
<?php 
} 
?>
