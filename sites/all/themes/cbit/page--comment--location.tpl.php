<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$nid = arg(2);
$node = node_load($nid);

$clinic_nid = $node->field_location_custref['und'][0]['entity']->nid;

$clinic = node_load($clinic_nid);
$clinic_title = $clinic->title;
$clinic_nid = $clinic->nid;
$comment_count = $clinic->comment_count;
$clinic_website= strip_tags($clinic->field_customer_website['und'][0]['value']);

$address = '';
if(isset($node->field_location_address['und'][0]['thoroughfare'])){
	$address .= $node->field_location_address['und'][0]['thoroughfare'].',';
}
if(isset($node->field_location_address['und'][0]['premise'])){
	$address .= $node->field_location_address['und'][0]['premise'].',';
}
if(isset($node->field_location_address['und'][0]['locality'])){
	$address .= $node->field_location_address['und'][0]['locality'].',';
}
if(isset($node->field_location_address['und'][0]['postal_code'])){
	$address .= $node->field_location_address['und'][0]['postal_code'].',';
}
if(isset($node->field_location_address['und'][0]['administrative_area'])){
	$address .= $node->field_location_address['und'][0]['administrative_area'].',';
}
if(isset($node->field_location_address['und'][0]['country'])){
	$address .= $node->field_location_address['und'][0]['country'].',';
}
$location_email = strip_tags($node->field_location_email['und'][0]['value']);


$img_path = $node->field_location_image['und']["0"]['uri'];
$images = substr($img_path,'9'); 
$location_thumb_image = image_style_url('client_details', $images );
if(isset($node->field_location_openhours['und'][0]['value'])){
	$abningstider = $node->field_location_openhours['und'][0]['value'];
}


?>
<?php 
	// $detect = mobile_detect_get_object();
	// $is_mobile = $detect->isMobile();
	
	$device = check_device();
?>

<?php print render($page['header']); ?>

<link href="<?php echo base_path().path_to_theme(); ?>/css/clinic_profile.css" rel="stylesheet" />
<link href="<?php echo base_path().path_to_theme(); ?>/css/default.css" rel="stylesheet" />
<link href="<?php echo base_path().path_to_theme(); ?>/css/tab.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>

<!--<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>-->

<?php if($device != "mobile") { ?>
	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			  <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->
<?php } ?>	

	
<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			 
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
<!--===========================Mobile Menu  End=========================*/-->

	
<div class="main-content-container container">
		<?php if($device == "mobile") { ?>
		  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>

	<div class="clinicprofile-container">
        <!-- Blog Post -->
          <?php print $messages; ?>
          <?php //print render($page['content']); ?>
                    
          <div class="clinicprofile-topsection">
			<!--Topsection -->

			<div class="clinicprofile-rating">
				<!--rating -->

				<div class="clinicprofile-rating-content">
					<h2 class="rating-title"><?php print $clinic_title; ?></h2>
					<img src="<?php echo base_path() . path_to_theme(); ?>/images/lastminute-star.png">
					<p>9 anmeldeser</p>
					<p class="line"></p>
					<p class="rating-address">
						<?php print $location_thoroughfare; ?>
						<?php print $premise; ?>
						<?php print $location_locality; ?>
						<?php print $location_postal_code; ?>
					  </p>
					
					<p class="line"></p>

					<div class="mails">
						<span><?php print $clinic_website; ?></span>
						<span><?php print $location_email; ?></span>
					</div>
					<input class="btn-rating-book" type="submit" value="Book Tid">
					<div class="sealle">
						<p>sealle klinikker <a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/readmore.png"></a>
						</p>

					</div>
				</div>
			</div>
			<!--rating -->

			<div class="clinicprofile-photo">
				<!--clinicprofile-photo-->
				<?php if($location_thumb_image){ ?>
				<img src="<?php print $location_thumb_image; ?>">
				<?php } else { ?>
					<img src="<?php echo base_path() . path_to_theme(); ?>/images/clinic-profile-banner.jpg">
				<?php } ?>
			</div>
			<!--clinicprofile-photo-->
		</div>
		<!--Topsection -->

		<div class="clinicprofile-content">
			<!-- Blog Post content-->
			<div class="clinicprofile-left">
				<!-- left content-->
				<div class="clinicprofile-comment-left-content">
                <h2>Anmelde <?php print $clinic_title; ?></h2>
					<?php
						 print drupal_render(drupal_get_form("comment_form", (object) array('nid' => arg(2))));
					?>
				</div>
			</div>
			<!-- /left content-->
			
			<!-- side bar -->
			<div class="clinicprofile-sidebar clinicprofile-comment-sidebar">
				<h2>ANMELDELSER</h2>
				<?php print render($page['recent_location_comments']); ?>
			</div>
			<!-- /. side bar -->
			<div class="clear-float"></div>
		</div>
		<!-- /. Blog Post content-->
          
          

        <div class="clear-float"></div>

        <div class="clinicprofile-footer">
            <!--footer-->

            <div class="footer-nav">
                <?php print render($page['footer_menu']); ?>
            </div>

            <div class="clear-float"></div>

            <div class="copyrights"><?php print render($page['copyrights']); ?></div>
        </div><!--/ .footer-->
    </div><!-- /. Blog Post -->
</div>

<script type="text/javascript">
	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});
	
</script>
<script type="text/javascript">
    jQuery(function(){
		jQuery('.clinicprofile-comment-left-content .field-name-field-clinic-loc-category .form-select').selectric();
	});
</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>


<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>
