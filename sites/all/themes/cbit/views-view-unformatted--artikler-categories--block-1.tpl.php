<?php

/**
* @file
* Default simple view template to display a list of rows.
*
* @ingroup views_templates
*/
$term_data = $view->style_plugin->rendered_fields;
global $base_url;
$arg2 = arg(2);
// echo '<pre>';print_r($term_data);exit;
?>
<?php
foreach($term_data as $key => $value) {

 $user_uid = $term_data[$key]['uid'];

 if(is_numeric($user_uid)){
	$iuid = $term_data[$key]['uid'];
 }else {
	 $user_array = str_replace("<a href=\"","",$user_uid);
	 $user_dts = explode(">",$user_array);
	 $iuid = $user_dts[1];
 }
 
  		$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('blog_profile', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}	

//$user_thumbnail_url = get_user_image($uid);

 //echo $user_thumbnail_url;
//exit;
	if(isset($term_data[$key]['field_ad_image'])){
		$image_path = $term_data[$key]['field_ad_image'];
	}
	if(isset($term_data[$key]['field_image'])){
		$image_path = $term_data[$key]['field_image'];
	}
?>
<div class="brick views-row">
	<div class="grid-item-01"> <!--first grid-->
		<span class="label"><a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>"><?php echo $arg2; ?></a></span>
		<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$term_data[$key]['nid']); ?>">
			<img src="<?php echo $image_path; ?>" class="img-responsive">
		</a>
		<div class="item-01-info">
			<div class="blog-info-01">
				<p class="blog-info-content"><?php echo $term_data[$key]['title']; ?></p>
			</div>
		</div>
		<div class="item-blog-author">
			<span class="info-author-image"><a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$iuid); ?>"><img src="<?php echo $user_thumbnail_url; ?>"></a></span>
			<span class="blog-author-details"><?php echo $term_data[$key]['created'].'   '.$term_data[$key]['name'] ?> </span>
		</div>
		<div class="comments-home">
			<?php echo like_widget_node($term_data[$key]['nid']); ?>
			<a href="<?php echo base_path().'node/'.$term_data[$key]['nid']; ?>"><span class="comment"><?php echo $term_data[$key]['comment_count'] ?> kommentarer</span></a>
		</div>
	</div>
</div>
<?php 
} 
?>
