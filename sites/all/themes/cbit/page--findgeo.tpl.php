<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$arg_1 = arg(1);
$vid = 6;
$kategori = taxonomy_get_tree($vid);
$kategori_options = '<option value="All">Vælg kategori</option>';
foreach ($kategori as $key => $val) {
	if($arg_1 == $kategori[$key]->tid) {
		$kategori_options .= '<option selected value="' . $kategori[$key]->tid . '">' . $kategori[$key]->name . '</option>';
	} else {
		$kategori_options .= '<option value="' . $kategori[$key]->tid . '">' . $kategori[$key]->name . '</option>';
	}
}

$smart_ip_session = smart_ip_session_get('smart_ip');
//echo "<pre>";print_r($smart_ip_session);
?>

<?php 
	// $detect = mobile_detect_get_object();
//	$is_mobile = $detect->isMobile();
	$device = check_device();
	
?>

<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>


<?php if($device != "mobile") { ?>
  	<nav class="header"><!--main-menu-->
	  <div class="main-menu">
		<div class="logo">
			<?php if ($logo): ?>
				<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
					<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
				</a>
			<?php endif; ?>
		</div>
		<div class="top-link">
		  <ul class="top-bar">
			<li class="first" style="padding-left:0px;">
			  <div id="slidediv">
				<?php print $search_box; ?>
			  </div>
			  <a href="#" class="search-btn">
				<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
			  </a>
			</li>
			<li class="second" style="padding-left:0px;">
			  <div id="slidediv1">
				<?php print render($page['subscribe']); ?>
			  </div>
			   <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
			</li>

			<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
			<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
			<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
			<li class="usr-login">
			  <?php
			  if ($user->uid == 0) {
				print l('LOGIN', 'ajax_register/login/nojs', $options);
			  } else {
				print l($user->name, 'user/' . $user->uid);
			  }
			  ?>
			</li>
			<li class="usr-logout">
			  <?php
			  if ($user->uid == 0) {
				print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
			  } else {
				print l('LOG UD', 'user/logout');
			  }
			  ?>
			</li>
		  </ul>
		</div>
		<div class="navigation-bar">
		  <?php print render($page['top_menu']); ?>
		</div>
	  </div>
	</nav><!-- /. end main-menu -->

<?php } ?>		
	<!--===========================Mobile Menu  Start=========================*/-->
		
<?php if($device == "mobile") { ?>
	<nav id="menu">

	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->	
	
<div class="main-content-container container">

		<?php if($device == "mobile") { ?>
		  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php } ?>

  <div class="lastminute-container">
    <!-- main content -->

		<?php print $messages; ?>
    <div class="lastminute-header">
      <!--header-->
      <h1>Find behandling</h1>
      <h5>Søg efter behandlinger</h5>
    </div>

    <div class="clear-float"></div>
    <div class="lastminute-content behandling-content">
      <div class="lastminut-form">
		<h2>Use the form to filter the Behandling entities</h2>
        <form id="bhand-form">

          <div class="lastminute-selectbox for">
				<h4>Til</h4>
				<select class="select-category">
					<option value="All">Any</option>
					<option value="1">Kvinde</option>
					<option value="2">Mand</option>
					<option value="3">Par</option>
				</select>
          </div>
          
            <div class="lastminute-selectbox radius">
                <h4>Område radius</h4>
                <select id ="select-distance" class="select-distance">
                    <option value="5">0-5 km</option>
                    <option value="10">5-10 km</option>
                    <option value="15">10-15 km</option>
                    <option value="20">15-20 km</option>
                </select>
            </div>
          
			<div class="lastminute-selectbox treatment category">
				<h4>Vælg behandling</h4>
				<select class="select-rating" id ="select1">
					<?php echo $kategori_options; ?>
				</select>
			</div>

          <div class="lastminute-selectbox location">
            <h4>Sted</h4>
			<input id="place" onfocus="if(this.value == 'Indtast by eller postnr.') { this.value = '';}" type="text" value="Indtast by eller postnr.">
          </div>
            <input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>
          <div class="clear"></div>

        </form>
      </div>
	
		<div class="behandling-container">
			<?php print render($page['content']); ?>
		</div>
		<div class="clear"></div>
 </div> <!--Same blog post Footer-->

    <div class="clear-float"></div>

    <div class="blogpost-footer">
      <div class="footer-nav">
        <?php print render($page['footer_menu']); ?>
      </div>
      <div class="copyrights"><?php print render($page['copyrights']); ?></div>
    </div><!-- /.Same blog post Footer-->
  </div>
</div>

	<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function() {
		jQuery('.lastminut-form h2').hide();
		var stickyNavTop = jQuery('.lastminut-form').offset().top;

		var stickyNav = function(){
			var scrollTop = jQuery(window).scrollTop();

			if (scrollTop > stickyNavTop) { 
				jQuery('.lastminut-form').addClass('highlighted');
				jQuery('.lastminut-form h2').show('slow');
			} else {
				jQuery('.lastminut-form').removeClass('highlighted'); 
				jQuery('.lastminut-form h2').hide('fast');
			}
		};

		stickyNav();

		jQuery(window).scroll(function() {
			stickyNav();
		});
		
		jQuery( ".select-category" ).change(function() {
		//alert(jQuery(this).val());
		jQuery( "#edit-field-item-gender-value" ).val(jQuery(this).val());
		jQuery( "#edit-field-item-gender-value" ).trigger("change");
		//jQuery("#views-exposed-form-clone-of-find-behandling-page").submit();
		});
		jQuery( ".select-rating" ).change(function() {
		alert("test");
		jQuery( "#edit-field-item-category-tid" ).val(jQuery(this).val());
		jQuery( "#edit-field-item-category-tid" ).trigger("change");
		});
		jQuery( "#place" ).keyup(function() {
		
		jQuery( "#edit-field-geofield-distance-origin" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-origin" ).trigger("keyup");
		});
		
		//jQuery( "#place" ).val("india");
		
		// if(jQuery( "#place" ).val() != "")
		// {
		// jQuery( "#edit-field-geofield-distance-origin" ).val(jQuery( "#place" ).val());
		// jQuery( "#edit-field-geofield-distance-origin" ).trigger("keyup");
		// }
		jQuery( ".select-distance" ).change(function() {
		jQuery( "#edit-field-geofield-distance-distance2" ).val(jQuery(this).val());
		jQuery( "#edit-field-geofield-distance-distance2" ).trigger("keyup");
		});
	});
	</script>
<script type="text/javascript">
    jQuery(function(){
	  jQuery('.select-category').selectric();
	  jQuery('.select-distance').selectric();
	  jQuery('.select-rating').selectric();
	});
</script>

<script type="text/javascript">
   jQuery('#bhand-form').submit(function (evt) {
        evt.preventDefault();
       var place = jQuery('#place').val();
    if (place == '' || place == 'Indtast by eller postnr.') {

      return false;
    } else {
      var category = jQuery('.select-rating option:selected').val();
      var radius = jQuery('.select-distance option:selected').val();
      var kon = jQuery('.select-category option:selected').val();
      var place = jQuery('#place').val();

      var distancefrom;
      var distanceto;
      var sorter;
      var unit = 6371;

      switch (radius) {
        case '0':
          distancefrom = 1;
          distanceto = 5;
          break;
        case '1':
          distancefrom = 5;
          distanceto = 10;
          break;
        case '2':
          distancefrom = 10;
          distanceto = 15;
          break;
        case '3':
          distancefrom = 15;
          distanceto = 20;
          break;
      }

      switch (kon) {
        case '1':
          kon_val = '1';
          break;
        case '2':
          kon_val = '2';
          break;
        case '3':
          kon_val = '3';
          break;
        default:
          kon_val = 'Any';
          break;
      }

      var theUrl1 = "<?php echo $base_url; ?>/find-behandling";
      var extraParameters1 = {
        'field_item_gender_value': kon_val,
        'field_clinic_loc_category_tid': category,
        'field_geofield_distance[distance]': distancefrom,
        'field_geofield_distance2[distance2]': distanceto,
        'field_geofield_distance[unit]': unit,
        'field_geofield_distance[origin]': place
      };
      window.location = getURL(theUrl1, extraParameters1);
    }
    });

  function getURL(theUrl, extraParameters) {
    var extraParametersEncoded = jQuery.param(extraParameters);
    var seperator = theUrl.indexOf('?') == -1 ? "?" : "&";
    return(theUrl + seperator + extraParametersEncoded);
  }
</script>

<script type="text/javascript">

	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});


</script>


<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>


<?php if($device == "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>
