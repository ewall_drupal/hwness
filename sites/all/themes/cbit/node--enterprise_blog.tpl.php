<?php
$nid = $node->nid;
$node_details = $node;
$author = $author_details = user_load($node->uid);
$uid = $author->uid;
global $base_url;
global $user;
// echo "<pre>";print_r($node_details);exit;
$date = date('d F Y', $node_details->created);
$date_1 = date('H.i', $node_details->created);
$path = $node_details->field_image['und'][0]['uri'];
$node_thumbnail_url = image_style_url('blog', $path);

$user_imgurl = $author_details->field_enterprise_blog_picture['und'][0]['uri'];
$user_thumbnail_url = image_style_url('blog_profile', $user_imgurl);

$user_uid = $author->uid;

$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();

$device = check_device();

	
 if(is_numeric($user_uid)){
	//$uid = $term_data[$key]['uid'];
	$iuid = $author->uid;
	 }else {
		 $user_array = str_replace("<a href=\"","",$user_uid);
		 $user_dts = explode(">",$user_array);
		 $iuid = $user_dts[1];
	 }
 
 		 $iuser = user_load($iuid);
 		 //echo "<pre>";print_r($user);exit;
		$img_path = $iuser->picture;
		if($img_path){
			$img_path = $iuser->picture->uri;
			$images = substr($img_path,'9'); 
			$user_thumbnail_url = image_style_url('blog_profile', $images );
		}else if(isset($author_details->field_enterprise_blog_picture['und'])){
			$user_imgurl = $author_details->field_enterprise_blog_picture['und'][0]['uri'];
			$user_thumbnail_url = image_style_url('blog_profile', $user_imgurl);
			//$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}else{
			$user_thumbnail_url = base_path().path_to_theme().'/images/default_profile.jpg';
		}
 

//$user_thumbnail_url = get_user_image($uid);

$blog_gallery = $node_details->field_blog_gallery['und'];
$blog_categories = $node_details->field_categories['und'];
$main_category = $node_details->field_categories['und'][0]['taxonomy_term']->name;
$cat_count = count($blog_categories);
$share_url = $base_url.'/'.drupal_get_path_alias('node/'.$nid);

_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
?>
<?php 
	$detect = mobile_detect_get_object();
	$is_mobile = $detect->isMobile();
	if($device == "mobile"){
		$class="mobile-detail-page";
	}
?>
<div class="blogpost-left <?php echo $class; ?>"><!-- left content-->
	<div class="blogpost-left-content">
        <div class="main-category">
            <span class="category-name label"><?php echo $main_category; ?></span>
            <img src="<?php echo $node_thumbnail_url ?>">
        </div>
		<div class="detail-page-content">
			<div class="blogpost-info">
				<p><?php print $node_details->title ?></p>
			</div>
			<div class="blogpost-author-content">
				<div class="blogpost-author">
					<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$author_details->uid); ?>"><img src="<?php echo $user_thumbnail_url ?>" align="left" vertical-align="middle" style="border-radius:50%; width:44px; height:44px;"></a>
				</div>
				<div class="blogpost-author-details">
					<p><?php echo $date ?>&nbsp;<span class="blog-post-time post-time"><?php echo $date_1; ?></span><br> <?php echo l(t($author_details->name), 'user/'.$author_details->uid) ?></p>
				</div>
				<div class="blogpost-author-likes">
					<?php //print drupal_render(plus1_build_node_jquery_widget($nid, $tag = 'plus1_node_vote')); ?>
					<?php  print like_widget_node($nid); ?>
				</div>
				<div class="blogpost-author-comments">
					<p><a href="<?php echo base_path().'node/'.$nid.'#comment-list'; ?>"><span class="blog-commentsicon"></span><?php echo $node_details->comment_count >1 ? $node_details->comment_count.' kommentarer' : $node_details->comment_count.' kommentarer' ?></a></p>
				</div>
				<div class="clear-float"></div>
			</div>
			<div class="blogpost-article">
				<?php print $node_details->body['und'][0]['safe_value'] ?>
			</div>
			<?php
				if(count($blog_gallery) > 0){
			?>
			<div class="blogpost-thumbnail">
				<ul>
					<?php 
						foreach($blog_gallery as $key => $val){
							$blog_gallery_url = $blog_gallery[$key]['uri'];
							$gallery_thumbnail_url = image_style_url('blog_gallery', $blog_gallery_url);
							$image = substr($blog_gallery_url, 9);
					?>
						<li>
							<a href="<?php print base_path().'sites/default/files/crop/'.$image ?>" rel="lightbox[gallery]">
								<img src="<?php print $gallery_thumbnail_url ?>" alt="img" />
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
			<div class="clear-float"></div>
			<div class="blogpost-author-content">
				<div class="blogpost-author">
					<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$author_details->uid); ?>"><img src="<?php echo $user_thumbnail_url ?>" align="left" vertical-align="middle" style="border-radius:50%; width:44px; height:44px;"></a>
				</div>
				<div class="blogpost-author-details">
					<p><?php echo $date ?>&nbsp;<span class="blog-post-time post-time"><?php echo $date_1; ?></span><br> <?php echo l(t($author_details->name), 'user/'.$author_details->uid) ?></p>
				</div>
				<div class="blogpost-author-likes">
					<?php //print drupal_render(plus1_build_node_jquery_widget($nid, $tag = 'plus1_node_vote')); ?>
					<?php  print like_widget_node($nid); ?>
				</div>
				<div class="blogpost-author-comments">
					<p><a href="<?php echo base_path().'node/'.$nid.'#comment-list'; ?>"><span class="blog-commentsicon"></span><?php echo $node_details->comment_count >1 ? $node_details->comment_count.' kommentarer' : $node_details->comment_count.' kommentarer' ?></a></p>
				</div>
			</div>
			<div class="blogpost-clink">
				<h4>Kategorier</h4>
				<ul class="clink">
					<?php foreach($blog_categories as $key => $val){ ?>
						<?php if($cat_count=='1') { ?>
							<li>
								<?php echo l(t($blog_categories[$key]['taxonomy_term']->name), 'blog/categories/'.$blog_categories[$key]['taxonomy_term']->tid); ?>
							</li>
						<?php } else if($key==($cat_count-1)){ ?>
							<li>
								<?php echo l(t($blog_categories[$key]['taxonomy_term']->name), 'blog/categories/'.$blog_categories[$key]['taxonomy_term']->tid); ?>
							</li>
						<?php } else { ?>
							<li>
								<?php echo l(t($blog_categories[$key]['taxonomy_term']->name.','), 'blog/categories/'.$blog_categories[$key]['taxonomy_term']->tid); ?>
							</li>
						<?php } ?>
							
					<?php } ?>
				</ul>
				<ul class="blogpost-social">
					<li>
						<a id="ref_fb"  href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $node_details->title; ?>&amp;p[summary]=<?php print strip_tags($node_details->body['und'][0]['safe_value']) ?>&amp;p[url]=<?php echo urlencode($share_url);?>&amp;p[images][0]=<?php echo urlencode($node_thumbnail_url);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600'); return false;">
							<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-facbook.png">
						</a>
					</li>
					<li>
						<a id="ref_tw" href="http://twitter.com/home?status=<?php echo $node_details->title; ?>+<?php echo urlencode($share_url);?>"  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
							<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-twitter.png">
						</a>
					</li>
					<li>
						<a id="ref_pr" href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo urlencode($node_thumbnail_url);?>&amp;url=<?php echo urlencode($share_url);?>&amp;is_video=false&amp;description=<?php print strip_tags($node_details->body['und'][0]['safe_value']) ?>" onclick="javascript:window.open(this.href, '_blank', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false;">
							<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-pinterest.png">
						</a>
					</li>
					<li>
						<a id="ref_gp" href="https://plus.google.com/share?url=<?php echo urlencode($share_url);?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');return false">
							<img src="<?php echo base_path().path_to_theme(); ?>/images/blogpost-g+.png">
						</a>
					</li>
				</ul>
				<div class="clear-float"></div>
			</div>
		</div>
	</div>
	<div class="clear-float"></div>
	<div class="blogpost-Commentpost" id="comment-list">
		<div class="blogcomment-title">
			<h1>Kommentér indlæg</h1>
            <?php
			  if ($user->uid == 0) {
				if($device == "mobile") {
					print '<div class="review"><span class="write-review">'.l('Skriv kommentarer', $base_url.'/user/login').'</span></div>';
			    }
				else {
					print '<div class="review"><span class="write-review">'.l('Skriv kommentarer', 'ajax_register/login/nojs', $options).'</span></div>';
			    }
			  } else {
				print '<div class="review"><span class="write-review">'.t('Skriv kommentarer').'</span></div>';
			  }
			  ?>

		</div>
		<?php
			print render($content['comments']['comment_form']);
			$comment_block = module_invoke('views', 'block_view', 'node_comments-block');
			print render($comment_block['content']);
		?>
	</div>
</div><!-- /. left content-->
<div class="blogpost-sidebar"><!-- side bar -->
	<!--<div class="blogpost-login">
		<h1>book <br>samme<br> behandling</h1>
		<h5>som i artiklen</h5>
		<form action ="<?php //echo $GLOBALS['base_url'].'/last-minute/'.$blog_categories[0]['taxonomy_term']->tid; ?>">
			 <input type="submit" value="Søg efter ledige behandlinger" class="btn-treatments"/>
		</form>
	</div>-->
	<div class="clear-float"></div>
	<div class="sidebar-content">
		<h1><?php print t('mest populære') ?></h1>
		<?php
			$popular_blog = module_invoke('cbit', 'block_view', 'cbit_blog_category_popular');
			print render($popular_blog['content']);
		?>
	</div>
	<div class="blogpost-categories">
		<div class="blog-selectbox">
			<h1><?php print t('KATEGORI') ?></h1>
			<label>
				<?php
					$jump_menu_block = module_invoke('views', 'block_view', 'treatment_category-block_3');
					print render($jump_menu_block['content']);
				?>
			</label>
		</div>
	</div>
	<div class="clear-float"></div>
	
	<div class="blogpost-sidegrid">
		<div class="blog-sidegrid-item">
			<h1>relaterede</h1>
			<?php
				$related = module_invoke('views', 'block_view', 'blog_categories-block_3');
				print render($related['content']);
			?>
			
		</div>
	</div><!-- blogpost-sidegrid -->
</div><!-- /. side bar -->
<script type="text/javascript">
jQuery('#comment-form #edit-submit').click(function(){
	var subject = jQuery('#edit-subject').val();
	var comment = jQuery('#comment-body-add-more-wrapper textarea').val();
	if(comment==''){
		jQuery('#comment-body-add-more-wrapper textarea').addClass('error');
		return false;
	} else {
		jQuery('#comment-body-add-more-wrapper textarea').removeClass('error');
	}
	
});
</script>
