<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$term_data = $view->style_plugin->rendered_fields;
//echo '<pre>';print_r($term_data);exit;


	
$i=0;
?>

<?php foreach ($term_data as $key => $value) {
	
	$nid1 = $term_data[$key]['nid'];
	$nid2 = $term_data[$key]['nid_1'];
	$readmore_nid = $nid1.$nid2;
	
	 ?>	
	<div class="lastminut-block muruga<?php echo $key; ?>">
		<div class="lastmint-item sted">
		  <div class="lastmint-product">
			  <h6>STED</h6>
			<h1><?php echo $term_data[$key]['title_1']; ?></h1>
			<div class="behandling-address">
				<?php echo $term_data[$key]['field_location_address']; ?>
			</div>
			<div class="lasatmint-rating"><img src="<?php echo base_path() . path_to_theme(); ?>/images/stars.png"></div>
		  </div>
		</div>

		<div class="lastmint-item btype">
		  <div class="lastmint-details">
			<h6>BEHANDLINGSTYPE</h6>

			<h1><?php echo $term_data[$key]['field_item_category']; ?></h1>
			<div id ="find-short-<?php echo $readmore_nid; ?>" class="find-short">
				<p><?php echo $term_data[$key]['field_location_shortdesc_1']; ?></p>
				<a class="read-more" rel="<?php echo $readmore_nid; ?>" href="javascript:void(0);"><?php echo t('Læs mere'); ?></a>
			</div>	
			<div id ="find-long-<?php echo $readmore_nid; ?>" class="find-long">
				<p><?php echo $term_data[$key]['field_location_shortdesc']; ?></p>
				<a class="read-less" rel="<?php echo $readmore_nid; ?>" href="javascript:void(0);"><?php echo t('Less'); ?></a>
			</div>
			
		  </div>
		</div>

		<div class="lastmint-item bprice">
		  <div class="lastmint-price">
			<p>Tid:<?php echo $term_data[$key]['field_item_duration']; ?> min</p>

			<h1>Pris: <?php echo $term_data[$key]['field_item_price']; ?> kr.</h1>
			<input class="btn-book" type="submit" value="BOOK NU">
			
			<div class="clear-float"></div>
		  </div>
		</div>
        <div class="clear-float"></div>
	</div>
<div class="clear-float"></div>
  <?php } ?>

