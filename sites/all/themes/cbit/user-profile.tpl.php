<?php /** * @file * Default theme implementation to present all user profile data. * * This template is used when viewing a registered member 's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture ']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary '] is provided, which contains data on the user's * history. Other data can be included by modules. $user_profile[ 'user_picture'] * is available for showing the account picture. * * Available variables: * - $user_profile: An array of profile items. Use render() to print them. * - Field variables: for each field instance attached to the user a * corresponding variable is defined; e.g., $account->field_example has a * variable $field_example defined. When needing to access a field's raw * values, developers/themers are strongly encouraged to use these * variables. Otherwise they will have to explicitly specify the desired * field language, e.g. $account->field_example['en'], thus overriding any * language negotiation rule that was previously applied. * * @see user-profile-category.tpl.php * Where the html is handled for the group. * @see user-profile-item.tpl.php * Where the html is handled for each item in the group. * @see template_preprocess_user_profile() * * @ingroup themeable */ global $base_url; global $user; _ajax_register_include_modal(); $classes = array(); $classes[] = 'ctools-use-modal'; $classes[] = 'ctools-modal-ctools-ajax-register-style'; $options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow')); $uid = arg(1); $account = user_load($uid); // echo '<pre>';print_r($account);exit;
//echo '<pre>'; print_r($account); exit;

/*
$profession = $account->field_profession;
if(isset($profession[LANGUAGE_NONE])){
	foreach($profession[LANGUAGE_NONE] as $key => $value){
		$professions[] = $profession[LANGUAGE_NONE][$key]['taxonomy_term']->name;
	}
	$prof = implode('  //  ', $professions);
}*/

$detect = mobile_detect_get_object();
$is_mobile = $detect->isMobile();


if(isset($account->field_user_description['und'][0]['value'])){
	$description = $account->field_user_description['und'][0]['value'];
}

$username = $account->name;
$firstname = $tabname = $lastname = $bannercaption = $family = $age = $favorite_training = $live_in = $fav_treatments = $fav_foods = $i_love = $coverimage = '';
if(isset($account->field_enterprise_blog_firstname['und'][0]['value'])){
	$firstname = $account->field_enterprise_blog_firstname['und'][0]['value'];
	$tabname = $firstname;
}else{
	$tabname = $username;
}
if(isset($account->field_enterprise_blog_lastname['und'][0]['value'])){
	$lastname = $account->field_enterprise_blog_lastname['und'][0]['value'];
}
if($firstname && $lastname) {
	$bannercaption = $firstname.'<br>'.$lastname;
	$om_bannercaption = $firstname.' '.$lastname;
}
if(isset($account->field_family['und'][0]['value'])){
	$family = $account->field_family['und'][0]['value'];
}
if(isset($account->field_age['und'][0]['value'])){
	$age = $account->field_age['und'][0]['value'];
}
if(isset($account->field_favorite_traning['und'][0]['value'])){
	$favorite_training = $account->field_favorite_traning['und'][0]['value'];
}
if(isset($account->field_i_live_in['und'][0]['value'])){
	$live_in = $account->field_i_live_in['und'][0]['value'];
} 
if(isset($account->field_favorite_treatments['und'][0]['value'])){
	$fav_treatments = $account->field_favorite_treatments['und'][0]['value'];
}
if(isset($account->field_favorite_food['und'][0]['value'])){
	$fav_foods = $account->field_favorite_food['und'][0]['value'];
}
if(isset($account->field_i_love['und'][0]['value'])){
	$i_love = $account->field_i_love['und'][0]['value'];
} 
if(isset($account->field_cover_image['und'][0]['uri'])){
	// $coverimage = image_style_url('profile_cover', $account->field_cover_image['und'][0]['uri']);
	$img_path = $account->field_cover_image['und'][0]['uri'];
	$images = substr($img_path,'9');
	$coverimage = base_path().'sites/default/files/crop/'.$images;
}
?>

<?php if (isset($account->roles[5]) && $account->roles[5] == 'vip') { 
	if($is_mobile){ ?> 
	<?php
  $img_path = $account->field_enterprise_blog_picture['und'][0]['uri'];
  $user_image = $base_url . '/sites/default/files/crop/' . substr($img_path, '9');
  // $product_thumb_image = image_style_url('deal_full', $images);
  ?>
		<div class="profile-left user-profile-page">
			<div class="left-content">
				<div class="left-container">
					<span class="image-container">
		<span class="clip_mask">
		<?php
		$user_uid = $uid;
		 if(is_numeric($user_uid)){
			$iuid = $uid;
		 }else {
			 $user_array = str_replace("<a href=\"","",$user_uid);
			 $user_dts = explode(">",$user_array);
			 $iuid = $user_dts[1];
		 }

		?>
			<img style="border-radius:50%; width:44px; height:44px; top:0px; left:0px;" src="<?php echo get_user_image($iuid); ?>"></span>
				</span>

				<div class="details-container">
					<b><?php echo ucfirst($account->name); ?></b>
					<?php if($user->uid == $uid) { ?>
					<br>
					<span class="profile-date"><?php echo l('Rediger', 'user/'.$uid.'/edit'); ?></span> | <span class="profile-date"><?php echo l('Mine bookninger', 'user/'.$uid); ?></span>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="left-content">
			<div class="left-container">
				<span class="image-container-icon"><img src="<?php echo base_path() . path_to_theme(); ?>/images/fav-profile.png"></span>
				<span class="fav-container"><b><?php echo _user_anmeldelser_count($uid); ?> anmeldelser</b></span>
			</div>
		</div>
		<div class="left-like">
			<span class="image-container-icon"><img src="<?php echo base_path() . path_to_theme(); ?>/images/profile-like.png"></span>
			<span class="fav-container">
		<b><?php echo ucfirst($account->name); ?> likes</b><br>
		<ul class="user-likes-data">
		<li><?php echo plus1_get_user_node_score('enterprise_blog', $uid); ?> blogindlæg</li>
		<li><?php echo plus1_get_user_node_score('trends', $uid); ?> trends</li>
		<li><?php echo plus1_get_user_comment_score($uid); ?> anmeldelser</li>
		<li><?php echo _follow_count($uid); ?> bloggere</li>
		</ul>
		</span>
		</div>
		</div>

		<div class="profile-right">
			<?php $block = module_invoke( 'views', 'block_view', 'user_comments-block');
			print render($block[ 'content']); ?>
		</div>
	<?php } else {?>
  <!-- FOR VIP ROLE USERS -->
  <div class="container-freewall-queens">
		<div class="queens-banner">
			<img src="<?php echo $coverimage; ?>" width="100%"/>
			<!--<img src="<?php //echo base_path() . path_to_theme(); ?>/images/queens-banner.png">-->
			<div class="banner-captions">
				<h1>
					<?php if($bannercaption) {
							echo ucfirst($bannercaption);
						}	
						else {
							echo ucfirst($username);
						} 
					?>
				</h1>
				<div class="queens-title queens-professions">
					<?php 
						if($description){
							echo $description;
						}
					?>
				</div>
				<div class="blogsnfollow">
					<?php echo _user_node_count($uid); ?> BLOGINDLÆG // <?php echo _followers_count($uid); ?> FØLGERE
					<?php
					//echo"<pre>";print_r($user);exit;
					$roles = $user->roles['5'];
					if($roles){
					?>
					 <p class="profile-date"><?php echo l('Manage Blogs & ARTIKLER', 'manage-artikler-blog-post'); ?></p>
					 <?php } ?>
				</div>
				
				<div class="blogsnfollow">
					
				</div>
				
				<div class="queens-socialicons">
					<?php if(($user->uid) && ($user->uid == arg(1))) { ?>
						<a href="<?php echo $base_url.'/user/'.$user->uid.'/edit'; ?>" title="Edit Account">
							<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/edit-icon.png" >
						</a>
					<?php } ?>
					<?php if(isset($account->field_facebook_link[LANGUAGE_NONE][0]['url'])) { ?>
						<a href="<?php echo $account->field_facebook_link[LANGUAGE_NONE][0]['url']; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/queens-facebook.png"></a>
					<?php } ?>
					<?php if(isset($account->field_instagram_link['und'][0]['url'])) { ?>
						<a href="<?php echo $account->field_instagram_link['und'][0]['url']; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/queens-instagram.png"></a>
					<?php } ?>
					<?php if(isset($account->field_youtube_link[LANGUAGE_NONE][0]['url'])) { ?>
						<a href="<?php echo $account->field_youtube_link[LANGUAGE_NONE][0]['url']; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/queens-youtube.png"></a>
					<?php } ?>
				</div>
			</div>
			<div class="banner-overlay"></div>
		</div>
	</div>
	<div class="clear-float"></div>
	<div class="container-freewall-queens">
		<div class="queens-container welreviews">	
			<div id="tab-container"  class='tab-container'>
				<ul class='tabs'>
					<li ><a href="#tab1" class="active">Seneste indlæg</a></li>
					 <li><a href="#tab2" data-toggle="tab"><?php echo $tabname; ?><span class="queen-like">wellness</span></a></li>
					<li><a href="#tab3"><?php echo t("Om mig"); ?></a></li>
					<li><a href="#tab4"><?php echo t("anmeldelser"); ?></a></li>
				</ul>
				<?php if($user->uid != arg(1)) { ?>
					<div class="queen-follow">
						<?php
						if ($user->uid == 0) {
						  print '<span class="follow-title anonymous">' . l('FØLG MIG', 'ajax_register/login/nojs', $options) . '</span>';
						} else {
						  print '<span class="follow-title">' . print flag_create_link('follow', $uid) . '</span>';
						}
						?>
					</div>
				<?php } ?>
				<div id="tab1">
					
						<?php
							$block = module_invoke('views', 'block_view', 'vip_contents-block');
							$vip_content_block = render($block['content']);
							if($vip_content_block != '') { ?>
							<div class="free-wall" id="freewall2">
								<?php print $vip_content_block; ?>
							</div>
							<?php } else { 
								print "<div class='vip-content-empty'>".t('No content posted yet by this user.')."</div>";
							}
						?>
					
				</div>
				<div id="tab2">
					<div class="free-wall" id="freewall3">
						<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
							<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
							<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
							<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
							<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
							<div class="brick">
							<div class="grid-item-01">
							<a href="#"><img src="<?php echo base_path() . path_to_theme(); ?>/images/tab2-grid.jpg" width="100%"></a>
							<!--first grid-->
								<div class="item-01-info">
									<h2>Yndlings frisor</h2>
									<hr>
									<h1>CHA CHA CHA</h1>
									<p>Lorem ipsum dolor sit amet, <br> consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
		<div id="tab4">		
		<?php
			$block = module_invoke('views', 'block_view', 'user_comments-block_1');
			$vip_user_comments_block = render($block['content']);
			if($vip_user_comments_block != '') { ?>
			<div class="free-wall" id="freewall2">
				<?php print $vip_user_comments_block; ?>
			</div>
			<?php } else { 
				print "<div class='vip-content-empty'>".t('Nos contents posted yet by this user.')."</div>";
			}
		?>			
	</div>			
				
				<div id="tab3">
					<div class="qp-tab-container">
						<table>
							<tr>
							<td class="page-usr-left" width="30%" style="background:#fff; padding:20px;padding-top:25px; text-align:left; vertical-align:top;">
								<table class="user-details">
									<h2><?php echo t('Oplysninger') ?></h2>
										<tr>
										<th><?php print t('Jeg bor i') ?></th>
										<td>
										<?php if($live_in) {
												echo $live_in;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Familie') ?></th>
										<td>
										<?php if($family) {
												echo $family;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Alder') ?></th>
										<td>
											<?php if($age) {
												echo $age;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Yndlings træning') ?></th>
										<td>
											<?php if($favorite_training) {
												echo $favorite_training;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Favorit behandlinger') ?></th>
										<td>
											<?php if($fav_treatments) {
												echo $fav_treatments;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Bedste livret') ?></th>
										<td>
											<?php if($fav_foods) {
												echo $fav_foods;
											}	
										?>
										</td>
										</tr>
										<tr>
										<th><?php print t('Jeg elsker') ?></th>
										<td>
											<?php if($i_love) {
												echo $i_love;
											}	
										?>
										</td>
										</tr>
								</table>
							</td>
							<td width="1%"></td>
							<td class="page-usr-right" width="60%" style="background:#fff; padding:25px; vertical-align: top;">
							<table class="user-desc">
								<tr>
									<td>
										<h2>
											<?php if($om_bannercaption) {
													echo 'Om '.ucfirst($om_bannercaption);
												}	
												else {
													echo 'Om '.ucfirst($username);
												} 
											?>
										</h2>
										<?php 
											if(isset($account->field_about_me[LANGUAGE_NONE][0]['value'])) {
												echo $account->field_about_me[LANGUAGE_NONE][0]['value'];
											}
										?>										
									</td>
								</tr>
							</table>
							</td>
							</tr>
						</table>
					</div>			
				</div>
			</div>	 
		</div>
		<div class="clear-float"></div>
	</div>

<?php } } else { ?>
  <!-- FOR AUTHENTICATED ROLE USERS -->

  <?php
  $img_path = $account->field_enterprise_blog_picture['und'][0]['uri'];
  $user_image = $base_url . '/sites/default/files/crop/' . substr($img_path, '9');
  // $product_thumb_image = image_style_url('deal_full', $images);
  ?>
<div class="profile-left user-profile-page">
    <div class="left-content">
        <div class="left-container">
            <span class="image-container">
<span class="clip_mask">
<?php
$user_uid = $uid;
 if(is_numeric($user_uid)){
	$iuid = $uid;
 }else {
	 $user_array = str_replace("<a href=\"","",$user_uid);
	 $user_dts = explode(">",$user_array);
	 $iuid = $user_dts[1];
 }

?>
    <img style="border-radius:50%; width:44px; height:44px; top:0px; left:0px;" src="<?php echo get_user_image($iuid); ?>"></span>
        </span>

        <div class="details-container">
            <b><?php echo ucfirst($account->name); ?></b>
            <?php if($user->uid == $uid) { ?>
            <br>
            <span class="profile-date"><?php echo l('Rediger', 
            'user/'.$uid.'/edit'); ?></span> | <span class="profile-date"><?php echo l('Mine bookninger', 'user/'.$uid); ?></span>
            <?php } ?>
        </div>
    </div>
</div>
<div class="left-content">
    <div class="left-container">
        <span class="image-container-icon"><img src="<?php echo base_path() . path_to_theme(); ?>/images/fav-profile.png"></span>
        <span class="fav-container"><b><?php echo _user_anmeldelser_count($uid); ?> anmeldelser</b></span>
    </div>
</div>
<div class="left-like">
    <span class="image-container-icon"><img src="<?php echo base_path() . path_to_theme(); ?>/images/profile-like.png"></span>
    <span class="fav-container">
<b><?php echo ucfirst($account->name); ?> likes</b><br>
<ul class="user-likes-data">
<li><?php echo plus1_get_user_node_score('enterprise_blog', $uid); ?> blogindlæg</li>
<li><?php echo plus1_get_user_node_score('trends', $uid); ?> trends</li>
<li><?php echo plus1_get_user_comment_score($uid); ?> anmeldelser</li>
<li><?php echo _follow_count($uid); ?> bloggere</li>
</ul>
</span>
</div>
</div>

<div class="profile-right">
    <?php $block = module_invoke( 'views', 'block_view', 'user_comments-block');
    print render($block[ 'content']); ?>
</div>
<?php } ?>



