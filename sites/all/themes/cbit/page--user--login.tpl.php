<?php
global $user;
global $base_url;
$arg0 = arg(0);
$arg1 = arg(1);
$arg2 = arg(2);

if ($arg0=='user' ) {
    if (($arg1 !='' ) && (is_numeric($arg1))) {
        $page_name='user-profile' ;
    }
}
$account = user_load($arg1);
_ajax_register_include_modal();
$classes=array();
$classes[]='ctools-use-modal' ;
$classes[]='ctools-modal-ctools-ajax-register-style' ;
$options=array( 'attributes'=>array('class' => $classes, 'rel' => 'nofollow'));
?>

<?php 
	// $detect = mobile_detect_get_object();
	// $is_mobile = $detect->isMobile();
	$device = check_device();
	
?>

<link href="<?php echo base_path() . path_to_theme(); ?>/css/tab.css" rel="stylesheet" />

<?php if($device != "mobile") { ?>

<nav class="header">
    <!--main-menu-->
    <div class="main-menu">
        <div class="logo">
            <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
            <?php endif; ?>
        </div>
        <div class="top-link">
            <ul class="top-bar">
                <li class="first" style="padding-left:0px;">
                    <div id="slidediv">
                        <?php print $search_box; ?>
                    </div>
                    <a href="#" class="search-btn">
                    <img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
                    </a>
                </li>
                <li class="second" style="padding-left:0px;">
                    <div id="slidediv1">
                        <?php print render($page[ 'subscribe']); ?>
                    </div>
                    <?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
                </li>

                <li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a>
                </li>
                <li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a>
                </li>
                <li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a>
                </li>
                <li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a>
                </li>
                <li class="usr-login">
                    <?php if ($user->uid == 0) { print l('LOGIN', 'ajax_register/login/nojs', $options); } else { print l($user->name, 'user/' . $user->uid); } ?>
                </li>
                <li class="usr-logout">
                    <?php if ($user->uid == 0) { print l('OPRET KONTO', 'ajax_register/register/nojs', $options); } else { print l('LOG UD', 'user/logout'); } ?>
                </li>
            </ul>
        </div>
        <div class="navigation-bar">
            <?php print render($page[ 'top_menu']); ?>
        </div>
    </div>
</nav>
<?php } ?>
<!-- /. end main-menu -->
<?php if($device == "mobile") { ?>
	<nav id="menu">

	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET PROFIL', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			<!-- <li><a title="" href="<?php //echo base_path(); ?>/last-minute">OPRET PROFIL</a></li>-->
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>
	<!--===========================Mobile Menu  End=========================*/-->

<div class="main-content-container container login-inner">
	  
<?php if($device == "mobile") { ?>	
	<div class="logo">
		<a href="#" class="menu-link-arrow">Menu</a>
		<a id="logo" rel="home" title="Home" href="<?php echo base_path(); ?>">
			<img alt="Home" src="<?php echo base_path(); ?>/sites/default/files/crop//logo.png">
		</a> 
	    <a href="#menu" class="menu-link">Menu</a>
	</div>

	<?php if($uid == 0) { ?>
	<?php if(arg(0) == "user" && arg(1)=="login") { ?>
		<div class="Log Indtitle">
			<span>Log Ind</span>
		</div>
	<?php } } ?>
<?php } ?>
	
    <?php if(arg(2)=='edit' ) { ?>
    <?php if ($title): ?>
    <div class="blog-header">
        <!--banner-->
        <h1 <?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
    </div>
    <?php endif; ?>
    <?php } ?>
    <?php if (isset($account->roles[5]) && $account->roles[5] == 'vip') { ?>
    <div class="profile section-queen-profile">
        <!-- FOR USER PROFILE PAGES -->
        <div class="section-queen">
            <div class="profile-container">
                <?php if(arg(2)=='edit' ) { ?>
                    <div class="user-profile-edit">
                        <?php if ($tabs): ?>
                        <div id="tabs-wrapper" class="clearfix">
                            <?php endif; ?>
                            <?php print render($title_prefix); ?>
                            <?php print render($title_suffix); ?>
                            <?php if ($tabs): ?>
                            <?php print render($tabs); ?>
                        </div>
                        <?php endif; ?>
                        <?php print render($tabs2); ?>
                        <?php print $messages; ?>
                        <?php print render($page[ 'content']); ?>
                    </div>
                <?php } else{ ?>
                    <?php print $messages; ?>
                    <?php print render($page[ 'content']); ?>
                <?php }?>
            </div>

            <div class="footer-section">
                <div class="footer-nav">
                    <div class="container-freewall-queens queens-bottom">
                        <div class="grid-item-06" style="padding-top:25px; margin-top:80px;">
                            <?php 
								if ($user->uid == 0) {
									print render($page[ 'newsletter_form']); 
								}
							?>

                            <div class="footer-navigation">
                                <?php print render($page[ 'footer_menu']); ?>
                            </div>

                            <div class="copyrights"><?php print render($page['copyrights']); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_path() . path_to_theme(); ?>/js/freewall.js" type="text/javascript"></script>
    <script src="<?php echo base_path() . path_to_theme(); ?>/js/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.easytabs.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            var wall2 = new freewall("#freewall2");
            wall2.reset({
                selector: '.brick',
                animate: true,
                cellW: 320,
                cellH: 'auto',
                onResize: function () {
                    wall2.fitWidth();
                }
            });
            wall2.fitWidth();
            wall2.container.find('.brick img').load(function () {
                wall2.fitWidth();
            });

        });
        jQuery(document).ready(function () {
            var wall3 = new freewall("#freewall3");
            wall3.reset({
                selector: '.brick',
                animate: true,
                cellW: 320,
                cellH: 'auto',
                onResize: function () {
                    wall3.fitWidth();
                }
            });
            wall3.fitWidth();
            wall3.container.find('.brick img').load(function () {
                wall3.fitWidth();
            });

        });
    </script>
    <script>
        // Wait until the DOM has loaded before querying the document
        jQuery(document).ready(function () {
            jQuery('ul.tabs').each(function () {
                // For each set of tabs, we want to keep track of
                // which tab is active and it's associated content
                var $active, $content, $links = jQuery(this).find('a');

                // If the location.hash matches one of the links, use that as the active tab.
                // If no match is found, use the first link as the initial active tab.
                $active = jQuery($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                $active.addClass('active');

                $content = jQuery($active[0].hash);

                // Hide the remaining content
                $links.not($active).each(function () {
                    jQuery(this.hash).hide();
                });

                // Bind the click event handler
                jQuery(this).on('click', 'a', function (e) {
                    // Make the old tab inactive.
                    $active.removeClass('active');
                    $content.hide();

                    // Update the variables with the new link and content
                    $active = jQuery(this);
                    $content = jQuery(this.hash);

                    // Make the tab active.
                    $active.addClass('active');
                    $content.show();

                    // Prevent the anchor's default click action
                    e.preventDefault();

                    var wall2 = new freewall("#freewall2");
                    wall2.reset({
                        selector: '.brick',
                        animate: true,
                        cellW: 320,
                        cellH: 'auto',
                        gutterX: 15,
                        guttery: 15,
                        onResize: function () {
                            wall2.fitWidth();
                        }
                    });
                    wall2.fitWidth();

                    var wall3 = new freewall("#freewall3");
                    wall3.reset({
                        selector: '.brick',
                        animate: true,
                        cellW: 320,
                        cellH: 'auto',
                        gutterX: 15,
                        guttery: 15,
                        onResize: function () {
                            wall3.fitWidth();
                        }
                    });
                    wall3.fitWidth();
                });
            });
        });
    </script>
    <?php } else { ?>
    <div class="profile">
        <div class="profile-container">
            <?php if(arg(2)=='edit' ) { ?>
            <div class="statisk-container user-content-bg">
                <div id="main-content">
                    <?php if ($tabs): ?>
                    <div id="tabs-wrapper" class="clearfix">
                        <?php endif; ?>
                        <?php print render($title_prefix); ?>
                        <?php if ($title): ?>
                        <h1 <?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
                        <?php endif; ?>
                        <?php print render($title_suffix); ?>
                        <?php if ($tabs): ?>
                        <?php print render($tabs); ?>
                    </div>
                    <?php endif; ?>
                    <?php print render($tabs2); ?>
                    <?php print $messages; ?>
                    <?php print render($page[ 'content']); ?>
                </div>
            </div>
            <?php } else if(!$user->uid) { ?>
				<div class="statisk-container user-content-bgs ss">
					<div id="main-content" class="anonymous-content">
						<?php if ($title): ?>
							<?php if($device != "mobile") { ?>
								<h1 <?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
							<?php } ?>
						<?php endif; ?>
						<?php print $messages; ?>
						<?php print render($page[ 'content']); ?>
					</div>
				</div>
            <?php } else {?>
            <div class="statisk-container">
                <div id="main-content">
                    <?php print $messages; ?>
                    <?php print render($page[ 'content']); ?>
                </div>
            </div>
            <?php } ?>
        </div>

        <div class="footer-section">
            <div class="footer-nav">
                <div class="container-freewall-queens queens-bottom">
                    <div class="grid-item-06" style="padding-top:25px; margin-top:80px;">
                        <?php 
							//~ if (isset($account->roles[5]) && $account->roles[5] == 'vip') { 
								//~ print render($page['newsletter_form']); 
							//~ } 
						?>

                        <div class="footer-navigation">
                            <?php print render($page[ 'footer_menu']); ?>
                        </div>

                        <div class="copyrights"><?php print render($page['copyrights']); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<script type="text/javascript">
			jQuery(".menu-link").click(function(){
				jQuery("#menu").toggleClass("active");
				jQuery(".container").toggleClass("active");
			});
		
</script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
   jQuery('a.menu-link-arrow').click(function(){
        //if(document.referrer.indexOf(window.location.hostname) != -1){
            window.history.go(-1);
            return false;
        //}
    });
});

</script>


