<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
global $user; 
$dest = drupal_get_destination();
$destination = $dest['destination'];
$term_data = $view->style_plugin->rendered_fields;
//echo '<pre>'; print_r($term_data); exit;
global $base_url;
?>

<?php foreach($term_data as $key => $value){

	$subject = $term_data[$key]['subject'];
	$comment_body = $term_data[$key]['comment_body'];
	$comment_full = $term_data[$key]['comment_body_1'];
	$nid = $term_data[$key]['nid'];
	$uid = $term_data[$key]['uid'];
	$cid = $term_data[$key]['cid'];
    $user1 = user_load($uid);
	$name = l($user1->name, 'user/'.$uid);
	$user_picture = $term_data[$key]['field_enterprise_blog_picture'];

    $iuid = $term_data[$key]['uid'];

		$user_uid = $iuid;
		if(is_numeric($user_uid)){
			$iuid = $term_data[$key]['uid'];
		}else {
			$user_array = str_replace("<a href=\"","",$user_uid);
			$user_dts = explode(">",$user_array);
			$iuid = $user_dts[1];
		}
		
		$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('thumbnail', $images );
		}else {
			$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}
		

    //$user_thumbnail_url = get_user_image($uid);

	$comment_count = $term_data[$key]['comment_count'];
	$edit_comment = $term_data[$key]['edit_comment'];
	$changed = $term_data[$key]['created'];
?>
	<div class="comments-block">
			<div class="comments-section">
				<div class="comments-left">
					<div class="user-picture">
						<a href="<?php echo $base_url.'/'.drupal_get_path_alias('user/'.$iuser->uid); ?>"><img src="<?php echo $user_thumbnail_url; ?>"></a>
					</div>
                    <div class="user-name">
						<?php echo $name; ?>
					</div>
					<div class="comment-date">
						<p><?php echo $changed ?> <span class="blog-post-time post-time"><?php echo $changed_1 ?></span></p>
					</div>
				</div>
				<div class="comments-right">
					<div class="comment-title"><?php echo $subject; ?></div>
                    <!-- <div id ="find-short-<?php echo $key; ?>" class="find-short comment-content">
						<p><?php echo $comment_body; ?></p>
						<p><a class="read-more" rel="<?php echo $key; ?>" href="javascript:void(0);"><?php echo t('Read more'); ?></a></p>
					</div> -->
					<div id ="find-long-<?php echo $key; ?>" class="find-long comment-content" style="display:block">
						<p><?php echo $comment_full; ?></p>
						<!-- <p><a class="read-less" rel="<?php echo $key; ?>" href="javascript:void(0);"><?php echo t('Less'); ?></a></p> -->
					</div>
					<div class="likes-rating">
						<div class="comment-likes">
							<?php //print drupal_render(plus1_build_node_jquery_widget($cid, $tag = 'plus1_comment_vote')); ?>
							<?php  print like_widget_comment($cid); ?>
						</div>
						<?php if($uid == $user->uid) { 
						?>
						<span class="icon-close">
							<a href="<?php echo base_path().'comment/'.$cid.'/delete-own?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/close.png" class="close-icon">Fjern</a>
						</span>
						<span class="icon-edit">
							<a href="<?php echo base_path().'comment/'.$cid.'/edit?destination='.$destination; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/edit.png" class="edit-icon">Rediger</a>
						</span>
						<?php } ?>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
	</div>
<?php } ?>
