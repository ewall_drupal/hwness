<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));
$term_data = $view->style_plugin->rendered_fields;
// echo '<pre>';
// print_r($term_data);
// exit;
/*

[nid] => 158
[cid] => 253
[uid] => 56
[tid] => 1954
[comment_body] => 6th test for share
[subject] => 6th test for share
[field_clinic_loc_rating] => 4
[changed] => 17 May 2014
[changed_1] => 15.15
[comment_count] => 9
[field_enterprise_blog_picture] => http://192.168.1.51/projects/git/heywellness/sites/default/files/crop//styles/thumbnail/public/Desert.jpg?itok=SUQIpvPf
[field_clinic_loc_category] => Kropsbehandling
[field_category_image] => http://192.168.1.51/projects/git/heywellness/sites/default/files/crop//Kropsbehandling_1.jpg
[name] => Sri Ewall
[nothing] => location/comment/253
 */
?>

<?php foreach ($term_data as $key => $value) {

     $iuid = $term_data[$key]['uid'];
		$user_uid = $iuid;
		if(is_numeric($user_uid)){
		$iuid = $term_data[$key]['uid'];
		}else {
		$user_array = str_replace("<a href=\"","",$user_uid);
		$user_dts = explode(">",$user_array);
		$iuid = $user_dts[1];
		}
		
	  	$iuser = user_load($iuid);
		$img_path = $iuser->picture;
		if($img_path){
		$img_path = $iuser->picture->uri;
		$images = substr($img_path,'9'); 
		$user_thumbnail_url = image_style_url('home_profile', $images );
		}else {
		$user_thumbnail_url =  $term_data[$key]['field_enterprise_blog_picture'];
		}	
		

	//$user_thumbnail_url = get_user_image($uid);

   /* if ($term_data[$key]['type'] == 'Advertisement') {
    ?>
    <div class="brick">
      <a href="<?php echo $term_data[$key]['field_ad_link']; ?>"><img src="<?php echo $term_data[$key]['field_ad_image']; ?>" width="100%"></a>
    </div>
  <?php } else { */ ?>
    <div class="brick">
      <div class="grid-item-01"> <!--first grid-->
        <span class="label"><?php echo $term_data[$key]['field_clinic_loc_category']; ?></span>
			<a href="<?php echo base_path().$term_data[$key]['nothing']; ?>">
				<?php if($term_data[$key]['field_category_image'] != '') { ?>
					<img src="<?php echo $term_data[$key]['field_category_image']; ?>" width="100%">
				<?php } else { ?>
					<img class="default" src="<?php echo base_path() . path_to_theme(); ?>/images/heywellness-default.jpg" width="100%">
				<?php } ?>
			</a>
        <div class="item-01-info">
          <div class="actions">
            <a class="btn btn-secondary btn-round" href="<?php echo $base_url.'/'.drupal_get_path_alias('user/' . $term_data[$key]['uid']); ?>">
              <span class="clip_mask"><img style="border-radius:50%; width:44px; height:44px; top:0px; left:0px;" src="<?php echo $user_thumbnail_url; ?>"></span>
            </a>
          </div>
					<div class="info-01">
            <p class="info-title">
							<span class="created-date"><?php echo $term_data[$key]['changed']; ?></span>&nbsp;<span class="profile-date"><?php echo $term_data[$key]['changed_1']; ?></span>
							<br>
							<span class="author-name">Af gæsteblogger <?php echo $term_data[$key]['name']; ?></span>
						</p>
            <p class="info-content"><?php echo $term_data[$key]['subject']; ?></p>
            <p class="info-details"><?php echo $term_data[$key]['comment_body']; ?><span class="readmore"><a href="<?php echo base_path().$term_data[$key]['nothing']; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/readmore.png"></a></span></p>
          </div>
        </div><hr>
          <?php print drupal_render(plus1_build_comment_jquery_widget($term_data[$key]['cid'], $tag = 'plus1_comment_vote')); ?>
        <span class="comment">
          <a href="<?php echo base_path().$term_data[$key]['nothing']; ?>"><?php echo kommentarer_count($term_data[$key][ 'cid']); ?> kommentarer</a>
        </span>
      </div>
    </div>
  <?php // }
} ?>
