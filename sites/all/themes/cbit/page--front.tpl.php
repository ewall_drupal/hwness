<?php
global $user;
global $base_url;
_ajax_register_include_modal();
$classes = array();
$classes[] = 'ctools-use-modal';
$classes[] = 'ctools-modal-ctools-ajax-register-style';
$options = array('attributes' => array('class' => $classes, 'rel' => 'nofollow'));

$device = check_device();

?>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/freewall.js"></script>
<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.selectric.min.js"></script>
<link href="<?php echo base_path() . path_to_theme(); ?>/css/jquery.bxslider.css" rel="stylesheet" />

<?php if($device == "mobile-tab") { ?>
    <link href="<?php echo base_path() . path_to_theme(); ?>/css/ipad.css" rel="stylesheet" />
<?php } ?>
 
	

	<?php if($device != "mobile") { ?>
	<nav class="header"><!--main-menu-->
		<div class="main-menu">
			<div class="logo">
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a>
				<?php endif; ?>
			</div>
			<div class="top-link">
				<ul class="top-bar">
					<li class="first" style="padding-left:0px;">
						<div id="slidediv">
							<?php print $search_box; ?>
						</div>
						<a href="#" class="search-btn">
							<img id="btnleft" src="<?php echo base_path() . path_to_theme(); ?>/images/search.png" >
						</a>
					</li>
					<li class="second" style="padding-left:0px;">
						<div id="slidediv1">
							<?php print render($page['subscribe']); ?>
						</div>
						<?php if ($user->uid == 0){ ?>
				<a href="#" class="search-btn line2">
					<img id="btnleft1" src="<?php echo base_path() . path_to_theme(); ?>/images/message.png" >
				</a>
			<?php } ?>
					</li>
					<li class="social fb"><a target="_blank" href="<?php echo variable_get('cbit_facebook', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/facebook.png"></a></li>
					<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_instagram', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/instagram.png"></a></li>
					<li class="social"><a target="_blank" href="<?php echo variable_get('cbit_youtube', ''); ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/youtube.png"></a></li>
					<li class="social google"><a target="_blank" href="<?php echo variable_get('cbit_googleplus', ''); ?>" class="line"><img src="<?php echo base_path() . path_to_theme(); ?>/images/g+.png"></a></li>
					<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOGIN', 'ajax_register/login/nojs', $options);
							} else {
								print l($user->name, 'user/' . $user->uid);
							}
						?>
					</li>
					<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET KONTO', 'ajax_register/register/nojs', $options);
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
					</li>
				</ul>
			</div>
			<div class="navigation-bar">
				<?php print render($page['top_menu']); ?>
			</div>
		</div>
	</nav><!-- /. end main-menu -->

<?php } ?>


<!--=========================== Mobile Menu Start ============================-->

<?php if($device == "mobile") { ?>
	<nav id="menu">
	   <!--<a href="#menu" class="menu-link">Menu</a>-->
	   <?php //print render($page['top_menu']); ?>
	   
	 <!-- -----Load Links Statically --------------->
	 
	<ul class="menu">
		<li class="first leaf"><a title="last minute" href="<?php echo $base_url; ?>/last-minute">LAST MINUTE</a></li>
		<li class="leaf"><a title="blog" href="<?php echo $base_url; ?>/blog">BLOG</a></li>
		<li class="leaf"><a title="artikler" href="<?php echo $base_url; ?>/artikler">ARTIKLER</a></li>
		<li class="leaf"><a title="anmeldelser" href="<?php echo $base_url; ?>/anmeldelser">ANMELDELSER</a></li>
		<?php if($user->uid) { ?>
			<li class="last leaf"><a title="min profil" href="<?php echo $base_url; ?>/user">MIN PROFIL</a></li>
		<?php } ?>
	</ul>
	<div class="login-menu-head"><h3><?php echo 'BRUGER';?></h3></div>
		<ul class="login-menu">
			<li class="usr-login">
						<?php
							if ($user->uid == 0) {
								print l('LOG IND', 'user/login');
							} else {
								print l($user->name, 'user' . $user->uid);
							}
						?>
			</li>
			<li class="usr-logout">
						<?php
							if ($user->uid == 0) {
								print l('OPRET PROFIL', 'user/register');
							} else {
								print l('LOG UD', 'user/logout');
							}
						?>
			</li>
			 <!--<li><a title="" href="<?php //echo base_path(); ?>/last-minute">OPRET PROFIL</a></li>-->
		</ul>
		<div class="login-menu-head">
			<h3><?php echo 'Heywellness';?></h3>
		</div>
		<ul class="footer-mobile">
			<li><a title="" href="<?php echo base_path().drupal_get_path_alias('node/52'); ?>">Om heywellness</a></li>
			<li><a title="Servicevilkår" href="<?php echo base_path().drupal_get_path_alias('node/54'); ?>">Servicevilkår</a></li>
			<li><a title="Fortrolighedspolitik" href="<?php echo base_path().drupal_get_path_alias('node/55'); ?>">Fortrolighedspolitik</a></li>
		</ul>
	</nav>
	
<?php } ?>

<!--=========================== Mobile Menu End ============================-->

<div class="main-content-container container">
	<div class="banner"> <!--banner-->
		<?php if($device != "mobile") { ?>
		<div class="banner-left">
			<?php print render($page['left_slider']); ?>
		</div>
		<?php } ?>
		<?php //if($is_mobile) { ?>
		  <div class="logo">
			    <a href="#" class="menu-link-arrow">Menu</a>
				<?php if ($logo): ?>
					<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
					</a> 
				<?php endif; ?>
				<a href="#menu" class="menu-link">Menu</a>
		  </div>
		<?php //} ?>
		<div class="lastmt-item">
			<h1>LAST MINUTE TILBUD</h1>
			<?php if($device == "mobile") { ?>
				<h2 class="mobile-subtitle">Søg efter last minute wellness tilbud og spar op til 70%</h2>    
			<?php } ?>
			<div class="banner-selectbox">
				<label>
					<?php
					$lastmin_tilbud_block = module_invoke('views', 'block_view', 'treatment_category-block_1');
					print render($lastmin_tilbud_block['content']);
					?>
				</label>
			</div>
			<h2>Søg efter last minute wellness tilbud og spar op til 70%</h2>
		</div>
		<?php if($device != "mobile") { ?>
		<div class="banner-right">
			<?php print render($page['right_slider']); ?>
		</div>
	<?php } ?>
	</div> <!-- /. banner -->

	<div class="slider-background"> <!-- slider-->
		<div class="clear-float"></div>
		<?php print $messages; ?>
		<div class="container-freewall0">
			<div class="title">
				<div class="heading">
				</div>
			</div>
			<h1 class="title-name">Bliv skøn nu!</h1>
		</div>
		<div class="clear-float"></div>
		
		<?php if($device == "mobile") { ?>
		<div class="container-fluid">
			<div class="col-md-12sd">
				<div class="well-none">
					<div class="mobile-vertical-slider">
						<?php 
							  $block = module_invoke('views', 'block_view', 'category_carousel-block_1');
							  print render($block['content']);
						?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="slider">
		<!-- thumbnail slider -->    
			<div class="container-fluid">
				<div class="col-md-12sd">
					<div class="well-none">
						<div id="myCarousel" class="carousel slide">
						<!-- Carousel items -->
							<div class="carousel-inner">
							  <?php
							  $block = module_invoke('views', 'block_view', 'category_carousel-block');
							  print render($block['content']);
							  ?>
							</div>
						<!--/carousel-inner-->
						</div>
					  <!--/myCarousel-->
					</div>
					<!--/well-->
				</div>
			</div>
		<!-- ./ thumbnail slider end -->
		</div>
		<div class="clear-float"></div>
	</div> <!-- /. slider -->

	<div class="section-02">
		<div class="clear-float"></div>
		<div class="container-freewall">
			<div class="title">
				<div class="heading"></div>
			</div>
			<h1 class="title-name" style="padding-bottom:41px; width:45%;">SENESTE BLOG INDLÆG</h1>
			<div id="freewall1" class="free-wall">
			  <?php print render($page['wellness_blog']); ?>
			</div>
		</div>
	</div>

	<div class="section-03"><!--section-03-->
		<div class="container-freewall-01">
			<div class="title">
				<div class="heading border">
				</div>
			</div>
			<h1 class="title-name" style="padding-bottom:10px; width:23%;">mød vores</h1>
			<h2 class="sub-title">wellness queens (& kings)</h2>
			<div id="freewall2" class="free-wall">
				<?php print render($page['wellness_users']); ?>
				<div id="sticky-info">
					<p>Følg vores <br> Wellness Bloggere, som hver uge tester og anmelder wellnessbehandlinger, produkter, spa-ophold mm.</p>
				</div>
			</div>
		</div>
	</div> <!-- /. section-03 background -->

	<div class="section-04"><!--section-03-->
		<div class="container-freewall-02">
			<div class="title">
			  <div class="heading">
			  </div>
			</div>
			<h1 class="title-name" style="width:28%;">ANMELDELSER</h1>
			<div id="freewall3" class="free-wall">
			  <?php print render($page['anmeldelser']); ?>
			</div>
		</div>
	</div> <!-- /. section-04 background -->

	<div class="section-02">
		<div class="container-freewalllast">
			<div class="title">
			  <div class="heading"></div>
			</div>
			<h1 class="title-name" style="padding-bottom:41px; width:45%;">WELLNESS TRENDS</h1>
			<div id="freewall4" class="free-wall">
				<?php print render($page['wellness_trends']); ?>
			</div>
		</div>
	</div><!-- /. section-02 background -->
	
<?php if($device != "mobile") { ?>
	<div class="section-06" style="background-color:#bcdfd1;">
		<div class="grid-item-06" style="padding-top:25px;">
			<?php 
				if ($user->uid == 0) {
					print render($page['newsletter_form']); 
				}
			?>
			<div class="footer-nav">
				<?php print render($page['footer_menu']); ?>
			</div>
			<div class="copyrights"><?php print render($page['copyrights']); ?></div>
		</div>
	</div>
<?php } ?>

</div>



<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.bxslider.min.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		jQuery('.slider4').bxSlider({
			minSlides: 5,
			maxSlides: 5,
			slideWidth: 250,
			//moveSlides: 5,
			slideMargin: 20,
			speed: 800
		});
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var wall = new freewall("#freewall1 .view-content");
		wall.reset({
			selector: '.brick',
			animate: true,
			cellW: 320,
			cellH: 'auto',
			delay: 50,
			gutterY: 10,
			gutterX: 10,
			onResize: function() {
				wall.fitWidth();
			}
		});

		wall.container.find('.brick img').load(function() {
			wall.fitWidth();
		});
		wall.fitWidth();
		var wall2 = new freewall("#freewall2 .view-content");
		wall2.reset({
			selector: '.brick',
			animate: true,
			cellW: 320,
			cellH: 'auto',
			delay: 50,
			gutterY: 10,
			gutterX: 10,
			onResize: function() {
				wall2.fitWidth();
			}
		});
		wall2.fitWidth();
		wall2.container.find('.brick img').load(function() {
			wall2.fitWidth();
		});

		var wall3 = new freewall("#freewall3 .view-content");
		wall3.reset({
			selector: '.brick',
			animate: true,
			cellW: 320,
			cellH: 'auto',
			delay: 50,
			gutterY: 10,
			gutterX: 10,
			onResize: function() {
				wall3.fitWidth();
			}
		});
		wall3.fitWidth();
		wall3.container.find('.brick img').load(function() {
		  wall3.fitWidth();
		});

		var wall4 = new freewall("#freewall4 .view-content");
		wall4.reset({
			selector: '.brick',
			animate: true,
			cellW: 320,
			cellH: 'auto',
			delay: 50,
			gutterY: 10,
			gutterX: 10,
			onResize: function() {
				wall4.fitWidth();
			}
		});
		wall4.fitWidth();
		wall4.container.find('.brick img').load(function() {
			wall4.fitWidth();
		});
	});
</script>

<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/jquery.cycle.all.js"></script>
<script type="text/javascript">
	jQuery(function($) {
		jQuery('.banner-slider-02').cycle({
			fx: '<?php echo variable_get('right_slider_effect', 'fade'); ?>',
			speed: <?php echo variable_get('right_slider_speed', '3000'); ?>,
			height: 297,
			width: 515,
			timeout: <?php echo variable_get('right_slider_timeout', '3000'); ?>
		});
		jQuery('.banner-slider-01').cycle({
			fx: '<?php echo variable_get('left_slider_effect', 'scrollLeft'); ?>',
			speed: <?php echo variable_get('left_slider_speed', '3000'); ?>,
			height: 297,
			width: 515,
			timeout: <?php echo variable_get('left_slider_timeout', '3000'); ?>
		});
		jQuery('#edit-newsletters .form-type-checkbox').each(function() { //loop through each checkbox
			jQuery(".form-checkbox").prop('checked', true);
		});
	});
</script>

<script type="text/javascript">
    jQuery(function(){
        jQuery('.lastmt-item .banner-selectbox .container-inline').removeClass('container-inline').addClass('container-inline1');
				jQuery('.front  .banner-selectbox .view-treatment-category .ctools-jump-menu-select').selectric();
	});
	jQuery(".menu-link").click(function(){
		  jQuery("#menu").toggleClass("active");
		  jQuery(".container").toggleClass("active");
	});

</script>

<script type="text/javascript" src="<?php echo base_path() . path_to_theme(); ?>/js/placeholders.min.js"></script>

<?php if($device != "mobile") { ?>

	<script type="text/javascript">
	jQuery(document).ready(function(){
	   jQuery('a.menu-link-arrow').click(function(){
			//if(document.referrer.indexOf(window.location.hostname) != -1){
				window.history.go(-1);
				return false;
		   // }
		});
	});

	</script>

<?php } ?>

