<?php

// Do NOT change this file without contacting CBIT

function cbit_rules_action_info() {
$actions = array(
    'cbit_action_signup_data' => array(
        'label' => t('Hent e-adm data ved tilmelding'),
        'group' => t('Portal sync'),
        ),
     'cbit_action_sync_data' => array(
        'label' => t('Sync e-adm data'),
        'group' => t('Portal sync'),
        ),
      'cbit_action_get_logo' => array(
        'label' => t('Hent logo'),
        'parameter' => array(
					'custid' => array(
					'type' => 'integer',
					'label' => t('Kunde ID')),
				),
				'group' => t('Portal sync'),
        ),
				'cbit_action_make_subdir' => array(
        'label' => t('Opret fil folder'),
        'parameter' => array(
					'custid' => array(
					'type' => 'integer',
					'label' => t('Kunde ID')),
				),
				'group' => t('Portal sync'),
        ),
        'cbit_action_get_emp_image' => array(
        'label' => t('Hent medarbejder foto'),
        'parameter' => array(
					'empid' => array(
					'type' => 'integer',
					'label' => t('Medarbejder ID')),
					'custid' => array(
					'type' => 'integer',
					'label' => t('Kunde ID')),
				),
				'group' => t('Portal sync')
				),
				'cbit_action_get_loc_image' => array(
        'label' => t('Hent lokations billede'),
        'parameter' => array(
					'empid' => array(
					'type' => 'integer',
					'label' => t('Lokations ID')),
					'custid' => array(
					'type' => 'integer',
					'label' => t('Kunde ID')),
				),
				'group' => t('Portal sync'),
        ),
        'cbit_action_sleep' => array(
        'label' => t('Sleep'),
        'parameter' => array(
					'sec' => array(
					'type' => 'integer',
					'label' => t('Sekunder')),
				),
				'group' => t('Portal sync'),
        ),
				'cbit_action_get_price' => array(
        'label' => t('Beregn booking pris'),
        'parameter' => array(
					'bookstr' => array(
					'type' => 'integer',
					'label' => t('Booking info')),
				),
				'group' => t('Booking'),
        ),
        'cbit_action_eadm_order' => array(
        'label' => t('Send booking til eadm'),
        'parameter' => array(
					'orderid' => array(
					'type' => 'integer',
					'label' => t('Ordre id')),
					'tid' => array(
					'type' => 'integer',
					'label' => t('Transaktion id')),
				),
				'group' => t('Booking'),
        ),
        'cbit_action_eadm_dummy_order' => array(
        'label' => t('Check booking'),
        'parameter' => array(
					'orderid' => array(
					'type' => 'integer',
					'label' => t('Ordre id')),
					'tid' => array(
					'type' => 'integer',
					'label' => t('Transaktion id')),
					'res' => array(
					'type' => 'integer',
					'label' => t('Resultat')),
				),
				'group' => t('Booking'),
        ),
        'cbit_action_mail_receit' => array(
        'label' => t('Send kvittering'),
        'parameter' => array(
					'orderid' => array(
					'type' => 'integer',
					'label' => t('Ordre id')),
				),
				'group' => t('Booking'),
				),
    );

return $actions;
}


/**
 *	Send receit to buyer
 *	Called from rule xxx
 */
function cbit_action_mail_receit($orderid) {

	$order = commerce_order_load($orderid);
	
	if ($order) {
		$customer_email = $order->mail;
		$created = $order->created;		
		$out = "<table border=\"1\">";
		$out .= "<tr><td>Produkt</td><td>Behandling</td><td>Pris</td><td>Antal</td><td>Total</td></tr>";
		
		$total = 0;
		$info_txt = '';
		
		foreach($order->commerce_line_items as $lines) {
			foreach($lines as $line) {
				$line_id = (int) $line['line_item_id'];
				$line_item = commerce_line_item_load($line_id);
				if ($line_item->field_bookdata && isset($line_item->field_bookdata['und'][0]['value'])) {
					$quantity = (int) $line_item->quantity;
					$commerce_unit_price = ((int) $line_item->commerce_unit_price['und'][0]['amount']) * 0.01;
					$commerce_total = ((int) $line_item->commerce_total['und'][0]['amount']) * 0.01;
					$commerce_product_id = $line_item->commerce_product['und'][0]['product_id'];
					$treatment = $line_item->field_treatment['und'][0]['value'];
					$product = commerce_product_load($commerce_product_id);
					// Add clinic info
					$bookstr = $line_item->field_bookdata['und'][0]['value'];
					$book_arr = explode("¤", $bookstr);
					$loc_id = $book_arr[0];
					$loc_data = cbit_loc_data($loc_id);
					
					$info_txt .= "<br><br>Du har booket en behandling hos ".$loc_data['name'];
					$info_txt .= "<br>Hvis du har spørgsmål til behandlingen, så kontakt venligst klinikken:<br>";
					$info_txt .= $loc_data['name']."<br>";
					$info_txt .= $loc_data['address']."<br>";
					$info_txt .= $loc_data['phone']."<br>";
					$info_txt .= $loc_data['email']."<br>";
					
					
				}
				else {
				// Product sale
					$quantity = (int) $line_item->quantity;
					$commerce_unit_price = ((int) $line_item->commerce_unit_price['und'][0]['amount']) * 0.01;
					$commerce_total = ((int) $line_item->commerce_total['und'][0]['amount']) * 0.01;
					$commerce_product_id = $line_item->commerce_product['und'][0]['product_id'];
					$treatment = "";
					$product = commerce_product_load($commerce_product_id);
				}
				
				$total += $commerce_total;
				
				$out .= "<tr><td>".$product->title."</td><td>".$treatment."</td><td>".number_format($commerce_unit_price, 2, ",", ".")."</td><td>".$quantity."</td><td>".number_format($commerce_total, 2, ",", ".")."</td></tr>";
			}
		}
	}
	
	$out .= "<tr><td colspan=\"5\"> </td></tr>";
	$out .= "<tr><td colspan=\"4\">I alt </td><td>".number_format($total, 2, ",", ".")."</td></tr>";
	$out .= "</table>";
	
	
	// Get customer profile
	$profile_id = $order->commerce_customer_billing['und'][0]['profile_id'];
	
	$profile = commerce_customer_profile_load($profile_id);
	if ($profile) {
		$customer_name = $profile->commerce_customer_address['und'][0]['name_line'];
		$customer_address = $profile->commerce_customer_address['und'][0]['thoroughfare'];
		$customer_zipcode = $profile->commerce_customer_address['und'][0]['postal_code'];
		$customer_city = $profile->commerce_customer_address['und'][0]['locality'];
		$customer_mobile = $profile->field_mobile['und'][0]['value'];
		$email_reminder = $profile->field_email_reminder['und'][0]['value'];
		$email_marketing = $profile->field_email_marketing['und'][0]['value'];
		$sms_reminder = $profile->field_sms_reminder['und'][0]['value'];
		$sms_marketing = $profile->field_sms_marketing['und'][0]['value'];
	}
	
	$order_info = "Ordrenr: ".$orderid."<br>";
	$order_info .= "Oprettet: ".date("d-m-Y H:i", $created)."<br><br>";
	
	$cust_info = "Kundeoplysninger:<br>";
	$cust_info .= $customer_name."<br>";
	$cust_info .= $customer_address."<br>";
	$cust_info .= $customer_zipcode." ".$customer_city."<br><br>";
	
	
	// Send mail
	$my_module = 'cbit';
  $my_mail_token = microtime();
  /*
  if ($from == 'default_from') {
    // Change this to your own default 'from' email address.
    $from = variable_get('system_mail', 'My Email Address <example@example.com>');
  }
  */
  $from = "tm@cbit.dk";
  $to = $customer_email;
  
  $mesg = $order_info.$cust_info.$out.$info_txt;
  
  $message = array(
    'id' => $my_module . '_' . $my_mail_token,
    'to' => $to,
    'subject' => 'Tak for din bestilling',
    'body' => array($mesg),
    'headers' => array(
      'From' => $from,
      'Sender' => $from,
      'Return-Path' => $from,
    ),
  );
  $system = drupal_mail_system($my_module, $my_mail_token);
  $message = $system->format($message);
  if ($system->mail($message)) {
		// Log ??
    return TRUE;
  }
  else {
		// Handle error
    return FALSE;
  }
}
 
 
/**
 * Send booking to eadm to commit booking and handle economics.
 * Called when a commerce payment transaction is saved and OK
 */

 function cbit_action_eadm_order($orderid, $tid) {

	cbit_eadm_handle_order($orderid);
	cbit_eadm_handle_transaction($tid);

}


function cbit_action_eadm_dummy_order($orderid, $tid, &$res) {

	$res = 7;
	return $res;
}

/**
 * Get price
 */

 function cbit_action_get_price($bookstr) {

	return 5400;
}

function cbit_action_sleep($sec) {

	sleep($sec);
}


/**
 * Get logo from e-adm and add to customer node
 */

 function cbit_action_get_logo($custid) {
  
  $new_dir = "public://$custid/";
  
  if (!file_exists($new_dir) and !is_dir($new_dir)) {
		drupal_mkdir($new_dir);
  }
  
  $handle = fopen('http://eadministration.dk/logo/'.$custid.'.jpg', 'r');
  $file = file_save_data($handle, 'public://'.$custid.'/'.'logo.jpg', FILE_EXISTS_REPLACE);
  fclose($handle);
   
  // Get the node
  $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'customer')
    ->fieldCondition('field_eadm_id', 'value', $custid, '=')
    ->range(0, 1)
    ->addMetaData('account', user_load(1)); // Run the query as user 1. ?????
    $result = $query->execute();
   
   if (isset($result['node'])) {
      $nid = array_keys($result['node']);
      $node = node_load($nid[0]);    
      $node->field_customer_logo['und'][0] = (array) $file;
   }
}


/**
 * Get employee image from e-adm and add to employee node
 */

 function cbit_action_get_emp_image($empid, $custid) {
  
  $handle = fopen('http://www.cliniquekavi.dk/medarb_foto.asp?id='.$empid, 'r');
  $file = file_save_data($handle, 'public://'.$custid.'/emp_'.$empid.'.jpg', FILE_EXISTS_REPLACE);
  fclose($handle);
   
  // Get the node
  $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'employee')
    ->fieldCondition('field_eadm_id', 'value', $empid, '=')
    ->range(0, 1)
    ->addMetaData('account', user_load(1)); // Run the query as user 1. ?????
    $result = $query->execute();
   
   if (isset($result['node'])) {
      $nid = array_keys($result['node']);
      $node = node_load($nid[0]);    
      $node->field_emp_foto['und'][0] = (array) $file;
   }
}


/**
 * Get location image from e-adm and add to location node
 */

 function cbit_action_get_loc_image($locid, $custid) {
  
  $handle = fopen("http://eadministration.dk/getbanner.asp?id=".$custid."&lokation=".$locid, 'r');
  $file = file_save_data($handle, 'public://'.$custid.'/'.$locid.'.jpg', FILE_EXISTS_REPLACE);
  fclose($handle);
   
  // Get the node
  $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'location')
    ->fieldCondition('field_eadm_id', 'value', $locid, '=')
    ->range(0, 1)
    ->addMetaData('account', user_load(1)); // Run the query as user 1. ?????
    $result = $query->execute();
   
   if (isset($result['node'])) {
      $nid = array_keys($result['node']);
      $node = node_load($nid[0]);    
      $node->field_location_image['und'][0] = (array) $file;
   }
}




/**
 * Make customer subdir if needed
 */

 function cbit_action_make_subdir($custid) {
  
  $new_dir = "public://$custid/";
  
  if (!file_exists($new_dir) and !is_dir($new_dir)) {
		drupal_mkdir($new_dir);
  }
}





/**
 * Start feeds to fetch locations, item_categories and items for the 
 * customer id retrieved from the current node.
 */

 function cbit_action_signup_data() {

  $location_url = "http://localhost/dummy/getlocations.php?klient=";
  $itemcategory_url = "http://localhost/dummy/getitemcategories.php?klient=";
  $item_url = "http://localhost/dummy/getitems.php?klient=";
 
 
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $nid = arg(1);
    $load_full_node = node_load($nid);
    $cust_id = (int) $load_full_node->field_signup_customer_id['und'][0]['value'];
  
  
    // Get locations
    // Define the id (machine name) of the importer you want to use.
    $importer_id = 'feed_signup_locations';

    // Load the Feeds Source object.
    $source = feeds_source($importer_id);

    // Load the source fetcher config.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);

    // Create url
    $feed_url = $location_url.$cust_id;

    // Set url
    $fetcher_config['source'] = $feed_url;

    // Set the source fetcher config.
    $source->setConfigFor($source->importer->fetcher, $fetcher_config);

    // Save the source.
    $source->save();

    drupal_set_message(t('Importerer lokationer'));
    
    // Execute the import.
    $source->startImport();

  
    // Get item categories
    // Define the id (machine name) of the importer you want to use.
    $importer_id = 'feed_signup_itemcategories';

    // Load the Feeds Source object.
    $source = feeds_source($importer_id);

    // Load the source fetcher config.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);

    // Create url
    $feed_url = $itemcategory_url.$cust_id;

    // Set url
    $fetcher_config['source'] = $feed_url;

    // Set the source fetcher config.
    $source->setConfigFor($source->importer->fetcher, $fetcher_config);

    // Save the source.
    $source->save();

    drupal_set_message(t('Importerer varegrupper'));
    
    // Execute the import.
    $source->startImport();

    
    // Get items
    // Define the id (machine name) of the importer you want to use.
    $importer_id = 'feed_signup_items';

    // Load the Feeds Source object.
    $source = feeds_source($importer_id);

    // Load the source fetcher config.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);

    // Create url
    $feed_url = $item_url.$cust_id;

    // Set url
    $fetcher_config['source'] = $feed_url;

    // Set the source fetcher config.
    $source->setConfigFor($source->importer->fetcher, $fetcher_config);

    // Save the source.
    $source->save();

    drupal_set_message(t('Importerer ydelser'));
    
    // Execute the import.
    $source->startImport();
  }
  else {
    drupal_set_message(t('Kunne ikke finde kunde nummer'), 'error');
  }
} 


/**
 * Start feeds to fetch locations, item_categories and items for the 
 * customer id retrieved from the current node.
 */

 function cbit_action_sync_data() {

  $location_url = "http://localhost/dummy/getlocations.php?klient=";
  $itemcategory_url = "http://localhost/dummy/getitemcategories.php?klient=";
  $item_url = "http://localhost/dummy/getitems.php?klient=";
 
 
 var_dump("Running...");
 return;
 
  $now = time();
  
  
  
  // Calc timestamp: now - delta

  // Get locations
  // Define the id (machine name) of the importer you want to use.
  $importer_id = 'feed_signup_locations';

  // Load the Feeds Source object.
  $source = feeds_source($importer_id);

  // Load the source fetcher config.
  $fetcher_config = $source->getConfigFor($source->importer->fetcher);

  // Create url
  $feed_url = $location_url.$cust_id;

  // Set url
  $fetcher_config['source'] = $feed_url;

  // Set the source fetcher config.
  $source->setConfigFor($source->importer->fetcher, $fetcher_config);

  // Save the source.
  $source->save();

  drupal_set_message(t('Synkroniserer lokationer'));
  
  // Execute the import.
  $source->startImport();


  // Get item categories
  // Define the id (machine name) of the importer you want to use.
  $importer_id = 'feed_signup_itemcategories';

  // Load the Feeds Source object.
  $source = feeds_source($importer_id);

  // Load the source fetcher config.
  $fetcher_config = $source->getConfigFor($source->importer->fetcher);

  // Create url
  $feed_url = $itemcategory_url.$cust_id;

  // Set url
  $fetcher_config['source'] = $feed_url;

  // Set the source fetcher config.
  $source->setConfigFor($source->importer->fetcher, $fetcher_config);

  // Save the source.
  $source->save();

  drupal_set_message(t('Synkroniserer varegrupper'));
  
  // Execute the import.
  $source->startImport();

  
  // Get items
  // Define the id (machine name) of the importer you want to use.
  $importer_id = 'feed_signup_items';

  // Load the Feeds Source object.
  $source = feeds_source($importer_id);

  // Load the source fetcher config.
  $fetcher_config = $source->getConfigFor($source->importer->fetcher);

  // Create url
  $feed_url = $item_url.$cust_id;

  // Set url
  $fetcher_config['source'] = $feed_url;

  // Set the source fetcher config.
  $source->setConfigFor($source->importer->fetcher, $fetcher_config);

  // Save the source.
  $source->save();

  drupal_set_message(t('Synkroniserer ydelser'));
  
  // Execute the import.
  $source->startImport();
} 


