<?php 
global $base_url;
$results = $variables['result'];
if(count($results)){
foreach($results as $nid =>$like){
	$node_details = node_load($nid);
	//echo "<pre>";print_r($node_details);
	$title = $node_details->title;
	$date = date('d. F Y', $node_details->created);
	if(isset($node_details->field_image['und'])){
		$path = $node_details->field_image['und'][0]['uri'];
	}else{
		$field = field_info_fields();
		$path = file_load($field['field_image']['settings']['default_image'])->uri;
	}
	$thumbnail_url = image_style_url('popular', $path);
	$body = substr($node_details->body['und'][0]['value'],0,100).' ...</p>';
?>
<div class="item-popular">
	<div class="popular-details">
		<span class="popular-date"><?php echo $date; ?></span>
		<?php //print drupal_render(plus1_build_node_jquery_widget($nid, $tag = 'plus1_node_vote')); ?>
		<?php print like_widget_node($nid) ?>
		<p><?php echo l(t($body), 'node/'.$node_details->nid, array('attributes' => array('class' => array('popular-link')),'html' => TRUE)); ?></p>
	</div>
	<div class="popular-image">
		<a href="<?php echo $base_url.'/'.drupal_get_path_alias('node/'.$node_details->nid); ?>">
			<img src="<?php print $thumbnail_url ?>">
		</a>
	</div>
	<div class="clear"></div>
</div>
<?php
}
}else{
	echo '<div class="empty-result">No Results found</div>';
}
?>
