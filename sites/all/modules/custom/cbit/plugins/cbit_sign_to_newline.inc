<?php

/**
 * defines the ctools plugin
 * Used to clean up text in Feeds Tamper
 */
$plugin = array(
  'form' => 'cbit_sign_to_newline_form',
  'callback' => 'cbit_sign_to_newline_callback',
  'name' => 'Sign to new line',
  'multi' => 'direct',
  'category' => 'Text',
);

/**
 * Even though there are no settings we still need the form
 */
function cbit_sign_to_newline_form($importer, $element_key, $settings) {
  $form = array();
  return $form;
}

/**
 * Replace a sign with a linebreak
 */
function cbit_sign_to_newline_callback($source, $item_key, $element_key, &$field, $settings) {
  $field = preg_replace("/¤/", "\n", $field);
}

?>
