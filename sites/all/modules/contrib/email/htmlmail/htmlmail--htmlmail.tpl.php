<?php

/**
 * @file
 * Sample template for HTML Mail test messages.
 */
?>
<table style="width: 560px; margin-left: auto; margin-right: auto;background-color:#ffffff;padding:20px;" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td height="83">
				<a href="http://www.heywellness.dk" target="_blank">
					<img src="http://www.devconvertic.dk/cbit/icon/heywellness_logo.jpg" alt="" width="143" height="76"/>
				</a>
			</td>
			<td style="text-align: right;">
				<span style="color: #e85c99;">
					<strong>
						<span style="font-family: georgia, palatino; font-size: 13px;"> 
							<table align="right">
								<tbody>
									<tr>
										<td>
											<a href="http://www.heywellness.dk/last-minute" style="color: #e85c99;text-decoration:none;padding-right:15px;" target="_blank">LAST MINUTE</a>
										</td>
										<td>
											<a href="http://www.heywellness.dk/blog" style="color: #e85c99;text-decoration:none;padding-right:15px;" target="_blank">BLOG</a>
										</td>
										<td>        
											<a href="http://www.heywellness.dk/artikler" style="color: #e85c99;text-decoration:none;" target="_blank">ARTIKLER</a>
										</td>
									</tr>
								</tbody>
							</table>
						</span>
					</strong>
				</span>
			</td>
		</tr>
		<tr>
			<td height="10px" colspan="2" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #E8E6F3;"></td>
		</tr>
		<tr style="height: 50px;">
			<td>
				<span style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 11px; color: #949292;">
					<strong>BLOG INDLÆG</strong>
				</span>
			</td>
			<td>
				<table align="right">
					<tbody>
					<tr>
						<td>
							<a href="http://facebook.com/heywellness.dk" target="_blank"><img style="float: right;" src="http://www.devconvertic.dk/cbit/icon/facebook_icon.jpg" alt="" /></a>
						</td>
						<td>
							<a href="http://instagram.com/heywellness.dk" target="_blank"><img style="float: right; padding: 0 5px 0 3px;" src="http://www.devconvertic.dk/cbit/icon/instagram_icon.jpg" alt="" /></a>
						</td>
						<td>
							<img style="float: right; padding: 0 5px 0 0px;" src="http://www.devconvertic.dk/cbit/icon/youtube_icon.jpg" alt="" />
						</td>
						<td>
							<img style="float: right;" src="http://www.devconvertic.dk/cbit/icon/google_icon.jpg" alt="" />
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border-top-width: 1px; border-top-style: solid; border-top-color: #E8E6F3;" colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="htmlmail-body" style="color:#2a2a2a;font-family: Georgia, Times, 'Times New Roman', serif; font-size: 18px; line-height: 150%;padding-bottom:10px;">
					<?php echo $body; ?>
				</div>
			</td>
		</tr>
		<tr>
			<td height="75" colspan="2" style="text-align: center;"><img src="http://www.devconvertic.dk/cbit/icon/wellness_love.jpg" alt="" /></td>
		</tr>
		<tr style="height: 40px;">
			<td style="font-family: tahoma, arial, helvetica, sans-serif; color: #9b9a9a; font-size: 11px; text-align: center; text-transform: uppercase;" colspan="2">HEYWELLNESS © A CBIT SPINOFF • BÜLOWSVEJ 3, 1870 FREDERIKSBERG C • INFO@HEYWLLNES.DK</td>
		</tr>
	</tbody>
</table>


<?php if ($debug): ?>
<hr />
<div class="htmlmail-debug">
  <dl><dt><p>
    To customize this test message:
  </p></dt><dd><ol><li><p><?php if (empty($theme)): ?>
    Visit <u>admin/config/system/htmlmail</u>
    and select a theme to hold your custom email template files.
  </p></li></ol></dd><dd><ol><li><p><?php elseif (empty($theme_path)): ?>
    Visit <u>admin/appearance</u>
    to enable your selected <u><?php echo ucfirst($theme); ?></u> theme.
  </p></li></ol></dd><dd><ol><li><p><?php endif; ?>
    Copy the
    <a href="http://drupalcode.org/project/htmlmail.git/blob_plain/refs/heads/7.x-2.x:/htmlmail--htmlmail.tpl.php"><code>htmlmail--htmlmail.tpl.php</code></a>
    file to your <u><?php echo ucfirst($theme); ?></u> theme directory
    <u><code><?php echo $theme_path; ?></code></u>.
  </p></li><li><p>
    Edit the copied file.
  </p></li></ol></dd></dl>
</div>
<?php endif;
