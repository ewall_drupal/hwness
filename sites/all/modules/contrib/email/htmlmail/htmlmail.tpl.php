<?php

/**
 * @file
 * Default template for HTML Mail
 *
 * DO NOT EDIT THIS FILE. Copy it to your theme directory, and edit the copy.
 *
 * ========================================================= Begin instructions.
 *
 * When formatting an email message with a given $module and $key, [1]HTML
 * Mail will use the first template file it finds from the following list:
 *  1. htmlmail--$module--$key.tpl.php
 *  2. htmlmail--$module.tpl.php
 *  3. htmlmail.tpl.php
 *
 * For each filename, [2]HTML Mail looks first in the chosen Email theme
 * directory, then in its own module directory, before proceeding to the
 * next filename.
 *
 * For example, if example_module sends mail with:
 * drupal_mail("example_module", "outgoing_message" ...)
 *
 *
 * the possible template file names would be:
 *  1. htmlmail--example_module--outgoing_message.tpl.php
 *  2. htmlmail--example_module.tpl.php
 *  3. htmlmail.tpl.php
 *
 * Template files are cached, so remember to clear the cache by visiting
 * admin/config/development/performance after changing any .tpl.php files.
 *
 * The following variables available in this template:
 *
 * $body
 *        The message body text.
 *
 * $module
 *        The first argument to [3]drupal_mail(), which is, by convention,
 *        the machine-readable name of the sending module.
 *
 * $key
 *        The second argument to [4]drupal_mail(), which should give some
 *        indication of why this email is being sent.
 *
 * $message_id
 *        The email message id, which should be equal to
 *        "{$module}_{$key}".
 *
 * $headers
 *        An array of email (name => value) pairs.
 *
 * $from
 *        The configured sender address.
 *
 * $to
 *        The recipient email address.
 *
 * $subject
 *        The message subject line.
 *
 * $body
 *        The formatted message body.
 *
 * $language
 *        The language object for this message.
 *
 * $params
 *        Any module-specific parameters.
 *
 * $template_name
 *        The basename of the active template.
 *
 * $template_path
 *        The relative path to the template directory.
 *
 * $template_url
 *        The absolute URL to the template directory.
 *
 * $theme
 *        The name of the Email theme used to hold template files. If the
 *        [5]Echo module is enabled this theme will also be used to
 *        transform the message body into a fully-themed webpage.
 *
 * $theme_path
 *        The relative path to the selected Email theme directory.
 *
 * $theme_url
 *        The absolute URL to the selected Email theme directory.
 *
 * $debug
 *        TRUE to add some useful debugging info to the bottom of the
 *        message.
 *
 * Other modules may also add or modify theme variables by implementing a
 * MODULENAME_preprocess_htmlmail(&$variables) [6]hook function.
 *
 * References
 *
 * 1. http://drupal.org/project/htmlmail
 * 2. http://drupal.org/project/htmlmail
 * 3. http://api.drupal.org/api/drupal/includes--mail.inc/function/drupal_mail/7
 * 4. http://api.drupal.org/api/drupal/includes--mail.inc/function/drupal_mail/7
 * 5. http://drupal.org/project/echo
 * 6. http://api.drupal.org/api/drupal/modules--system--theme.api.php/function/hook_preprocess_HOOK/7
 *
 * =========================================================== End instructions.
 */
  $template_name = basename(__FILE__);
  $current_path = realpath(NULL);
  $current_len = strlen($current_path);
  $template_path = realpath(dirname(__FILE__));
  if (!strncmp($template_path, $current_path, $current_len)) {
    $template_path = substr($template_path, $current_len + 1);
  }
  $template_url = url($template_path, array('absolute' => TRUE));
?>
<table align="center" style="background-color: #ffffff; border: 3px solid #e85997; padding: 0 10px; width: 560px;" cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td height="83">
				<a href="http://www.heywellness.dk" target="_blank">
					<img src="http://www.devconvertic.dk/cbit/icon/heywellness_logo.jpg" alt="" width="143" height="76"/>
				</a>
			</td>
			<td style="text-align: right;">
				<span style="color: #e85c99;">
					<strong>
						<span style="font-family: georgia, palatino; font-size: 13px;"> 
							<table align="right">
								<tbody>
									<tr>
										<td>
											<a href="http://www.heywellness.dk/last-minute" style="color: #e85c99;text-decoration:none;padding-right:15px;" target="_blank">LAST MINUTE</a>
										</td>
										<td>
											<a href="http://www.heywellness.dk/blog" style="color: #e85c99;text-decoration:none;padding-right:15px;" target="_blank">BLOG</a>
										</td>
										<td>        
											<a href="http://www.heywellness.dk/artikler" style="color: #e85c99;text-decoration:none;" target="_blank">ARTIKLER</a>
										</td>
									</tr>
								</tbody>
							</table>
						</span>
					</strong>
				</span>
			</td>
		</tr>
		<tr>
			<td height="10px" colspan="2" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #E8E6F3;"></td>
		</tr>
		<tr style="height: 50px;">
			<td style="vertical-align:middle; font-family: tahoma, arial, helvetica, sans-serif; font-size: 11px; color: transparent; font-weight: bold;">.</td>
			<td>
				<table align="right">
					<tbody>
					<tr>
						<td>
							<a href="http://facebook.com/heywellness.dk" target="_blank"><img style="float: right;" src="http://www.devconvertic.dk/cbit/icon/facebook_icon.jpg" alt="" /></a>
						</td>
						<td>
							<a href="http://instagram.com/heywellness.dk" target="_blank"><img style="float: right; padding: 0 5px 0 3px;" src="http://www.devconvertic.dk/cbit/icon/instagram_icon.jpg" alt="" /></a>
						</td>
						<td>
							<img style="float: right; padding: 0 5px 0 0px;" src="http://www.devconvertic.dk/cbit/icon/youtube_icon.jpg" alt="" />
						</td>
						<td>
							<img style="float: right;" src="http://www.devconvertic.dk/cbit/icon/google_icon.jpg" alt="" />
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="border-top-width: 1px; border-top-style: solid; border-top-color: #E8E6F3;" colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="htmlmail-body" style="color:#2a2a2a;font-family: Georgia, Times, 'Times New Roman', serif; font-size: 15px; line-height: 150%;padding-bottom:10px;">
					<?php echo $body; ?>
				</div>
			</td>
		</tr>
		<tr>
			<td height="75" colspan="2" style="text-align: center;"><img src="http://www.devconvertic.dk/cbit/icon/wellness_love.jpg" alt="" /></td>
		</tr>
		<tr style="height: 40px;">
			<td style="font-family: tahoma, arial, helvetica, sans-serif; color: #9b9a9a; font-size: 11px; text-align: center; text-transform: uppercase;" colspan="2">HEYWELLNESS © A CBIT SPINOFF • BÜLOWSVEJ 3, 1870 FREDERIKSBERG C • INFO@HEYWELLNESS.DK</td>
		</tr>
	</tbody>
</table>

<?php if ($debug):
  $module_template = "htmlmail--$module.tpl.php";
  $message_template = "htmlmail--$module--$key.tpl.php";
?>
<hr />
<div class="htmlmail-debug">
  <dl><dt><p>
    To customize this message:
  </p></dt><dd><ol><li><p><?php if (empty($theme)): ?>
    Visit <u>admin/config/system/htmlmail</u>
    and select a theme to hold your custom email template files.
  </p></li><li><p><?php elseif (empty($theme_path)): ?>
    Visit <u>admin/appearance</u>
    to enable your selected
    <u><?php echo drupal_ucfirst($theme); ?></u> theme.
  </p></li><li><?php endif;
if ("$template_path/$template_name" == "$theme_path/$message_template"): ?><p>
    Edit your<br />
    <code><?php echo "$template_path/$template_name"; ?></code>
    <br />file.
  </p></li><li><?php
else:
  if (!file_exists("$theme_path/htmlmail.tpl.php")): ?><p>
    Copy<br />
    <code><?php echo "$module_path/htmlmail.tpl.php"; ?></code>
    <br />to<br />
    <code><?php echo "$theme_path/htmlmail.tpl.php"; ?></code>
  </p></li><li><?php
  endif;
  if (!file_exists("$theme_path/$module_template")): ?><p>
    For module-specific customization, copy<br />
    <code><?php echo "$module_path/htmlmail.tpl.php"; ?></code>
    <br />to<br />
    <code><?php echo "$theme_path/$module_template"; ?></code>
  </p></li><li><?php
  endif;
  if (!file_exists("$theme_path/$message_template")): ?><p>
    For message-specific customization, copy<br />
    <code><?php echo "$module_path/htmlmail.tpl.php"; ?></code>
    <br />to<br />
    <code><?php echo "$theme_path/$message_template"; ?></code>
  </p></li><li><?php endif; ?><p>
    Edit the copied file.
  </p></li><li><?php
endif; ?><p>
    Send a test message to make sure your customizations worked.
  </p></li><li><p>
    If you think your customizations would be of use to others,
    please contribute your file as a feature request in the
    <a href="http://drupal.org/node/add/project-issue/htmlmail">issue queue</a>.
  </p></li></ol></dd><?php if (!empty($params)): ?><dt><p>
    The <?php echo $module; ?> module sets the <u><code>$params</code></u>
    variable.  For this message,
  </p></dt><dd><p><code><pre>
$params = <?php echo check_plain(print_r($params, 1)); ?>
  </pre></code></p></dd><?php endif; ?></dl>
</div>
<?php endif;
