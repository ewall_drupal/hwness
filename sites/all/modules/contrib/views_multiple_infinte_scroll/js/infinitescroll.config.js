//Taken from: http://drupal.org/node/1473558
(function ($) {
  Drupal.behaviors.viewsMultiInfiniteScroll = {
    attach: function (context, settings) {
      $(function(){
        $('ul.multi_infinite_scroll_pager').each(function(index){
        	var parent = $(this).parents("div.view");
	        var id = $(parent).attr("class");
	        var ids = id.split(" ");
	        
	        for(var i=0;i<ids.length;i++)
		        if(ids[i].indexOf("view-display-id-")>-1) id = "."+ids[i];
	        var $container = $('#freewalla div.view-content');
	          $container.infinitescroll({
	            navSelector  : id+' .pager',    // selector for the paged navigation
	            nextSelector : id+' .pager-next a',  // selector for the NEXT link (to page 2)
	            itemSelector : id+' .views-row',     // selector for all items you'll retrieve
	            debug:false,
	            loading: {
	              finishedMsg: '',
	              img: Drupal.settings.basePath + 'sites/all/modules/contrib/views_infinite_scroll/images/ajax-loader.gif'
	            },
	            state:{currPage:0},
	            infid:index,
	            pathParse:function(path,current){
	            	path = path.split("").reverse().join(""); //reverse the string
		            path = path.replace(current,"@@@");		  //replace the first ocurrence of current page in reversed string
		            path = path.split("").reverse().join(""); //reverse again
		            path = path.split("@@@");				  //split the string
		            return path;
	            }
	        })
        });
  }); 
  }
};
})(jQuery);